<?php

return [
    'default' => 'velocity',

    'themes' => [
        'default' => [
            'views_path' => 'resources/themes/default/views',
            'assets_path' => 'public/themes/default/assets',
            'name' => 'Default'
        ],

        // 'bliss' => [
        //     'views_path' => 'resources/themes/bliss/views',
        //     'assets_path' => 'public/themes/bliss/assets',
        //     'name' => 'Bliss',
        //     'parent' => 'default'
        // ]

        'ypl' => [
            'views_path' => 'resources/themes/ypl/views',
            'assets_path' => 'public/themes/ypl/assets',
            'name' => 'YPL',
//            'parent' => 'velocity'
        ],

        'velocity' => [
            'views_path' => 'resources/themes/velocity/views',
            'assets_path' => 'public/themes/velocity/assets',
           'name' => 'Velocity',
        //   'parent' => 'default'
       ],
    ]
];