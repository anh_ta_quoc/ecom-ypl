<?php
function renderDesktopMenu($categories, $root = false)
{
    $menu = '';
    foreach ($categories as $category) {
        $html = '';
        $has_children = $category->children && $category->children->count();
        if ($root) {
            $html = '<li class="departments__item">
            <a class="departments__item-link" href="' . route('shop.productOrCategory.index', $category->url_path) . '">'
                . $category->name;
            if ($has_children) {
                $html .= '<svg class="departments__item-arrow" width="6px" height="9px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                        </svg>
                    </a>';
                $html .= '<div class="departments__submenu departments__submenu--type--menu">
                                <div class="menu menu--layout--classic">
                                    <div class="menu__submenus-container"></div>
                                    <ul class="menu__list">';
                $html .= renderDesktopMenu($category->children);
                $html .= '</ul></div></div>';
            } else {
                $html .= '</a>';
            }
            $html .= '</li>';
        } else {
            $html = '<li class="menu__item">
                <div class="menu__item-submenu-offset"></div>
                <a class="menu__item-link" href="' . route('shop.productOrCategory.index', $category->url_path) . '">'
                . $category->name;

            if ($has_children) {
                $html .= '<svg class="menu__item-arrow" width="6px" height="9px">
                        <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                    </svg>
                </a>';
                $html .= '<div class="menu__submenu"><!-- .menu -->
                            <div class="menu menu--layout--classic">
                                <div class="menu__submenus-container"></div>
                                <ul class="menu__list">';
                $html .= renderDesktopMenu($category->children);
                $html .= '</ul></div></div>';
            } else {
                $html .= '</a>';
            }
            $html .= '</li>';
        }
        $menu .= $html;
    }
    return $menu;
}

function renderSideBarMenu($categories, $level = 1)
{
    $menu = '';
    foreach ($categories as $category) {
        $html = '';
        $has_children = $category->children && $category->children->count();
        if ($has_children) {
            $child_level = $level + 1;
            $html = '  <li class="filter-categories-alt__item filter-categories-alt__item--open" data-collapse-item>
                            <button class="filter-categories-alt__expander" data-collapse-trigger></button>
                             <a href="' . route('shop.productOrCategory.index', $category->url_path) . '">'. $category->name . '</a>';
            $html .= '<div class="filter-categories-alt__children" data-collapse-content>
                           <ul class="filter-categories-alt__list filter-categories-alt__list--level--' . $child_level . '">';
            $html .= renderSideBarMenu($category->children, $child_level);
            $html .= '</ul></div></li>';
        } else {
            $html = ' <li class="filter-categories-alt__item" data-collapse-item>
                    <a href="' . route('shop.productOrCategory.index', $category->url_path) . '">'
                . $category->name . '</a>';
        }
        $menu .= $html;

    }
    return $menu;

}

