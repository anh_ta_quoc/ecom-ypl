<?php

namespace App\Models;

use Webkul\Customer\Models\Customer as CustomerBaseModel;

class Customer extends CustomerBaseModel
{
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'date_of_birth',
        'email',
        'phone',
        'password',
        'api_token',
        'customer_group_id',
        'subscribed_to_news_letter',
        'is_verified',
        'token',
        'notes',
        'status',
        'is_dealer',
        'request_dealer'
    ];
}