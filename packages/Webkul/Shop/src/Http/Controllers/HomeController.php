<?php

namespace Webkul\Shop\Http\Controllers;

use Webkul\Shop\Http\Controllers\Controller;
use Webkul\Core\Repositories\SliderRepository;
use Webkul\Velocity\Helpers\Helper;
use Webkul\Product\Helpers\ProductImage;
use Webkul\Velocity\Repositories\Product\ProductRepository as VelocityProductRepository;
 class HomeController extends Controller
{

     /**
      * SliderRepository object
      *
      * @var \Webkul\Core\Repositories\SliderRepository
      */
     protected $sliderRepository;

     /**
      * Create a new controller instance.
      *
      * @param  \Webkul\Core\Repositories\SliderRepository  $sliderRepository
      * @return void
      */
     /**
      * ProductImage object
      *
      * @var \Webkul\Product\Helpers\ProductImage
      */
     protected $productImageHelper;
     /**
      * ProductRepository object
      *
      * @var \Webkul\Product\Repositories\ProductRepository
      */
     protected $productRepository;

     /**
      * ProductRepository object of velocity package
      *
      * @var \Webkul\Velocity\Repositories\Product\ProductRepository
      */
     protected $velocityProductRepository;

     /**
      * CategoryRepository object of velocity package
      *
      * @var \Webkul\Category\Repositories\CategoryRepository
      */
     public function __construct(SliderRepository $sliderRepository,
                                 Helper $velocityHelper,
                                 ProductImage $productImageHelper,
                                 VelocityProductRepository $velocityProductRepository)
     {
         $this->sliderRepository = $sliderRepository;
         $this->velocityHelper = $velocityHelper;

         $this->productImageHelper = $productImageHelper;

         $this->velocityProductRepository = $velocityProductRepository;

         parent::__construct();
     }
    /**
     * loads the home page for the storefront
     * 
     * @return \Illuminate\View\View 
     */
    public function index()
    {
        $currentChannel = core()->getCurrentChannel();

        $currentLocale = core()->getCurrentLocale();

        $sliderData = $this->sliderRepository
          ->where('channel_id', $currentChannel->id)
          ->where('locale', $currentLocale->code)
          ->get()
          ->toArray();
        $count = 10;
        $feature_products = $this->velocityProductRepository->getFeaturedProducts($count);
        $arr_feature_products = [];
        $new_products = $this->velocityProductRepository->getNewProducts($count);
        $arr_new_products = [];
        foreach ($feature_products as $product) {
            array_push($arr_feature_products, $this->velocityHelper->formatProduct($product));
        }
        foreach ($new_products as $product) {
            array_push($arr_new_products, $this->velocityHelper->formatProduct($product));
        }

        return view($this->_config['view'], compact('sliderData','arr_feature_products', 'arr_new_products'));
    }

    /**
     * loads the home page for the storefront
     * 
     * @return \Exception
     */
    public function notFound()
    {
        abort(404);
    }
}