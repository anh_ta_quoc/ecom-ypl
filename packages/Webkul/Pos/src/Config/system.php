<?php

return [
    [
        'key' => 'pos',
        'name' => 'pos::app.admin.system.pos.extension_name',
        'sort' => 2
    ],  [
        'key' => 'pos.configuration',
        'name' => 'pos::app.admin.system.pos.settings',
        'sort' => 1,
    ],  [
        'key' => 'pos.configuration.general',
        'name' => 'pos::app.admin.system.pos.general',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'status',
                'title' => 'pos::app.admin.system.general.status',
                'type' => 'select',
                'validation' => 'required',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.general.active',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.general.inactive',
                        'value' => false
                    ]
                ]
            ],
            [
                'name' => 'heading_on_login',
                'title' => 'pos::app.admin.system.general.login-heading',
                'type' => 'text',
                'validation' => 'required|max:50',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'sub_heading_on_login',
                'title' => 'pos::app.admin.system.general.sub-login-heading',
                'type' => 'text',
                'validation' => 'required|max:50',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'footer_content',
                'title' => 'pos::app.admin.system.general.footer-content',
                'type' => 'text',
                'validation' => 'required|max:50',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'footer_note',
                'title' => 'pos::app.admin.system.general.footer-note',
                'type' => 'text',
                'validation' => 'required|max:50|alpha_spaces',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'footer_link_text',
                'title' => 'pos::app.admin.system.general.footer-link-text',
                'type' => 'text',
                'validation' => 'required|max:50|alpha_spaces',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'footer_link',
                'title' => 'pos::app.admin.system.general.footer-link',
                'type' => 'text',
                'validation' => 'max:150',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'pos_logo',
                'title' => 'pos::app.admin.system.general.pos-logo',
                'type' => 'image',
                'validation' => 'image|ext:jpeg,jpg,png,svg|size:20480',
                'channel_based' => false,
                'locale_based' => false
            ]
        ]
    ],  [
        'key' => 'pos.configuration.barcode',
        'name' => 'pos::app.admin.system.pos.barcode',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'size',
                'title' => 'pos::app.admin.system.barcode.size',
                'type' => 'text',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'print_product_name',
                'title' => 'pos::app.admin.system.barcode.print-option',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.barcode.yes',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.barcode.no',
                        'value' => false
                    ]
                ]
            ], [
                'name' => 'image_type',
                'title' => 'pos::app.admin.system.barcode.image-style',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.barcode.horizontal',
                        'value' => 'horizontal'
                    ], [
                        'title' => 'pos::app.admin.system.barcode.vertical',
                        'value' => 'vertical'
                    ]
                ]
            ], [
                'name' => 'barcode_with',
                'title' => 'pos::app.admin.system.barcode.format',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.barcode.product-id',
                        'value' => 'product_id'
                    ], [
                        'title' => 'pos::app.admin.system.barcode.sku',
                        'value' => 'sku'
                    ]
                ]
            ], [
                'name' => 'barcode_prefix',
                'title' => 'pos::app.admin.system.barcode.barcode_prefix',
                'type' => 'text',
                'validate' => 'alpha|max:10',
                'channel_based' => false,
                'locale_based' => false
            ], [
                'name' => 'search_option',
                'title' => 'pos::app.admin.system.barcode.search_option',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.barcode.product_search',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.barcode.barcode_screener',
                        'value' => false
                    ]
                ]
            ], [
                'name' => 'hide_barcode',
                'title' => 'pos::app.admin.system.barcode.barcode_panel',
                'type' =>  'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.general.active',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.general.inactive',
                        'value' => false
                    ]
                ]
            ]
        ]
    ],  [
        'key' => 'pos.configuration.product',
        'name' => 'pos::app.admin.system.pos.product',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'allow_sku',
                'title' => 'pos::app.admin.system.product.allow_sku',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.general.active',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.general.inactive',
                        'value' => false
                    ]
                ]
            ],
        ]
    ],  [
        'key' => 'pos.configuration.bill-receipt',
        'name' => 'pos::app.admin.system.pos.bill-receipt',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'show_logo',
                'title' => 'pos::app.admin.system.bill-receipt.show-logo',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.general.active',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.general.inactive',
                        'value' => false
                    ]
                ]
            ],  [
                'name' => 'custom_address',
                'title' => 'pos::app.admin.system.bill-receipt.custom-address',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.general.active',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.general.inactive',
                        'value' => false
                    ]
                ]
            ],  [
                'name' => 'store_name',
                'title' => 'pos::app.admin.system.bill-receipt.store-name',
                'type' => 'text',
                'validation' => 'max:100',
                'channel_based' => true,
                'locale_based' => true
            ],  [
                'name' => 'store_address',
                'title' => 'pos::app.admin.system.bill-receipt.store-address',
                'type' => 'textarea',
                'validation' => 'max:200',
                'channel_based' => true,
                'locale_based' => true
            ],  [
                'name' => 'email_address',
                'title' => 'pos::app.admin.system.bill-receipt.email-address',
                'type' => 'text',
                'validation' => 'email',
                'channel_based' => true,
                'locale_based' => true
            ],  [
                'name' => 'website',
                'title' => 'pos::app.admin.system.bill-receipt.website',
                'type' => 'text',
                'channel_based' => false,
                'locale_based' => false
            ],  [
                'name' => 'phone_number',
                'title' => 'pos::app.admin.system.bill-receipt.contact-no',
                'type' => 'text',
                'validation' => 'numeric',
                'channel_based' => false,
                'locale_based' => false
            ],  [
                'name' => 'cc_number',
                'title' => 'pos::app.admin.system.bill-receipt.cc-no',
                'type' => 'text',
                'validation' => 'numeric',
                'channel_based' => false,
                'locale_based' => false
            ],  [
                'name' => 'gstin',
                'title' => 'pos::app.admin.system.bill-receipt.gstin-no',
                'type' => 'text',
                'validation' => 'numeric',
                'channel_based' => false,
                'locale_based' => false
            ],  [
                'name' => 'show_barcode',
                'title' => 'pos::app.admin.system.bill-receipt.show-barcode',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'pos::app.admin.system.general.active',
                        'value' => true
                    ], [
                        'title' => 'pos::app.admin.system.general.inactive',
                        'value' => false
                    ]
                ]
            ],  [
                'name' => 'bill_footer_note',
                'title' => 'pos::app.admin.system.bill-receipt.footer-custom-note',
                'type' => 'textarea',
                'validation' => 'max:200',
                'channel_based' => true,
                'locale_based' => true
            ],
        ]
    ]
];