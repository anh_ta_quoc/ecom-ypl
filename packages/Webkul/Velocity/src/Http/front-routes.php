<?php

Route::group(['middleware' => ['web', 'locale', 'theme', 'currency']], function () {
    Route::namespace('Webkul\Velocity\Http\Controllers\Shop')->group(function () {
        Route::get('/product-details/{slug}', 'ShopController@fetchProductDetails')
            ->name('velocity.shop.product');

        Route::get('/categorysearch', 'ShopController@search')
            ->name('velocity.search.index')
            ->defaults('_config', [
                'view' => 'shop::search.search'
            ]);
        Route::get('work_order/confirm/{id}', 'WorkOrderController@confirm')
            ->defaults('_config', [
                'view' => 'velocity::shop.work_order.confirm'
            ])
            ->name('work_order.confirm');
        Route::get('work_order/result/{id}', 'WorkOrderController@result')
            ->defaults('_config', [
                'view' => 'velocity::shop.work_order.result'
            ])
            ->name('work_order.result');
        Route::put('work_order/accept/{id}', 'WorkOrderController@accept')
            ->defaults('_config', [
                'redirect' => 'work_order.confirm'])
            ->name('work_order.accept');
        Route::put('work_order/reject/{id}', 'WorkOrderController@reject')
            ->defaults('_config', [
                'redirect' => 'work_order.confirm'])
            ->name('work_order.reject');
        Route::post('work_order/storeByCustomer', 'WorkOrderController@storeByCustomer')
            ->defaults('_config', [
                'redirect' => 'shop.cms.page'])
            ->name('work_order.storeByCustomer');

        Route::get('/categories', 'ShopController@fetchCategories')
        ->name('velocity.categoriest');

        Route::get('/category-details', 'ShopController@categoryDetails')
            ->name('velocity.category.details');

        Route::get('/fancy-category-details/{slug}', 'ShopController@fetchFancyCategoryDetails')->name('velocity.fancy.category.details');

        Route::get('/mini-cart', 'CartController@getMiniCartDetails')
            ->name('velocity.cart.get.details');

        Route::post('/cart/add', 'CartController@addProductToCart')
            ->name('velocity.cart.add.product');

        Route::delete('/cart/remove/{id}', 'CartController@removeProductFromCart')
            ->name('velocity.cart.remove.product');

        Route::get('/comparison', 'ComparisonController@getComparisonList')
            ->name('velocity.product.compare')
            ->defaults('_config', [
                'view' => 'shop::guest.compare.index'
            ]);

        Route::group(['middleware' => ['customer']], function () {
            Route::get('/customer/account/comparison', 'ComparisonController@getComparisonList')
                ->name('velocity.customer.product.compare')
                ->defaults('_config', [
                    'view' => 'shop::customers.account.compare.index'
                ]);
        });

        Route::put('/comparison', 'ComparisonController@addCompareProduct')
            ->name('customer.product.add.compare');

        Route::delete('/comparison', 'ComparisonController@deleteComparisonProduct')
            ->name('customer.product.delete.compare');

        Route::get('/guest-wishlist', 'ShopController@getWishlistList')
            ->name('velocity.product.guest-wishlist')
            ->defaults('_config', [
                'view' => 'shop::guest.wishlist.index'
            ]);

        Route::get('/items-count', 'ShopController@getItemsCount')
            ->name('velocity.product.details');

        Route::get('/detailed-products', 'ShopController@getDetailedProducts')
            ->name('velocity.product.details');

        Route::get('/category-products/{categoryId}', 'ShopController@getCategoryProducts')
            ->name('velocity.category.products');
    });
});