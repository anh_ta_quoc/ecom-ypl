<?php

namespace Webkul\Velocity\Http\Controllers\Shop;

use Mail;

use YPL\Repair\Repositories\WorkOrderRepository;
use YPL\Repair\Repositories\ItemRepairRepository;
use YPL\Repair\Repositories\WorkOrderItemRepository;
use YPL\Repair\Repositories\ItemRepository;
use YPL\Repair\Models\WorkOrder;
use YPL\Repair\Mail\NewWorkOrderNotification;
use Excel;


class WorkOrderController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $workOrderRepository;
    protected $itemRepository;
    protected $itemRepairRepository;
    protected $workOrderItemRepository;


    public function __construct(
        WorkOrderRepository $workOrderRepository,
        ItemRepository $itemRepository,
        ItemRepairRepository $itemRepairRepository,
        WorkOrderItemRepository $workOrderItemRepository

    )
    {
        $this->_config = request('_config');

        $this->workOrderRepository = $workOrderRepository;
        $this->itemRepairRepository = $itemRepairRepository;
        $this->itemRepository = $itemRepository;
        $this->workOrderItemRepository = $workOrderItemRepository;


    }


    public function confirm($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        return view($this->_config['view'], compact('work_order'));
    }

    public function result($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        return view($this->_config['view'], compact('work_order'));
    }

    public function reject($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $work_order->status = WorkOrder::STATUS_CANCELED_OTHER;
        $work_order->save();

        session()->flash('success', 'Rejected To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function accept($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $work_order->status = WorkOrder::STATUS_ESTIMATED_ACCEPTED;
        $work_order->save();

        session()->flash('success', 'Accept To Repair For Work Order # ' . $work_order->id . ' success ');
        return redirect()->route($this->_config['redirect'], $work_order->id);

    }

    public function storeByCustomer()
    {
        $input = request()->all();

        if (request()->has('item_id')) {
            $item_id = $input['item_id'];

            $item = $this->itemRepository->findOrFail($item_id);
            $input['product_model'] = $item->name;
            $input['product_name'] = $item->name;
            $input['product_brand'] = $item->item_category->name;
        } else {

        }
        $input['type'] = 'shop';
        $input['shop_id'] = 1;
        $input['job_status'] = WorkOrder::JOB_STATUS_OPENED;
        $input['status'] = WorkOrder::STATUS_SUBMITTED;
        $work_order = $this->workOrderRepository->create($input);
        if (isset($input['item_repair_ids']) && $input['item_repair_ids'] != '') {
            $item_repair_ids = $input['item_repair_ids'];
            foreach (explode(",", $item_repair_ids) as $item_repair_id) {
                $item_repair = $this->itemRepairRepository->findOrFail($item_repair_id);
                $work_order_item = [];
                $work_order_item['work_order_id'] = $work_order->id;
                $work_order_item['item_repair_id'] = $item_repair->id;
                $work_order_item['item_repair_price'] = $item_repair->price;
                $work_order_item['item_id'] = $item_id;

                $this->workOrderItemRepository->create($work_order_item);
            }
        }
        try {
            Mail::queue(new NewWorkOrderNotification($work_order));
        } catch (\Exception $e) {
            report($e);
        }
        session()->flash('success', 'Request To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route('work_order.result', $work_order->id);

    }


}