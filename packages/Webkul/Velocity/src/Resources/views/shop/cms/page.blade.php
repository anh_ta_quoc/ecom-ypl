@extends('shop::layouts.master')

@section('page_title')
    {{ $page->page_title }}
@endsection

@section('head')
    @isset($page->meta_title)
        <meta name="title" content="{{ $page->meta_title }}"/>
    @endisset

    @isset($page->meta_description)
        <meta name="description" content="{{ $page->meta_description }}"/>
    @endisset

    @isset($page->meta_keywords)
        <meta name="keywords" content="{{ $page->meta_keywords }}"/>
    @endisset
@endsection

@section('content-wrapper')
    <div class="cms-page-container cart-details row">
        {!! DbView::make($page)->field('html_content')->render() !!}
    </div>
@endsection

@push('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js" integrity="sha512-gY25nC63ddE0LcLPhxUJGFxa2GoIyA5FLym4UJqHDEMHjp8RET6Zn/SHo1sltt3WuVtqfyxECP38/daUc/WVEA==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $("owl-carousel").owlCarousel();
        });
    </script>
@endpush

@push('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css"/>
    <style>
        .contact_page .icon[class*=fa-]:before,
        footer ul a:before {
            font-family: FontAwesome
        }

        .contact_page .icon {
            padding-left: 94px;
        }

        .contact_page .icon {
            position: relative;
            padding-left: 70px;
            min-height: 55px;
            display: block;
        }

        .fa {
            line-height: inherit;
            font-family: inherit;
        }

        .contact_page .icon {
            position: relative;
            padding-left: 70px; /*! min-height:55px; */
            display: block
        }

        .contact_page .icon:before {
            position: absolute;
            left: 0;
            top: 0;
            width: 55px;
            height: 55px;
            line-height: 55px;
            color: #E6482F;
            text-align: center;
            font-size: 40px
        }

        .contact_page .icon + .icon {
            margin-top: 20px
        }
        .contact-form {
            border-left: 7px dotted red;
            padding: 4px;
            margin: 12px auto;
        }
        .contact_page .panel .col-sm-4{text-align:left}
        .contact_page .form-horizontal .form-group label {
            font-size: 14px;
            text-transform: uppercase;
            margin-bottom: 15px;
            padding-left:20px
        }

    </style>
    <style>
        #content, .top-boxed, .top-fullwidth {
            padding-bottom: 30px;
        }
        .header-about {top: 0;position: absolute;width: 100%;}
        .header-text {margin-left: 45px;width: 55%;font: Bold 56px/66px Roboto;position: relative;color: white;top: -65px;}
        .smart-phone-h {padding-bottom: 20px;border-bottom: 10px solid orange;}
        .first-para {text-align: justify;font: 16px/22px Roboto;padding: 120px 0px 0px 20px;}
        a.brand-links {color: #E6482F !important;text-decoration: underline;}
        .sp-sections {padding: 47px;}
        .old-sellers {background: linear-gradient(90deg, rgb(197, 33, 40) 0%, rgb(230, 72, 47) 100%);color:white}
        .oldseller-inner-div {text-align: left;font: 32px/35px Roboto;}
        h2.know-us {font: Bold 36px/30px Roboto;}
        .certificate-img {margin-top: 20px;}
        .video-div {padding: 0px;margin-top: 30px;}
        .customer-centric-heading {font: Bold 36px/30px Roboto;}
        .cs-heading {font: Bold 19px/22px Roboto;}
        .cs-para {font: 16px/22px Roboto;text-align: justify;}
        .customer-row {background: linear-gradient(90deg, rgb(197, 33, 40) 0%, rgb(230, 72, 47) 100%);color:white}
        .pos-up {top: -115px;}
        .tech-expert-div {background: #191919;}
        p.tech-expert {font: 100 32px/35px Roboto;color: #FDA228;}
        .space {padding: 65px 35px 100px;}
        img.checklist-img {margin-bottom: 30px;}
        p.variety-para {text-align: justify;font: 25px/30px Roboto;margin-bottom: 35px;}
        p.finest-selection {font: Bold 19px/22px Roboto;}
        p.variety-inner-para {text-align: justify;font: 16px/22px Roboto;}
        .mobile-about-us {margin-top: 30px;}
        .variety-img-div{padding:0px;}
        .p-sourcing-row {background: #E8E8E8;}
        @media only screen and (max-width: 767px) {
            .header-text {margin-left: 20px;width: 90%;font: Bold 22px/24px Roboto;top: -90px;}
            .smart-phone-h {padding-bottom: 5px;border-bottom: 5px solid orange;}
            .oldseller-inner-div {font: 16px/24px Roboto;}
            .sp-sections {padding: 25px;}
            .certificate-img {margin-top: 10px;width: 50px;}
            .first-para {padding:0px;}
            h2.know-us {font: Bold 24px/30px Roboto;}
            .customer-centric-heading {font: Bold 24px/30px Roboto;}
            img.checklist-img {width: 50px;margin-bottom: 24px;}
            p.tech-expert {font: 100 16px/24px Roboto;}
            .pos-up {top: 0px;}
            p.variety-para {font: 18px/24px Roboto;margin-bottom: 15px;}
            .space {padding: 30px 15px 30px;}
            .mobile-spacing {padding:0px;}
        }
        .img-fx {
            height: 40px;
            width: 40px;
        }
    </style>
@endpush