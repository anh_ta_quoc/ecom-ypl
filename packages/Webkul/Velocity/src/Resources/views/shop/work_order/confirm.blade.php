@extends('shop::layouts.master')

@section('page_title')
    Need to be confirmed ??
@endsection


@section('content-wrapper')
    <div class="container page-content page-confirm mb-5 mt-5">

        <div class="row">

        <!-- <div name="{{ __('admin::app.sales.orders.info') }}" :selected="true">
                <div class="sale-container"> -->

            <div class="col-md-12">
                <h3>CUSTOMER</h3>
            </div>


            <div class="col-md-4">
                <div class="secton-title">
                    <span>Product Info</span>
                </div>

                <table class="boder table-confirm">
                    <tr>
                        <td class="title">Product Name</td>
                        <td class="value">{{ $work_order->product_name }}</td>
                    </tr>
                    <tr>
                        <td class="title">Product Brand</td>
                        <td class="value">{{ $work_order->product_imei }}</td>
                    </tr>
                    <tr>
                        <td class="title">Product IMEI</td>
                        <td class="value">{{ $work_order->product_imei }}</td>
                    </tr>
                    <tr>
                        <td class="title">Product Status</td>
                        <td class="value">{{ $work_order->product_status }}</td>
                    </tr>
                    <tr>
                        <td class="title">Note</td>
                        <td class="value">{{ $work_order->note }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">

                <div class="secton-title">
                    <span>Customer Info</span>
                </div>

                <table class="boder table-confirm">
                    <tr>
                        <td class="title">Customer Name</td>
                        <td class="value">{{ $work_order->customer_name }}</td>
                    </tr>
                    <tr>
                        <td class="title">Customer Phone</td>
                        <td class="value">{{ $work_order->customer_phone }}</td>
                    </tr>
                    <tr>
                        <td class="title">Customer Email</td>
                        <td class="value">{{ $work_order->customer_email }}</td>
                    </tr>
                    <tr>
                        <td class="title">Customer Adress</td>
                        <td class="value">{{ $work_order->customer_address }}</td>
                    </tr>
                </table>

            </div>
            <div class="col-md-4">
                @if (count($work_order->item_repairs) >0)
                    <div class="secton-title">
                        <span>Item Repair</span>
                    </div>
                    <table class="border table-confirm">
                        <thead>
                        <tr>
                            <th>Issue</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($work_order->item_repairs as $item)
                            <tr>
                                <td class="title">{{$item->name}}</td>
                                <td class="value">{{$item->price}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><b>Total</b></td>
                            <td>{{$work_order->item_repairs->sum('price')}}</td>
                        </tr>

                        </tbody>
                    </table>
                @endif

            </div>
            <div class="col-md-4">
                @if ($work_order->est_date!=null)
                    <div class="secton-title">
                        <span>Quotation</span>
                    </div>
                    <table class="boder table-confirm">
                        <tr>
                            <td class="title">Estimate Date</td>
                            <td class="value">{{ $work_order->est_date }}</td>
                        </tr>
                        <tr>
                            <td class="title">Estimate Price</td>
                            <td class="value">{{ $work_order->est_price }}</td>
                        </tr>
                        <tr>
                            <td class="title">Estimate Plan</td>
                            <td class="value">{{ $work_order->est_plan }}</td>
                        </tr>
                    </table>
                @endif

            </div>

            <div class="col-md-12">
                <div class="row d-flex justify-content-center">
                    @if($work_order->status == \YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_QUOTED)
                        <div class="col-md-3 text-center justify mt-5">
                            <form action="{{route('work_order.reject',$work_order->id)}}" method="POST">
                                @csrf()
                                <input name="_method" type="hidden" value="PUT">
                                <button type="submit" class="btn btn-danger">Reject</button>
                            </form>
                            <form action="{{route('work_order.accept',$work_order->id)}}" method="POST">
                                @csrf()
                                <input name="_method" type="hidden" value="PUT">
                                <button type="submit" class="btn btn-primary">Accept</button>
                            </form>
                        </div>
                    @endif
                </div>
            </div>


        </div>

    </div>
@endsection