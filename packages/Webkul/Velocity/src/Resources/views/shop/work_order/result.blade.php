@extends('shop::layouts.master')

@section('page_title')
    Need to be confirmed ??
@endsection


@section('content-wrapper')
    <div class="container page-content page-confirm mb-5 mt-5">

        <div class="row">


            <div class="col-md-12 mb-5 text-center">
                <h3>We have received your work order and will process shortly
                </h3>
            </div>

            <div class="col-md-5">
                <div class="total-repair">
                    <h2 class="title-h2 text-red">Work Order Information</h2>
                    <table class="table-result-1 mb-3">
                        <tr>
                            <td class="title">ID</td>
                            <td class="value">{{ $work_order->id }}</td>
                        </tr>
                        <tr>
                            <td class="title">Submitted Date</td>
                            <td class="value">{{ $work_order->created_at->format('D m Y') }}</td>
                        </tr>
                        <tr>
                            <td class="title">Status</td>
                            <td class="value">{{ $work_order->status }}</td>
                        </tr>
                        <tr>
                            <td class="title">Note</td>
                            <td class="value">{{ $work_order->note }}</td>
                        </tr>
                    </table>
                    <h2 class="title-h2 text-red">Repair Services</h2>
                    <table class="table-result">
                        <thead>
                        <tr>
                            <th class="text-left">Services</th>
                            <th class="text-right">Estimated cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($work_order->item_repairs as $item)
                            <tr>
                                <td class="title">{{$item->name}}</td>
                                <td class="value">{{$item->price}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><b>Total</b></td>
                            <td>{{$work_order->item_repairs->sum('price')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-7 modal-repair-2">
                <h2 class="title-h2">Device Information</h2> 
                <table class="boder table-result-2">
                    <tr>
                        <td class="title"><b>Brand</b></td>
                        <td class="value">{{ $work_order->product_brand }}</td>
                    </tr>
                    <tr>
                        <td class="title"><b>Model</b></td>
                        <td class="value">{{ $work_order->product_name }}</td>
                    </tr>
                    <tr>
                        <td class="title"><b>IMEI / Serial Number</b></td>
                        <td class="value">{{ $work_order->product_imei }}</td>
                    </tr>
                    <tr>
                        <td class="title"><b>Condition</b></td>
                        <td class="value">{{ $work_order->product_status }}</td>
                    </tr>

                </table>
                <h2 class="title-h2 mt-5">Customer Information</h2>
                <table class="boder  table-result-2">
                    <tr>
                        <td class="title"><b>Name</b></td>
                        <td class="value">{{ $work_order->customer_name }}</td>
                    </tr>
                    <tr>
                        <td class="title"><b>Phone</b></td>
                        <td class="value">{{ $work_order->customer_phone }}</td>
                    </tr>
                    <tr>
                        <td class="title"><b>Email</b></td>
                        <td class="value">{{ $work_order->customer_email }}</td>
                    </tr>
                    <tr>
                        <td class="title"><b>Pick up / Return address</b></td>
                        <td class="value">{{ $work_order->customer_address }}</td>
                    </tr>
                </table> 
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 mt-5 text-center">
                <h3>If you have any inquiry please contact us</h3>
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-6 result-info">
                <div class="secton-title">
                    <span>Customer Service</span>
                </div>
                <table class="boder table-confirm">
                    <tr>
                        <td class="title">Email:</td>
                        <td class="value">admin@jtechstore.com.au</td>
                    </tr>
                    <tr>
                        <td class="title">Hotline</td>
                        <td class="value">03 9043 0791</td>
                    </tr>
                </table>
                <br/>
                <div class="row d-flex justify-content-center">
                    <a href="{{route('shop.home.index')}}" class="btn btn-primary">BACK TO HOMEPAGE</a>
                </div>
            </div>
            <div class="col-md-3">
            </div>

        </div>

    </div>
@endsection