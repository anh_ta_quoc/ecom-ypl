<?php

namespace YPL\Repair\DataGrids;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;
use YPL\Repair\Models\WorkOrder;

class WorkOrderDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('work_orders')
            ->select('id', 'customer_name', 'customer_phone', 'customer_email',
                'product_brand', 'product_model', 'product_name', 'product_imei', 'note','status', 'job_status', 'invoice_status', 'urgent_status', 'created_at');
        if (auth()->guard('admin')->user()->role_id != 1) {
            if (auth()->guard('admin')->user()->type == 'shop') {
                $queryBuilder = $queryBuilder->where('type', 'shop')
                    ->where('shop_id', auth()->guard('admin')->user()->shop_id);
            }
            if (auth()->guard('admin')->user()->type == 'insurance') {
                $queryBuilder = $queryBuilder->where('type', 'insurance')
                    ->where('insurance_id', auth()->guard('admin')->user()->insurance_id);
            }
            if (auth()->guard('admin')->user()->type == 'partner') {
                $queryBuilder = $queryBuilder->where('type', 'partner')
                    ->where('partner_id', auth()->guard('admin')->user()->partner_id);
            }
            if (auth()->guard('admin')->user()->type == 'repair_shop') {
                $queryBuilder = $queryBuilder->where('repair_type', 'Partner')
                    ->where('repair_shop_id', auth()->guard('admin')->user()->repair_shop_id);
            }

//         $this->addFilter('content_id', 'con.id');


        }
        if (request('status') != null) {
            $queryBuilder = $queryBuilder->where('status', request('status'));
        }
        if (request('duration') != null) {
            $queryBuilder = $queryBuilder->whereDate('created_at','=', Carbon::now()->subDays(request('duration')));
        }
        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index' => 'id',
            'label' => 'Work Order ID',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index' => 'job_status',
            'label' => 'WO Status',
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->job_status == WorkOrder::JOB_STATUS_OPENED) {
                    return '<span class="badge badge-md badge-info">' . $value->job_status . '</span>';
                } elseif ($value->job_status == WorkOrder::JOB_STATUS_CLOSED) {
                    return '<span class="badge badge-md badge-success">' . $value->job_status . '</span>';
                } elseif ($value->job_status == WorkOrder::JOB_STATUS_CANCELLED) {
                    return '<span class="badge badge-md badge-danger">' . $value->job_status . '</span>';
                }
            },
        ]);
        $this->addColumn([
            'index' => 'urgent_status',
            'label' => 'Urgent Status',
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->urgent_status) {
                    return '<span class="badge badge-md badge-danger">Urgent</span>';
                } else {
                    return '';
                }
            },
        ]);
        $this->addColumn([
            'index' => 'duration',
            'label' => 'Duration',
            'type' => 'int',
            'sortable' => true,
            'searchable' => false,
            'closure' => true,
            'wrapper' => function ($value) {
                $duration = Carbon::parse($value->created_at)->diffInDays(Carbon::now());
                if ($duration >= 10) {
                    return '<span class="badge badge-md badge-danger">'.$duration.'</span>';
                } elseif (7<= $duration && $duration <10) {
                    return '<span class="badge badge-md badge-warning">'.$duration.'</span>';
                } elseif (4<= $duration && $duration <7) {
                    return '<span class="badge badge-md badge-success">'.$duration.'</span>';
                } else {
                    return '<span class="">'.$duration.'</span>';
                }
            },
        ]);
        $this->addColumn([
            'index' => 'customer_name',
            'label' => 'Customer Info',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                return '<span class=""><b>Name:  </b>' . $value->customer_name . '</span><br/>' .
                    '<span class=""><b>Email:  </b>' . $value->customer_email . '</span><br/>' .
                    '<span class=""><b>Phone:  </b>' . $value->customer_phone . '</span><br/>';
            },
        ]);
        $this->addColumn([
            'index' => 'customer_phone',
            'label' => '',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                return '';
            },
        ]);
        $this->addColumn([
            'index' => 'customer_email',
            'label' => '',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                return '';
            },
        ]);
        $this->addColumn([
            'index' => 'product_name',
            'label' => 'Product Info',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                return '<span class=""><b>Brand:  </b>' . $value->product_brand . '</span><br/>' .
                    '<span class=""><b>Name:  </b>' . $value->product_name . '</span><br/>' .
                    '<span class=""><b>Note:  </b>' . $value->note . '</span><br/>';
            },
        ]);
        $this->addColumn([
            'index' => 'product_imei',
            'label' => '',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                return '';
            },
        ]);


        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Submitted date',
            'type' => 'datetime',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index' => 'status',
            'label' => 'Job Status',
            'type' => 'string',
            'sortable' => true,
            'searchable' => false,
            'filterable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->status == WorkOrder::STATUS_SUBMITTED) {
                    return '<span class="badge badge-md badge-info">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_POSTAGE_INSTRUCTIONS_ISSUED) {
                    return '<span class="badge badge-md badge-info">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_ALLOCATED || $value->status == WorkOrder::STATUS_DELIVERED_TO_WAREHOUSE) {
                    return '<span class="badge badge-md badge-success">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_ESTIMATED_REQUIRED || $value->status == WorkOrder::STATUS_ESTIMATED_QUOTED) {
                    return '<span class="badge badge-md badge-warning">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_CANCELED_OTHER) {
                    return '<span class="badge badge-md badge-danger">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_UNDER_REPAIR) {
                    return '<span class="badge badge-md badge-info">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_ESTIMATED_ACCEPTED || $value->status == WorkOrder::STATUS_UNDER_REPAIR || $value->status == WorkOrder::STATUS_TECHNICIAN_COMPLETED) {
                    return '<span class="badge badge-md badge-success">' . $value->status . '</span>';
                } elseif ($value->status == WorkOrder::STATUS_PART_ORDERED || $value->status == WorkOrder::STATUS_PART_REQUEST || $value->status == WorkOrder::STATUS_PART_IN_STOCK) {
                    return '<span class="badge badge-md badge-success">' . $value->status . '</span>';
                } else {
                    return '<span class="badge badge-md badge-info">' . $value->status . '</span>';

                }
            },

        ]);
        $this->addColumn([
            'index' => 'invoice_status',
            'label' => 'Invoice status',
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index' => 'status',
            'label' => '',
            'type' => 'string',
            'searchable' => false,
            'closure' => true,
            'wrapper' => function ($value) {
                return '<div class="action pull-right">
                    <a href="' . route('admin.work_order.print', $value->id) . '" data-method="GET" data-action="' . route('admin.work_order.print', $value->id) . '"  target="_blank" title="Print"><span class="icon print-icon"></span></a></div>';
            },
        ]);


    }


    public function prepareActions()
    {
        $this->addAction([
            'title' => 'View',
            'method' => 'GET',
            'route' => 'admin.work_order.view',
            'icon' => 'icon eye-icon',
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'POST',
            'route' => 'admin.work_order.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'WorkOrder']),
            'icon' => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
