<?php

namespace YPL\Repair\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class PartnerDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('partners')
            ->select('id', 'name', 'location','phone','type','noti_email');

//         $this->addFilter('content_id', 'con.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index'      => 'name',
            'label'      => ' Name',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'type',
            'label'      => ' Type',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'location',
            'label'      => 'Location',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'phone',
            'label'      => 'Phone',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'noti_email',
            'label'      => 'email',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

    

    }

    public function prepareActions() {

        $this->addAction([
            'type'   => 'Edit',
            'method' => 'GET',
            'route'  => 'admin.partner.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'type'         => 'Delete',
            'method'       => 'POST',
            'route'        => 'admin.partner.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Partner']),
            'icon'         => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
