<?php

namespace YPL\Repair\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class ItemRepairDataGrid extends DataGrid
{

    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('item_repairs')
            ->select('id', 'name', 'price')
            ->where('item_id', request('item_id'));

//         $this->addFilter('content_id', 'con.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index'      => 'name',
            'label'      => ' Name',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'price',
            'label'      => 'Price',
            'type'       => 'number',
            'sortable'   => true,
            'searchable' => true,
            'filterable' => true,
            'closure'    => true,

        ]);


    }

    public function prepareActions() {
        $this->addAction([
            'type'   => 'Edit',
            'method' => 'GET',
            'route'  => 'admin.item_repair.edit',
            'icon'   => 'icon pencil-lg-icon',

        ]);

        $this->addAction([
            'type'         => 'Delete',
            'method'       => 'POST',
            'route'        => 'admin.item_repair.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'ItemRepair']),
            'icon'         => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
