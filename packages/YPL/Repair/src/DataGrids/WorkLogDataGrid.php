<?php

namespace YPL\Repair\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class WorkLogDataGrid extends DataGrid
{

    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('work_logs as wl')
            ->leftJoin('admins', 'wl.user_id', '=', 'admins.id')
            ->addSelect('wl.id as id', 'admins.email as email', 'wl.note as note', 'wl.created_at as created_at',
                        'wl.old_status as old_status','wl.new_status as new_status' );
        if (request('work_order_id')) {
            $queryBuilder->where('work_order_id', request('work_order_id'));
        }
          if (request('id')) {
              $queryBuilder->where('work_order_id', request('id'));
          }


        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'email',
            'label'      => 'Staff Email',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'old_status',
            'label'      => ' Old Status',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'new_status',
            'label'      => ' New Status',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'note',
            'label'      => ' Note',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'created_at',
            'label'      => ' Created At',
            'type'       => 'date',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);




    }

    public function prepareActions() {
        $this->addAction([
            'type'   => 'Edit',
            'method' => 'GET',
            'route'  => 'admin.work_log.edit',
            'icon'   => 'icon pencil-lg-icon',
           
        ]);

        $this->addAction([
            'type'         => 'Delete',
            'method'       => 'POST',
            'route'        => 'admin.work_log.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'WorkLog']),
            'icon'         => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
