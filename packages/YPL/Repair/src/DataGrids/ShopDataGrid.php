<?php

namespace YPL\Repair\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class ShopDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('shops')
            ->select('id', 'name', 'location');

//         $this->addFilter('content_id', 'con.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index'      => 'name',
            'label'      => ' Name',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'location',
            'label'      => 'Location',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

    

    }

    public function prepareActions() {
        $this->addAction([
            'type'   => 'Edit',
            'method' => 'GET',
            'route'  => 'admin.shop.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'type'         => 'Delete',
            'method'       => 'POST',
            'route'        => 'admin.shop.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Shop']),
            'icon'         => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
