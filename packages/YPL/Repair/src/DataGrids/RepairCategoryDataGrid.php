<?php

namespace YPL\Repair\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class RepairCategoryDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('repair_categories as v_cat')
            ->select('v_cat.id as id', 'ct.name', 'v_cat.status')
            ->leftJoin('repair_category_translations as ct', function($leftJoin) {
                $leftJoin->on('v_cat.id', '=', 'ct.repair_category_id')
                    ->where('ct.locale', app()->getLocale());
            })
            ->groupBy('v_cat.id');

        // $this->addFilter('content_id', 'con.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
//        $this->addColumn([
//            'index'      => 'category_id',
//            'label'      => trans('velocity::app.admin.category.datagrid.category-id'),
//            'type'       => 'number',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);

        $this->addColumn([
            'index'      => 'name',
            'label'      => trans('velocity::app.admin.category.datagrid.category-name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

//        $this->addColumn([
//            'index'      => 'icon',
//            'label'      => trans('velocity::app.admin.category.datagrid.category-icon'),
//            'type'       => 'string',
//            'sortable'   => true,
//            'searchable' => true,
//            'filterable' => true,
//            'closure'    => true,
//            'wrapper'    => function ($row) {
//                return '<span class="wk-icon '.$row->icon.'"></span>';
//            },
//        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => trans('velocity::app.admin.category.datagrid.category-status'),
            'type'       => 'string',
            'sortable'   => true,
            'searchable' => true,
            'filterable' => true,
            'closure'    => true,
            'wrapper'    => function($row) {
                if ( $row->status ) {
                    return '<span class="badge badge-md badge-success">Enabled</span>';
                } else {
                    return '<span class="badge badge-md badge-danger">Disabled</span>';
                }
            },
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type'   => 'Edit',
            'method' => 'GET',
            'route'  => 'admin.repair_category.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'type'         => 'Delete',
            'method'       => 'POST',
            'route'        => 'admin.repair_category.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'RepairCategory']),
            'icon'         => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
