<?php

namespace YPL\Repair\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class PolicyDataGrid extends DataGrid
{

    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {

        $queryBuilder = DB::table('policies')
            ->select('id', 'name','is_required');

//         $this->addFilter('content_id', 'con.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {

        $this->addColumn([
            'index'      => 'name',
            'label'      => ' Name',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
        $this->addColumn([
            'index'      => 'is_required',
            'label'      => 'Is Required',
            'type'       => 'string',
            'sortable'   => true,
            'searchable' => true,
            'filterable' => true,
            'closure'    => true,
            'wrapper'    => function($row) {
                if ( $row->is_required ) {
                    return '<span class="badge badge-md badge-success">Yes</span>';
                } else {
                    return '<span class="badge badge-md badge-danger">No</span>';
                }
            },
        ]);


    }

    public function prepareActions() {
        $this->addAction([
            'type'   => 'Edit',
            'method' => 'GET',
            'route'  => 'admin.policy.edit',
            'icon'   => 'icon pencil-lg-icon',
            'wrapper'    => function($row) {
                if ( $row->is_required ) {
                    return '<span class="badge badge-md badge-success">Yes</span>';
                } else {
                    return '<span class="badge badge-md badge-danger">No</span>';
                }
            },
        ]);

        $this->addAction([
            'type'         => 'Delete',
            'method'       => 'POST',
            'route'        => 'admin.policy.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Policy']),
            'icon'         => 'icon trash-icon',
        ]);
    }

//    public function prepareMassActions()
//    {
//        $this->addMassAction([
//            'type'   => 'delete',
//            'action' => route('velocity.admin.category.mass-delete'),
//            'label'  => trans('admin::app.datagrid.delete'),
//            'method' => 'DELETE',
//        ]);
//    }
}
