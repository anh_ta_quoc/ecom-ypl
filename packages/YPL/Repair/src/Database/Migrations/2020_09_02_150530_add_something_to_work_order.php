<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomethingToWorkOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_orders', function (Blueprint $table) {
            $table->string('invoice_status')->nullable(true);
            $table->string('urgent_status')->nullable(true);
            $table->string('storage_location')->nullable(true);
            $table->string('repair_location')->nullable(true);
            $table->integer('group_policy_id')->nullable(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_orders', function (Blueprint $table) {
            $table->dropColumn('invoice_status');
            $table->dropColumn('urgent_status');
            $table->dropColumn('storage_location');
            $table->dropColumn('repair_location');
            $table->dropColumn('group_policy_id');

        });
    }
}
