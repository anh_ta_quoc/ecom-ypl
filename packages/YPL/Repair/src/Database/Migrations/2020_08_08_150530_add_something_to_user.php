<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomethingToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->string('type')->nullable(true);
            $table->integer('shop_id')->nullable(true);
            $table->integer('partner_id')->nullable(true);
            $table->integer('insurance_id')->nullable(true);
            $table->integer('repair_shop_id')->nullable(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('shop_id');
            $table->dropColumn('partner_id');
            $table->dropColumn('insurance_id');
            $table->dropColumn('repair_shop_id');


        });
    }
}
