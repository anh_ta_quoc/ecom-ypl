<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('tracking_number')->nullalble();
            $table->integer('created_by')->nullable();
            $table->integer('received_by')->nullable();


            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();;
            $table->string('customer_phone')->nullable();;
            $table->string('customer_address')->nullable();
            $table->integer('customer_id')->nullable();

            $table->string('product_imei');
            $table->string('product_brand');
            $table->string('product_model');
            $table->string('product_name');
            $table->string('product_status')->nullable();


            $table->string('job_status');
            $table->string('status');
            $table->string('type');

            $table->dateTime('est_date')->nullable();
            $table->text('est_plan')->nullable();
            $table->float('est_price',12,2)->nullable();

            $table->float('budget',12,2)->nullable();
            $table->integer('partner_id')->nullable();
            $table->integer('shop_id')->nullable();
            $table->integer('insurance_id')->nullable();

            $table->integer('repair_shop_id')->nullable();
            $table->string('repair_type')->nullable();
            $table->integer('repair_person_id')->nullable();



            $table->text('policies')->nullable();
            $table->text('checklists')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
