<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBorrowToWorkOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_orders', function (Blueprint $table) {
            $table->string('borrow_imei')->nullable(true);
            $table->string('borrow_model')->nullable(true);
            $table->float('borrow_price')->nullable(true);
            $table->text('borrow_note')->nullable(true);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_orders', function (Blueprint $table) {
            $table->dropColumn('borrow_imei');
            $table->dropColumn('borrow_model');
            $table->dropColumn('borrow_price');
            $table->dropColumn('borrow_note');



        });
    }
}
