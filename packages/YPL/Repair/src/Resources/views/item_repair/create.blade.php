@extends('repair::layouts.content')

@section('page_title')
    Add Item Repair
@stop


@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.item_repair.store',$item_id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Add Item Repair for : {{$item->name}}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()


                    <accordian :title="'General'" :active="true">
                        <div slot="body">

                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') }}" data-vv-as="&quot;Name&quot;">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>


                            <div class="control-group" :class="[errors.has('price') ? 'has-error' : '']">
                                <label for="price" class="required">Price</label>
                                <input type="number" step="0.01" class="control" name="price" v-validate="'required'" value="{{ old('price') }}" data-vv-as="&quot;Price&quot;">

                                <span class="control-error" v-if="errors.has('price')">@{{ errors.first('price') }}</span>
                            </div>


                        </div>
                    </accordian>



                </div>
            </div>

        </form>
    </div>

@stop
