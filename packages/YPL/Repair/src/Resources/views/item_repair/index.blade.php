@extends('repair::layouts.content')

@section('page_title')
    Item Repair
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <i class="icon angle-left-icon back-link" onclick=" window.location = '{{ url('/admin/item/') }}';"></i>

                <h1>  Item Repair : {{$item->name}}</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.item_repair.create',$item_id) }}" class="btn btn-lg btn-primary">
                    Add Item Repair
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('itemRepairGrid', 'YPL\Repair\DataGrids\ItemRepairDataGrid')

            {!! $itemRepairGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

