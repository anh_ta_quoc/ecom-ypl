@extends('admin::layouts.content')

@section('page_title')
    Edit Shop
@stop
@section('tab')
    <div class="tabs"><ul><li class="active" ><a href="{{route('admin.shop.index')}}">
                    Shop
                </a></li > <li ><a href="{{route('admin.partner.index')}}">
                    Partner
                </a></li></ul></div>
@stop
@section('content')
    <div class="content">
        <form method="POST" action="{{route('admin.shop.update',$shop->id)}}" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Edit Shop
                    </h1>


                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                       Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">


                    <accordian :title="'{{ __('admin::app.catalog.categories.general') }}'" :active="true">
                        <div slot="body">



                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') ?: $shop->name }}" data-vv-as="&quot;Name&quot;">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('location') ? 'has-error' : '']">
                                <label for="location" class="required">Location</label>
                                <input type="text" class="control" name="location" value="{{ old('location') ?: $shop->location }}" data-vv-as="&quot;Location&quot;">
                                <span class="control-error" v-if="errors.has('location')">@{{ errors.first('location') }}</span>
                            </div>
                        </div>
                    </accordian>



                </div>
            </div>

        </form>
    </div>
@stop

