@extends('repair::layouts.content')

@section('page_title')
    Shops
@stop
@section('tab')
    <div class="tabs"><ul><li class="active"><a href="{{route('admin.shop.index')}}">
                    Shop
                </a></li> <li><a href="{{route('admin.partner.index')}}">
                   Partner
                </a></li></ul></div>
@stop
@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>  Shops</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.shop.create') }}" class="btn btn-lg btn-primary">
                    Add Shop
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('shopGrid', 'YPL\Repair\DataGrids\ShopDataGrid')

            {!! $shopGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

