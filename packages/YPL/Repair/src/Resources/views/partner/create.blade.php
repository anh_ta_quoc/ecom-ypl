@extends('repair::layouts.content')

@section('page_title')
    Add Partner
@stop

@section('tab')
    <div class="tabs"><ul><li ><a href="{{route('admin.shop.create')}}">
                    Shop
                </a></li > <li class="active"><a href="{{route('admin.partner.create')}}">
                    Partner
                </a></li></ul></div>
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.partner.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Add Partner
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()




                    <accordian :title="'General'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') }}" data-vv-as="&quot;Name&quot;">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('location') ? 'has-error' : '']">
                                <label for="location" class="required">Location</label>
                                <input type="text" class="control" name="location" value="{{ old('location') }}" data-vv-as="&quot;Location&quot;">
                                <span class="control-error" v-if="errors.has('location')">@{{ errors.first('location') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('type') ? 'has-error' : '']">
                                <label for="type" >Type</label>
                                <select class="control" v-validate="'required'" id="type" name="type" data-vv-as="&quot;Type&quot;">
                                    <option value="{{\YPL\Repair\Models\Partner::TYPE_PARTNER}}">
                                        {{\YPL\Repair\Models\Partner::TYPE_PARTNER}}
                                    </option>
                                    <option value="{{\YPL\Repair\Models\Partner::TYPE_INSURANCE}}">
                                        {{\YPL\Repair\Models\Partner::TYPE_INSURANCE}}
                                    </option>
                                    <option value="{{\YPL\Repair\Models\Partner::TYPE_REPAIR}}">
                                        {{\YPL\Repair\Models\Partner::TYPE_REPAIR}}
                                    </option>
                                </select>
                                <span class="control-error" v-if="errors.has('type')">@{{ errors.first('type') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                <label for="phone" class="required" >Phone</label>
                                <input type="text" class="control" name="phone" value="{{ old('phone') }}" data-vv-as="&quot;Phone&quot;">
                                <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('noti_email') ? 'has-error' : '']">
                                <label for="noti_email" class="required"  >Noti Email</label>
                                <input type="text" type="email"  class="control" name="noti_email" value="{{ old('noti_email') }}" data-vv-as="&quot;Noti Email&quot;">
                                <span class="control-error" v-if="errors.has('noti_email')">@{{ errors.first('noti_email') }}</span>
                            </div>

                        </div>
                    </accordian>



                </div>
            </div>

        </form>
    </div>

@stop
