@extends('repair::layouts.content')

@section('page_title')
    Partners
@stop
@section('tab')
    <div class="tabs"><ul><li ><a href="{{route('admin.shop.index')}}">
                    Shop
                </a></li > <li class="active"><a href="{{route('admin.partner.index')}}">
                    Partner
                </a></li></ul></div>
@stop
@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>  Partners</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.partner.create') }}" class="btn btn-lg btn-primary">
                    Add Partner
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('partnerGrid', 'YPL\Repair\DataGrids\PartnerDataGrid')

            {!! $partnerGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

