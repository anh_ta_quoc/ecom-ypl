@extends('admin::layouts.content')

@section('page_title')
    Edit Product Model
@stop
@section('tab')
    <div class="tabs"><ul><li class="active"><a href="{{route('admin.item.index')}}">
                    Product Model
                </a></li> <li><a href="{{route('admin.item_category.index')}}">
                    Product Brand
                </a></li></ul></div>
@stop
@section('content')
    <div class="content">
        <form method="POST" action="{{route('admin.item.update',$item->id)}}" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Edit Product Model
                    </h1>


                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                       Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">


                    <accordian :title="'{{ __('admin::app.catalog.categories.general') }}'" :active="true">
                        <div slot="body">



                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') ?: $item->name }}" data-vv-as="&quot;Name&quot;">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('item_category_id') ? 'has-error' : '']">
                                <label for="item_category_id" class="item_category_id">Product Brand</label>
                                <select class="control" v-validate="'required'" id="item_category_id" name="item_category_id" data-vv-as="&quot;Is Required&quot;">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($item->item_category_id == $category->id) selected="" @endif>{{$category->name}}</option>
                                    @endforeach

                                </select>
                                <span class="control-error" v-if="errors.has('item_category_id')">@{{ errors.first('item_category_id') }}</span>
                            </div>
                            <div class="control-group {!! $errors->has('feature_image.*') ? 'has-error' : '' !!}">
                                <label>{{ __('admin::app.catalog.categories.image') }}</label>

                                <image-wrapper :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'"
                                               input-name="feature_image" :multiple="false" :images='"{{ $item->image_url }}"' ></image-wrapper>

                                <span class="control-error" v-if="{!! $errors->has('feature_image.*') !!}">
                                    @foreach ($errors->get('feature_image.*') as $key => $message)
                                        @php echo str_replace($key, 'Image', $message[0]); @endphp
                                    @endforeach
                                </span>

                            </div>
                        </div>
                    </accordian>



                </div>
            </div>

        </form>
    </div>
@stop

