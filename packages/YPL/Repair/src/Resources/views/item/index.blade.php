@extends('repair::layouts.content')

@section('page_title')
    Product Model
@stop
@section('tab')
    <div class="tabs"><ul><li class="active"><a href="{{route('admin.item.index')}}">
                    Product Model
                </a></li> <li><a href="{{route('admin.item_category.index')}}">
                    Product Brand
                </a></li></ul></div>
@stop
@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>  Product Model</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.item.create') }}" class="btn btn-lg btn-primary">
                    Add Product Model
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('itemGrid', 'YPL\Repair\DataGrids\ItemDataGrid')

            {!! $itemGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

