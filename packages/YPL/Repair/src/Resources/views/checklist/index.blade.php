@extends('repair::layouts.content')

@section('page_title')
    Checklists
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>  Checklists</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.checklist.create') }}" class="btn btn-lg btn-primary">
                    Add Checklist
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('checklistGrid', 'YPL\Repair\DataGrids\ChecklistDataGrid')

            {!! $checklistGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

