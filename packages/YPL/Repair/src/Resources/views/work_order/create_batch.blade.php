@extends('repair::layouts.content')

@section('page_title')
    Add Work Order
@stop

@section('tab')
    <div class="tabs"><ul><li ><a href="{{route('admin.work_order.create')}}">
                    Single
                </a></li> <li class="active"><a href="{{route('admin.work_order.create_batch')}}">
                    Upload File
                </a></li></ul></div>
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.work_order.store_batch') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Add Work Order
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input type="hidden" name="locale" value="all"/>




                    <accordian :title="'Import File'" :active="true">
                        <div slot="body">
                            <div class="control-group">
                                <label for="import_file" class="required">Download</label>
                                <a href="{{asset('vendor/YPL/repair/assets/file/WorkOrderTemplate.xlsx')}}">Template File</a>
                            </div>

                            <div class="control-group" :class="[errors.has('import_file') ? 'has-error' : '']">
                                <label for="import_file" class="required">File</label>
                                <input type="file" class="control" name="import_file" v-validate="'required'" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"   style="padding-top: 5px;">
                                <span class="control-error" v-if="errors.has('import_file')">@{{ errors.first('import_file') }}</span>
                            </div>



                        </div>
                    </accordian>





                </div>
            </div>

        </form>
    </div>



@stop

