@extends('admin::layouts.content')

@section('page_title')
    Update Work Order
@stop

@section('content')
    <div class="content">
        <?php $locale = request()->get('locale') ?: app()->getLocale(); ?>

        <form method="POST" action="{{route('admin.work_order.update',$work_order->id)}}" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/work_order') }}';"></i>

                        Update Work Order
                    </h1>


                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Update
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <accordian :title="'Urgent Status'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('urgent_status') ? 'has-error' : '']">
                                <label for="urgent_status" class="required">Urgent Status</label>
                                <select class="control"  id="urgent_status" name="urgent_status" >
                                        <option value="1" @if($work_order->urgent_status == 1) selected @endif>
                                            Yes
                                        </option>
                                        <option value="0" @if($work_order->urgent_status == 0) selected @endif >
                                            No
                                        </option>
                                </select>
                            </div>



                        </div>
                    </accordian>

                    <accordian :title="'Group Policy'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('group_policy_id') ? 'has-error' : '']">
                                <label for="is_required" class="required">Group Policy</label>
                                <select class="control"  id="status" name="group_policy_id" >
                                    <option></option>
                                    @foreach($group_policies as $group_policy)
                                        <option value="{{$group_policy->id}}">{{$group_policy->name}}</option>
                                    @endforeach
                                </select>
                            </div>



                        </div>
                    </accordian>


                    <accordian :title="'Device Location'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('storage_location') ? 'has-error' : '']">
                                <label for="storage_location">Storage Location</label>
                                <input type="text"  class="control" id="storage_location" name="storage_location" value="{{ old('storage_location') ?: $work_order->storage_location }}" />
                                <span class="control-error" v-if="errors.has('storage_location')">@{{ errors.first('storage_location') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('repair_location') ? 'has-error' : '']">
                                <label for="repair_location">Repair Location</label>
                                <input type="text"  class="control" id="repair_location" name="repair_location"  value="{{ old('repair_location') ?: $work_order->repair_location }}" />
                                <span class="control-error" v-if="errors.has('repair_location')">@{{ errors.first('repair_location') }}</span>
                            </div>






                        </div>
                    </accordian>

                </div>
            </div>

        </form>
    </div>
@stop

