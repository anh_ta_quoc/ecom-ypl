@extends('repair::layouts.content')

@section('page_title')
    Work Orders
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>  Work Orders</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>
                @if (auth()->guard('admin')->user()->type != 'repair_shop')
                <a href="{{ route('admin.work_order.create') }}" class="btn btn-lg btn-primary">
                    Add New Work Order
                </a>
                 @endif
            </div>
        </div>
        <div class="page-header">
            <date-filter></date-filter>
        </div>
        <div class="page-content">
            @inject('workOrderGrid', 'YPL\Repair\DataGrids\WorkOrderDataGrid')

            {!! $workOrderGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

{{--@push('scripts')--}}
    {{--@include('admin::export.export', ['gridName' => $workOrderGrid])--}}
{{--@endpush--}}
@push('scripts')
    @include('repair::export.export', ['gridName' => $workOrderGrid])
    <script type="text/x-template" id="status-container-template">
        <div class="control-group text">
            <select class="control" :name="'job_status['+work_order_id+']'" :id="work_order_id" @change="changeJobStatus">
                <option value="OPENED" :selected="job_status == 'OPENED'">OPENED</option>
                <option value="COMPLETED" :selected="job_status == 'COMPLETED'">COMPLETED</option>
                <option value="CLOSED" :selected="job_status == 'CLOSED'">CLOSED</option>
            </select>
        </div>
    </script>

    <script>
        Vue.component('status-container', {
            template: '#status-container-template',
            props: ['work_order_id', 'job_status'],
            inject: ['$validator'],
            data: () => ({
                job_status: 'ON PROGRESS',
            }),
            created() {

            },
            methods: {
                changeJobStatus(event) {
                    var this_this = this;
                    var _clickedElement = event.target;
                    var selectedValue = _clickedElement.value;

                    this_this.$http.post("{{ url('admin/work_order/change_job_status') }}", {
                        _token: '{{ csrf_token() }}',
                        id: this_this.work_order_id,
                        job_status: selectedValue
                    })
                        .then(response => {
                            if (response.data.status) {
                                $('.alert-wrapper').html('<div class="alert alert-success"><span class="icon white-cross-sm-icon" onclick="$(this).parent().remove();"></span><p>'+ response.data.message +'</p></div>');
                            } else {
                                $('.alert-wrapper').html('<div class="alert alert-danger"><span class="icon white-cross-sm-icon" onclick="$(this).parent().remove();"></span><p>'+ response.data.message +'</p></div>');
                            }
                        })
                        .catch(function (error) {
                            console.log('Error in change job status: ' + error);
                        });

                }
            }
        });
    </script>
    <script type="text/x-template" id="date-filter-template">
        <div style="text-align:center;">
            <div>


                <div class="control-group" style="width: 250px;margin: 0px 0px 0px 20px;">
                    <select class="control" v-model="status">
                        <option value="">-- Job Status --</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_SUBMITTED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_SUBMITTED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_POSTAGE_INSTRUCTIONS_ISSUED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_POSTAGE_INSTRUCTIONS_ISSUED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_ALLOCATED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_ALLOCATED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_DELIVERED_TO_WAREHOUSE}}">{{\YPL\Repair\Models\WorkOrder::STATUS_DELIVERED_TO_WAREHOUSE}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_REQUIRED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_REQUIRED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_QUOTED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_QUOTED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_ACCEPTED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_ESTIMATED_ACCEPTED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_UNDER_REPAIR}}">{{\YPL\Repair\Models\WorkOrder::STATUS_UNDER_REPAIR}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_PART_REQUEST}}">{{\YPL\Repair\Models\WorkOrder::STATUS_PART_REQUEST}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_PART_ORDERED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_PART_ORDERED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_PART_IN_STOCK}}">{{\YPL\Repair\Models\WorkOrder::STATUS_PART_IN_STOCK}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_SAVAGE_FOR_PART}}">{{\YPL\Repair\Models\WorkOrder::STATUS_SAVAGE_FOR_PART}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_TECHNICIAN_COMPLETED}}">{{\YPL\Repair\Models\WorkOrder::STATUS_TECHNICIAN_COMPLETED}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_UNIT_ON_TEST}}">{{\YPL\Repair\Models\WorkOrder::STATUS_UNIT_ON_TEST}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_WAITING_FOR_PAYMENT}}">{{\YPL\Repair\Models\WorkOrder::STATUS_WAITING_FOR_PAYMENT}}</option>
                        <option value="{{\YPL\Repair\Models\WorkOrder::STATUS_CANCELED_OTHER}}">{{\YPL\Repair\Models\WorkOrder::STATUS_CANCELED_OTHER}}</option>
                    </select>
                </div>
                <div class="control-group" style="width: 250px;margin: 0px 0px 0px 20px;">
                    <input class="control" type="number" v-model="duration" placeholder="Duration">
                </div>

            </div>



            <div style="width: 100%;margin-top: 10px;overflow: hidden;text-align: right;">
                <button type="button" class="btn btn-lg btn-dark" @click="applyFilter">Filter</button>
                <button type="button" class="btn btn-lg btn-primary" @click="clearURL">Clear Filter</button>
            </div>
        </div>
    </script>

    <script>
        Vue.component('date-filter', {

            template: '#date-filter-template',

            data: function() {
                return {
                    status: "{{ request('status') ? request('status') : '' }}",
                    duration: "{{ request('duration') ? request('duration') : '' }}",
                    search_term: '',
                    is_searching: false,
                }
            },

            methods: {
                applyFilter: function() {
                    var url = '';

                    if ( this.status !== '') {
                        url += "&status=" + encodeURIComponent(this.status);
                    }
                    if (this.duration !== '') {
                        url += "&duration="+ encodeURIComponent(this.duration);
                    }

                    if ( url !== '') {
                        window.location.href = '?' + url.substring(1, url.length);
                    }
                },

                clearURL: function() {
                    window.location.href = '{{ route('admin.work_order.index') }}';
                },


            }
        });
    </script>
@endpush
