<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link
            rel="stylesheet"
            href=" {{ asset('vendor/YPL/repair/assets/css/repair.css') }} "
    />
    <title>Print Work Order</title>
</head>
<body>
@php
    $channel = core()->getCurrentChannel();

    $homeSEO = $channel->home_seo;

    if (isset($homeSEO)) {
        $homeSEO = json_decode($channel->home_seo);

        $metaTitle = $homeSEO->meta_title;

        $metaDescription = $homeSEO->meta_description;

        $metaKeywords = $homeSEO->meta_keywords;
    }
@endphp
<div class="pos-invoice-panel">
    <div class="pos_thermal">
        <div class="pos_shop_detail">
            <!-- <div class="details_row receipt-header">
                    <span class="main_heading">
                        <div class="pos_logo_thumb" >
                            <img src="{{Storage::url(core()->getConfigData('pos.configuration.general.pos_logo'))}}" class="pos_logo" />
                        </div>

                    </span>
                <span class="detail_value">
                        <div class="address-container">

                            <span class="store_name" >{{$metaTitle}}</span>

                            <span><pre>{{core()->getConfigData('pos.configuration.bill-receipt.store_address')}}</pre></span>

                            <span><b>Email: </b>{{core()->getConfigData('pos.configuration.bill-receipt.email_address')}}</span>

                            <span><b>Website: </b><a href="{{core()->getConfigData('pos.configuration.bill-receipt.website')}}" target="_blank">{{core()->getConfigData('pos.configuration.bill-receipt.website')}}</a></span>

                            <span><b>Phone:</b>{{core()->getConfigData('pos.configuration.bill-receipt.phone_number')}}</span>

                            <span><b>Customer Care: </b>{{core()->getConfigData('pos.configuration.bill-receipt.cc_number')}}</span>

                        </div>

                    </span>
            </div> -->
            <div class="separate_row"></div>

            <div class="details_row">
                <span class="main_heading">Date:</span>
                <span class="detail_value">{{ $work_order->created_at->format('d M Y') }}</span>
            </div>
            <div class="details_row">
                <span class="main_heading">Barcode: </span>
                <!-- <span class="detail_value bold_content">#{{ $work_order->id }}</span> -->
            </div>
            <div class="details_row">
                <!-- <span class="main_heading">Barcode: </span>
                <span class="detail_value bold_content">#{{ $work_order->id }}</span> -->
                <img src="https://barcode.tec-it.com/barcode.ashx?data={{ $work_order->id }}&code=Code128&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Jpg&rotation=0&color=%23000000&bgcolor=%23ffffff&codepage=&qunit=Mm&quiet=0" />
                <!-- <p style="text-align:center;">#{{ $work_order->id }}</p> -->
            </div>

            <div class="details_row">
                <span class="main_heading">Customer: </span>
                <!-- <span class="detail_value padding_content">
                        <i class="fa fa-id-card"></i>
                    {{ $work_order->customer_name }}
                    </span> -->

            </div>
            <!-- <div class="details_row">
                <span class="main_heading"></span>
                <span class="detail_value padding_content">
                        <i class="fa fa-phone"></i>
                    {{ $work_order->customer_phone }}
                    </span>
            </div>
            <div class="details_row">
                <span class="main_heading"></span>
                <span class="detail_value padding_content">
                        <i class="fa fa-envelope"></i>
                    {{ $work_order->customer_email }}
                    </span>
            </div> -->
            <div class="details_row">
                <p><strong>N:</strong> {{ $work_order->customer_name }}</p>
                <p><strong>P:</strong> {{ $work_order->customer_phone }}</p>
                <p><strong>E:</strong> {{ $work_order->customer_email }}</p>
            </div>

        </div>

        {{--<div class="separate_row"></div>--}}
        <div class="details_row">
            <span class="main_heading">Product: </span>


        </div>
        <div class="details_row">
            <p><strong>B:</strong> {{ $work_order->product_brand }}</p>
            <p><strong>M:</strong> {{ $work_order->product_name }}</p>
            <p><strong>N:</strong> {{ $work_order->note }}</p>
        </div>
    <!-- <div class="order_items">
            <table class="itams_table">
                <tr>
                    <th class="item_head">Brand</th>
                    <th class="item_head">Product</th>
                    {{--<th class="item_head">Price</th>--}}
                    <th class="item_head">Note</th>
                </tr>

                <tr>
                    <td class="invoice_product_brand">
                        <div class="product_name">{{ $work_order->product_brand }}

                        </div>
                    </td>
                    <td class="invoice_product_name">
                        <div class="product_name">{{ $work_order->product_name }}

                        </div>
                    </td>
                    {{--<td>--}}
						{{--<span>--}}
                            {{--$ {{ $work_order->est_price }}--}}
                        {{--</span>--}}
                    {{--</td>--}}
                    <td>
                        {{ $work_order->note }}
                    </td>

                </tr>
            </table>

            {{--<table class="itams_table">--}}
                {{--<tr>--}}
                    {{--<td>Total quantity</td>--}}
                    {{--<td>1</td>--}}

                    {{--<td>Sub total:</td>--}}
                    {{--<td>--}}

                            {{--<span>--}}
                                 {{--$ {{ $work_order->est_price }}--}}
                            {{--</span>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td colspan="2"></td>--}}
                    {{--<td>Discount:</td>--}}
                    {{--<td>--}}

                            {{--<span>--}}
                                {{--$ 0.00--}}
                            {{--</span>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td colspan="2"></td>--}}
                    {{--<td>Tax:</td>--}}
                    {{--<td>--}}
                            {{--<span>--}}
                                {{--$ 0.00--}}
                            {{--</span>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td colspan="2"></td>--}}
                    {{--<td><b>Total:</b></td>--}}
                    {{--<td>--}}

                            {{--<span >--}}
                                {{--<b>$ {{ $work_order->est_price }}</b>--}}
                            {{--</span>--}}
                    {{--</td>--}}
                {{--</tr>--}}


            {{--</table>--}}
        </div>

        <div class="separate_row"></div>

        <div class="pos_order_note">
            <div class="details_row">
                <span class="main_heading"> Note</span>
                <span class="detail_value">N/A</span>
            </div>
        </div> -->

        <div class="separate_row"></div>


    </div>
</div>
</body>
<script>
    window.print();
</script>
</html>