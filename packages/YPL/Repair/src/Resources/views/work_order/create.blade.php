@extends('repair::layouts.content')

@section('page_title')
    Add Work Order
@stop

@section('tab')
    <div class="tabs"><ul><li class="active"><a href="{{route('admin.work_order.create')}}">
                    Single
                </a></li> <li><a href="{{route('admin.work_order.create_batch')}}">
                    Upload File
                </a></li></ul></div>
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.work_order.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Add Work Order
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input type="hidden" name="locale" value="all"/>




                    <accordian :title="'Customer'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('customer_name') ? 'has-error' : '']">
                                <label for="customer_name" class="required">Name</label>
                                <input type="text" class="control" name="customer_name" v-validate="'required'" value="{{ old('customer_name') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.firstname') }}&quot;">
                                <span class="control-error" v-if="errors.has('customer_name')">@{{ errors.first('customer_name') }}</span>
                            </div>


                            <div class="control-group" :class="[errors.has('customer_email') ? 'has-error' : '']">
                                <label for="customer_email" class="required">{{ __('shop::app.customer.signup-form.email') }}</label>
                                <input type="email" class="control" name="customer_email" v-validate="'required|email'" value="{{ old('customer_email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                <span class="control-error" v-if="errors.has('customer_email')">@{{ errors.first('customer_email') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('customer_phone') ? 'has-error' : '']">
                                <label for="phone">{{ __('admin::app.customers.customers.phone') }}</label>
                                <input type="text" class="control" name="customer_phone" value="{{ old('customer_phone') }}" data-vv-as="&quot;{{ __('admin::app.customers.customers.phone') }}&quot;">
                                <span class="control-error" v-if="errors.has('customer_phone')">@{{ errors.first('customer_phone') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('customer_address') ? 'has-error' : '']">
                                <label for="customer_address">Address</label>
                                <input type="text" class="control" name="customer_address" value="{{ old('customer_address') }}" data-vv-as="&quot;Customer Address&quot;">
                                <span class="control-error" v-if="errors.has('customer_address')">@{{ errors.first('customer_address') }}</span>
                            </div>

                        </div>
                    </accordian>


                    <accordian :title="'{{ __('admin::app.catalog.categories.general') }}'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('product_name') ? 'has-error' : '']">
                                <label for="product_name" class="required">Product Name</label>
                                <input type="text" v-validate="'required'" class="control" id="product_name" name="product_name" value="{{ old('product_name') }}" data-vv-as="&quot;Product Name&quot;" v-slugify-target="'slug'"/>
                                <span class="control-error" v-if="errors.has('product_name')">@{{ errors.first('name') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('product_imei') ? 'has-error' : '']">
                                <label for="product_imei" class="required">Imei</label>
                                <input type="text" v-validate="'required'" class="control" id="product_imei" name="product_imei" value="{{ old('product_imei') }}" data-vv-as="&quot;IMEI&quot;" />
                                <span class="control-error" v-if="errors.has('product_imei')">@{{ errors.first('product_imei') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('product_brand') ? 'has-error' : '']">
                                <label for="product_brand" class="required">Brand</label>
                                <input type="text" v-validate="'required'" class="control" id="product_brand" name="product_brand" value="{{ old('product_brand') }}" data-vv-as="&quot;Brand&quot;" />
                                <span class="control-error" v-if="errors.has('product_brand')">@{{ errors.first('product_brand') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('product_status') ? 'has-error' : '']">
                                <label for="product_status" class="required">Condition</label>
                                <select class="control" v-validate="'required'" id="product_status" name="product_status" data-vv-as="&quot;Type&quot;">
                                    <option value="New" selected >
                                        New
                                    </option>
                                    <option value="Repaired">
                                        Repaired
                                    </option>
                                    <option value="Samsung IW">
                                        Samsung IW ( In warranty)
                                    </option>

                                </select>
                            </div>


                            <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                                <label for="note" >Note</label>
                                <textarea class="control" name="note" v-validate="'max:250'"  data-vv-as="&quot;Note&quot;"> </textarea>
                                <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                            </div>




                        </div>
                    </accordian>
                    @if (auth()->guard('admin')->user()->type == null || auth()->guard('admin')->user()->type == 'shop')
                    <accordian :title="'Request From '" :active="true">
                        <div slot="body" id="request_from">

                            <div class="control-group" :class="[errors.has('type') ? 'has-error' : '']">
                                <label for="type" class="required">Request from</label>
                                <select class="control" v-validate="'required'" id="type" name="type" data-vv-as="&quot;Type&quot;">
                                    <option value="shop" selected >
                                        Shop
                                    </option>
                                    <option value="partner">
                                        Partner
                                    </option>
                                    <option value="insurance" >
                                        Insurance
                                    </option>
                                </select>
                                <span class="control-error" v-if="errors.has('type')">@{{ errors.first('type') }}</span>
                            </div>
                            <div id="request_content">
                                <div class="control-group shop" id="shop"  >
                                    <label for="type" class="required">Shop</label>
                                    <select class="control"  id="shop" name="shop_id" >
                                        @foreach($shops as $shop)
                                            <option value="{{$shop->id}}" >
                                                {{$shop->name}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="control-group partner" id="partner" style="display: none" >
                                    <label for="type" class="required">Partner</label>
                                    <select class="control"  id="partner_id" name="partner_id" >
                                        @foreach($partners as $partner)
                                            <option value="{{$partner->id}}" >
                                                {{$partner->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="control-group insurance" id="insurance" style="display: none;" >
                                    <label for="insurance" class="required">Insurance</label>
                                    <select class="control"  id="insurance_id" name="insurance_id" >
                                        @foreach($insurances as $insurance)
                                            <option value="{{$insurance->id}}" >
                                                {{$insurance->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="control-group insurance" id="policies" style="display: none;" >
                                    <label for="policies_select" class="required">Policies</label>
                                    <select class="control"  id="policies_select" name="policies[]" multiple >
                                        @foreach($policies as $policy)
                                            <option value="{{ $policy->id}}" >
                                                {{ $policy->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="control-group insurance" id="budget" style="display: none;" >
                                    <label for="budget" class="required">Budget</label>
                                    <input type="number"  class="control" id="budget" name="Budget" step="0.01" value="{{ old('budget') }}" data-vv-as="&quot;Budget&quot;" />

                                </div>

                            </div>

                        </div>
                    </accordian>
                    @endif
                    @if (auth()->guard('admin')->user()->type == 'insurance')
                        <accordian :title="'Request From '" :active="true">
                            <div slot="body" id="request_from">

                                <div class="control-group" :class="[errors.has('type') ? 'has-error' : '']">
                                    <label for="type" class="required">Request from</label>
                                    <select readonly="" class="control" v-validate="'required'" id="type" name="type" data-vv-as="&quot;Type&quot;">

                                        <option value="insurance" selected >
                                            Insurance
                                        </option>
                                    </select>
                                    <span class="control-error" v-if="errors.has('type')">@{{ errors.first('type') }}</span>
                                </div>
                                <div id="request_content">

                                    <div class="control-group insurance" id="insurance" style="" >
                                        <label for="insurance" class="required">Insurance</label>
                                        <select readonly="" class="control"  id="insurance_id" name="insurance_id" >
                                            @foreach($insurances as $insurance)
                                                @if(auth()->guard('admin')->user()->insurance_id == $insurance->id)
                                                <option value="{{$insurance->id}}" >
                                                    {{$insurance->name}}
                                                </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="control-group insurance" id="policies" >
                                        <label for="policies_select" class="required">Policies</label>
                                        <select class="control"  id="policies_select" name="policies[]" multiple >
                                            @foreach($policies as $policy)
                                                <option value="{{ $policy->id}}" >
                                                    {{ $policy->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="control-group insurance" id="budget" >
                                        <label for="budget" class="required">Budget</label>
                                        <input type="number" required  class="control" id="budget" name="Budget" step="0.01" value="{{ old('budget') }}" data-vv-as="&quot;Budget&quot;" />

                                    </div>

                                </div>

                            </div>
                        </accordian>
                    @endif
                    @if (auth()->guard('admin')->user()->type == 'partner')
                        <accordian :title="'Request From '" :active="true">
                            <div slot="body" id="request_from">

                                <div class="control-group" :class="[errors.has('type') ? 'has-error' : '']">
                                    <label for="type" class="required">Request from</label>
                                    <select readonly="" class="control" v-validate="'required'" id="type" name="type" data-vv-as="&quot;Type&quot;">

                                        <option value="partner" selected>
                                            Partner
                                        </option>

                                    </select>
                                    <span class="control-error" v-if="errors.has('type')">@{{ errors.first('type') }}</span>
                                </div>
                                <div id="request_content">

                                    <div class="control-group partner" id="partner" style="display: none" >
                                        <label for="type" class="required">Partner</label>
                                        <select class="control" readonly=""  id="partner_id" name="partner_id" >
                                            @foreach($partners as $partner)
                                                @if(auth()->guard('admin')->user()->partner_id == $partner->id)
                                                <option value="{{$partner->id}}"
                                                 selected="selected" >
                                                    {{$partner->name}}
                                                </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>


                                </div>

                            </div>
                        </accordian>
                    @endif
                    <accordian :title="'Need to Borrow Phone'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('borrow_imei') ? 'has-error' : '']">
                                <label for="customer_name" >IMEI</label>
                                <input type="text" class="control" name="borrow_imei"  value="{{ old('borrow_imei') }}" >
                                <span class="control-error" v-if="errors.has('borrow_imei')">@{{ errors.first('borrow_imei') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('borrow_model') ? 'has-error' : '']">
                                <label for="customer_name" >Model</label>
                                <input type="text" class="control" name="borrow_model"  value="{{ old('borrow_model') }}" >
                                <span class="control-error" v-if="errors.has('borrow_model')">@{{ errors.first('borrow_model') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('borrow_price') ? 'has-error' : '']">
                                <label for="customer_name" >Price</label>
                                <input type="number" class="control" name="borrow_price"  value="{{ old('borrow_price') }}" >
                                <span class="control-error" v-if="errors.has('borrow_price')">@{{ errors.first('borrow_price') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('borrow_note') ? 'has-error' : '']">
                                <label for="customer_name" >note</label>
                                <input type="text" class="control" name="borrow_note"  value="{{ old('borrow_note') }}" >
                                <span class="control-error" v-if="errors.has('borrow_note')">@{{ errors.first('borrow_note') }}</span>
                            </div>



                        </div>
                    </accordian>

                </div>
            </div>

        </form>
    </div>

    <div id="temp" style="display: none">
        <div class="control-group" id="shop" >
            <label for="type" class="required">Shop</label>
            <select class="control"  id="shop" name="shop_id" >
                @foreach($shops as $shop)
                    <option value="{{$shop->id}}" >
                        {{$shop->name}}
                    </option>
                @endforeach

            </select>
        </div>
        <div class="control-group" id="partner" >
            <label for="type" class="required">Partner</label>
            <select class="control"  id="partner" name="partner_id" >
                @foreach($partners as $partner)
                    <option value="{{$partner->id}}" >
                        {{$partner->name}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="control-group" id="insurance" >
            <label for="type" class="required">Insurance</label>
            <select class="control"  id="insurance" name="insurance_id" >
                @foreach($insurances as $insurance)
                    <option value="{{$insurance->id}}" >
                        {{$insurance->name}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>


@stop

@push('scripts')
    <script>
        $( document ).ready(function() {
            $('#type').on('change', function() {
                var val = $(this).val();
                $("#request_content .control-group").hide();
                $("." + val).show();
            });


            $('#insurance_id').on('change', function(){
                var insurance_id = $(this).val();
                if(insurance_id){
                    $.ajax({
                        type:'GET',
                        url:'{{route('policy.all')}}' +'/'+insurance_id,
                        dataType: "json",
                        success:function(data){

                            $('#policies_select').html('');
                            $.each(data, function(i, item) {
                                $('#policies_select').append('<option value="'+item.id+'">'+item.name+'</option>');
                            });

                        }
                    });
                }else{

                }
            });
        });
    </script>
@endpush
