@extends('repair::layouts.content')

@section('page_title')
    Work Order # {{$work_order->id}}
@stop
@section('tab')
    <div class="tabs">
        <ul>
            <li class="active"><a href="{{route('admin.work_order.view',$work_order->id)}}">
                    Info
                </a></li>
            <li><a href="{{route('admin.work_log.index', ['work_order_id'=>$work_order->id])}}">
                    Work Log
                </a></li>
        </ul>
    </div>
@stop
@inject ('shopRepository', 'YPL\Repair\Repositories\ShopRepository')
@inject ('partnerRepository', 'YPL\Repair\Repositories\PartnerRepository')
@inject ('groupPolicyRepository', 'YPL\Repair\Repositories\GroupPolicyRepository' )
@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link"
                       onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                    Work Order # {{$work_order->id}}
                </h1>
            </div>

            <div class="page-action">
                @if ($work_order->canIssue())
                    <a class="btn btn-sm btn-success" @click="showModal('issue')">POSTAGE INSTRUCTION ISSUED
                    </a>
                @endif
                @if($work_order->canAllocate())
                    <a class="btn btn-sm btn-success" @click="showModal('allocate')">ALLOCATED</a>
                @endif
                @if ($work_order->canDeliver())
                    <a class="btn btn-sm btn-success" @click="showModal('delivery')">DELIVERED TO WAREHOUSE
                    </a>
                @endif
                @if ($work_order->canEstimate())
                    <a class="btn btn-sm btn-success" @click="showModal('needToEstimate')">ESTIMATION REQUIRED
                    </a>
                @endif
                @if ($work_order->canEstimateQuote())
                    <a class="btn btn-sm btn-success" @click="showModal('estimate')">ESTIMATION QUOTED
                    </a>
                @endif

                @if ($work_order->canRepair())
                    <a class="btn btn-sm btn-success" @click="showModal('claim')">UNDER REPAIR</a>
                @endif
                @if ($work_order->canAssignment())
                    <a class="btn btn-sm btn-success" @click="showModal('assignment')">ASSIGN TO REPAIR PARTNER</a>
                @endif
                @if ($work_order->canRequestPart())
                    <a class="btn btn-sm btn-success" @click="showModal('needToPart')">PART REQUEST</a>
                @endif
                @if ($work_order->canOrderPart())
                    <a class="btn btn-sm btn-success" @click="showModal('orderPart')">PART ORDERED</a>
                @endif
                @if ($work_order->canSavePart())
                    <a class="btn btn-sm btn-success" @click="showModal('savePart')">PART IN STOCK</a>
                @endif
                @if ($work_order->canUnitTest())
                    <a class="btn btn-sm btn-success" @click="showModal('unitTest')">UNIT ON TEST</a>
                @endif
                @if ($work_order->canSavagePart())
                    <a class="btn btn-sm btn-danger" @click="showModal('savagePart')">Savage For Part</a>
                @endif

                @if ($work_order->canComplete())
                    <a class="btn btn-sm btn-success" @click="showModal('complete')">COMPLETED</a>
                @endif
                @if ($work_order->canReturn())
                    <a class="btn btn-sm btn-danger" @click="showModal('return')">RETURN</a>
                @endif
                @if ($work_order->canEstimateAccept())
                    <a class="btn btn-sm btn-success" @click="showModal('accept')">ESTIMATION ACCEPTED</a>
                @endif
                @if ($work_order->canReject())
                    <a class="btn btn-sm btn-danger" @click="showModal('reject')">REJECT</a>
                @endif

            </div>
        </div>

        <div class="page-content">
            <tabs>
                <tab name="Work Order" :selected="true">
                    <div class="sale-container">
                        <div class="accordian active">
                            <div class="accordian-content">
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Quotation</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <span class="title">Urgent Status</span>
                                            <span class="value">{{$work_order->urgent_status}}
                                                <a href="#" @click="showModal('updateUrgent')">
                                                <span class="icon pencil-lg-icon"></span>
                                            </a>
                                            </span>
                                        </div>
                                        <div class="row">
                                            <span class="title">Job Status</span>
                                            <span class="value">{{$work_order->status}}</span>
                                        </div>
                                        <div class="row">
                                            <span class="title">WO Status</span>
                                            <span class="value">{{$work_order->job_status}}</span>
                                        </div>
                                        <div class="row">
                                            <span class="title">Invoice Status</span>
                                            <span class="value">{{$work_order->invoice_status}}
                                                @if($work_order->canUpdateInvoice())
                                                    <a href="#" @click="showModal('updateInvoice')">
                                                    <span class="icon pencil-lg-icon"></span>
                                                </a>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="row">
                                            <span class="title">Request Part</span>
                                            <span class="value">{{$work_order->part_request}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Services</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <span class="title">
                                                Estimated cost
                                            </span>
                                            <span class="value">
                                                ${{number_format($work_order->est_price)}}
                                            </span>
                                        </div>
                                        <div class="row">
                                            <span class="title">
                                                Repair plan
                                            </span>
                                            <span class="value">
                                                {{$work_order->est_plan}}
                                            </span>
                                        </div>
                                        <div class="row">
                                            <span class="title">
                                                ETA
                                            </span>
                                            <span class="value">
                                                {{$work_order->est_date}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @if(count($work_order->item_repairs) > 0)

                                    <div class="sale-section">
                                        <div class="secton-title">
                                            <span>Quotation</span>
                                        </div>
                                        <div class="section-content">
                                            @foreach($work_order->item_repairs as $item)
                                                <div class="row">
                                            <span class="title">
                                                {{$item->name}}
                                            </span>
                                                    <span class="value">
                                                ${{number_format($item->price)}}
                                            </span>
                                                </div>
                                            @endforeach
                                            <div class="row">
                                            <span class="title">
                                                Total
                                            </span>
                                                <span class="value">
                                                ${{number_format($work_order->item_repairs->sum('price'))}}
                                            </span>
                                            </div>
                                        </div>

                                    </div>
                                @endif
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Group Policy</span>
                                    </div>
                                    @if($work_order->group_policy_id != null)
                                        <div class="section-content">
                                            <div class="row">
                                                <span class="title">From</span>
                                                <span class="value">{{$work_order->type}}</span>
                                            </div>
                                            <div class="row">
                                                <span class="title">Partner</span>
                                                <span class="value">
                                                @if($work_order->type == 'shop' && $work_order->shop_id != null)
                                                        {{ $shopRepository->findOrFail($work_order->shop_id)->name }}
                                                    @elseif($work_order->type == 'insurance' && $work_order->insurance_id != null)
                                                        {{ $partnerRepository->findOrFail($work_order->insurance_id)->name }}
                                                    @elseif($work_order->type == 'partner' && $work_order->partner_id != null)
                                                        {{ $partnerRepository->findOrFail($work_order->partner_id)->name }}
                                                    @endif
                                            </span>
                                            </div>
                                            <div class="row">
                                            <span class="title">
                                                Scheme
                                            </span>
                                                <span class="value">
                                                {{$work_order->group_policy->name}}
                                                    <a href="#" @click="showModal('updatePolicy')"><span
                                                                class="icon pencil-lg-icon"></span></a>
                                            </span>
                                            </div>
                                            <div class="row">
                                            <span class="title">
                                                Policies
                                            </span>
                                                <span class="value">
                                                <ul>
                                                @foreach($work_order->group_policy->policies as $item)
                                                        <li>{{$item->name}}</li>
                                                    @endforeach
                                                </ul>
                                            </span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </tab>
                <tab name="Device">
                    <div class="sale-container">
                        <div class="accordian active">
                            <div class="accordian-content">
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Device Information</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <div class="row">
                                                <span class="title">Name</span>
                                                <span class="value">{{$work_order->product_name}}</span>
                                            </div>
                                            <div class="row">
                                                <span class="title">Brand</span>
                                                <span class="value">{{$work_order->product_brand}}</span>
                                            </div>
                                            <div class="row">
                                                <span class="title">IMEI / Serial Number</span>
                                                <span class="value">{{$work_order->product_imei}}</span>
                                            </div>
                                            <div class="row">
                                                <span class="title">Note</span>
                                                <span class="value">{{$work_order->note}}</span>
                                            </div>
                                            <div class="row">
                                                <span class="title">Condition</span>
                                                <span class="value">{{$work_order->product_status}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </tab>
                <tab name="Location">
                    <div class="sale-container">
                        <div class="accordian active">
                            <div class="accordian-content">
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Repair Location</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <span class="title">
                                                Location
                                            </span>
                                            <span class="value">
                                                {{$work_order->repair_location}}
                                                <a type="button" href="#" @click="showModal('updateRepairLocation')">
                                                    <span class="icon pencil-lg-icon"></span>
                                                </a>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Storage Location</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <span class="title">
                                                Location
                                            </span>
                                            <span class="value">
                                                {{$work_order->storage_location}}
                                                <a type="button" href="#" @click="showModal('updateStorageLocation')">
                                                    <span class="icon pencil-lg-icon"></span>
                                                </a>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </tab>
                <tab name="Customer">
                    <div class="sale-container">
                        <div class="accordian active">
                            <div class="accordian-content">
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Information</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <span class="title">
                                                Name
                                            </span>
                                            <span class="value">
                                                {{$work_order->customer_name}}
                                            </span>
                                        </div>
                                        <div class="row">
                                            <span class="title">
                                                Email
                                            </span>
                                            <span class="value">
                                                {{$work_order->customer_email}}
                                            </span>
                                        </div>
                                        <div class="row">
                                            <span class="title">
                                                Phone
                                            </span>
                                            <span class="value">
                                                {{$work_order->customer_phone}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Pick up & Return</span>
                                    </div>
                                    <div class="section-content">
                                        <div class="row">
                                            <span class="title">
                                                Address:
                                            </span>
                                            <span class="value">
                                                {{$work_order->customer_address}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </tab>
            </tabs>
        </div>
        <modal id="updateStorageLocation" :is-open="modalIds.updateStorageLocation">
            <h3 slot="header">Update Storage Location</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.update',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div slot="body">
                        <div class="control-group" :class="[errors.has('storage_location') ? 'has-error' : '']">
                            <label for="storage_location">Storage Location</label>
                            <input type="text" v-validate="'required'" class="control" step="0.01" id="storage_location"
                                   name="storage_location"
                                   value="{{ old('storage_location') ?: $work_order->storage_location }}"
                                   data-vv-as="&quot;storage location&quot;"/>
                            <span class="control-error" v-if="errors.has('storage_location')">@{{ errors.first('storage_location') }}</span>
                        </div>

                    </div>

                    <button type="submit" class="btn  btn-primary">
                        Save
                    </button>

                </form>
            </div>
        </modal>
        <modal id="updateRepairLocation" :is-open="modalIds.updateRepairLocation">
            <h3 slot="header">Update Repair Location</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.update',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('repair_location') ? 'has-error' : '']">
                        <label for="repair_location">Repair location</label>
                        <input type="text" v-validate="'required'" class="control" id="repair_location"
                               name="repair_location"
                               value="{{ old('storage_location') ?: $work_order->repair_location }}"
                               data-vv-as="&quot;repair location&quot;"/>
                        <span class="control-error" v-if="errors.has('repair_location')">@{{ errors.first('repair_location') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>

                </form>
            </div>
        </modal>
        <modal id="issue" :is-open="modalIds.issue">
            <h3 slot="header">POSTAGE INSTRUCTION ISSUES</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.issue',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div slot="body">


                        <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                            <label for="product_status">Note</label>
                            <textarea type="text" class="control" id="note"
                                      name="note"
                            >{!! old('note') !!}</textarea>
                            <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                        </div>
                    </div>

                    <button type="submit" class="btn  btn-primary">
                        Save
                    </button>

                </form>
            </div>
        </modal>
        <modal id="updatePolicy" :is-open="modalIds.updatePolicy">
            <h3 slot="header">Update Group Policy</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.update',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('updatePolicy') ? 'has-error' : '']">
                        <label for="group_policy_id">Group Policy</label>
                        <select class="control" v-validate="'required'" id="group_policy_id" name="group_policy_id"
                                data-vv-as="&quot;urgent status&quot;">
                            @foreach($groupPolicyRepository->all() as $groupPolicy)
                                <option value="{{$groupPolicy->id}}"
                                        @if($work_order->group_policy_id ==$groupPolicy->id) selected="" @endif>{{$groupPolicy->name}}</option>
                            @endforeach
                        </select>
                        <span class="control-error" v-if="errors.has('group_policy_id')">@{{ errors.first('group_policy_id') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="updateUrgent" :is-open="modalIds.updateUrgent">
            <h3 slot="header">Update Urgent Status</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.update',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('urgent_status') ? 'has-error' : '']">
                        <label for="urgent_status">Urgent Status</label>
                        <select class="control" v-validate="'required'" id="urgent_status" name="urgent_status"
                                data-vv-as="&quot;urgent status&quot;">
                            <option value="No" @if($work_order->urgent_status == 'No') selected @endif>No</option>

                            <option value="Yes" @if($work_order->urgent_status == 'Yes') selected @endif>Yes</option>

                        </select>
                        <span class="control-error" v-if="errors.has('urgent_status')">@{{ errors.first('urgent_status') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>

                </form>
            </div>
        </modal>

        <modal id="updateInvoice" :is-open="modalIds.updateInvoice">
            <h3 slot="header">Update Invoice Status</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.update',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('invoice_status') ? 'has-error' : '']">
                        <label for="invoice_status">Invoice Status</label>
                        <select class="control" v-validate="'required'" id="invoice_status" name="invoice_status"
                                data-vv-as="&quot;invoice_statuss&quot;">
                            <option value="{{\YPL\Repair\Models\WorkOrder::INVOICE_STATUS_WAITING}}"
                                    @if($work_order->invoice_status == \YPL\Repair\Models\WorkOrder::INVOICE_STATUS_WAITING) selected @endif>
                                WAITING FOR PAYMENT
                            </option>

                            <option value="{{\YPL\Repair\Models\WorkOrder::INVOICE_STATUS_PAID}}"
                                    @if($work_order->invoice_status == \YPL\Repair\Models\WorkOrder::INVOICE_STATUS_PAID) selected @endif>
                                PAID
                            </option>

                        </select>
                        <span class="control-error" v-if="errors.has('invoice_status')">@{{ errors.first('invoice_status') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>

                </form>
            </div>
        </modal>

        <modal id="allocate" :is-open="modalIds.allocate">
            <h3 slot="header">Mark As ALLOCATED</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.allocate',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control" id="note" name="note">{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>

        <modal id="delivery" :is-open="modalIds.delivery">
            <h3 slot="header">Mark as DELIVERED TO WAREHOUSE
            </h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.delivery',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control" name="note">{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="needToEstimate" :is-open="modalIds.needToEstimate">
            <h3 slot="header">Mark as ESTIMATION REQUIRED
            </h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.needToEstimate',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('product_status') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control" id="note" name="note">{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>

        <modal id="estimate" :is-open="modalIds.estimate">
            <h3 slot="header"> Mark as ESTIMATION QUOTED
            </h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.sendQuotation',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('est_price') ? 'has-error' : '']">
                        <label for="est_price" class="required">Estimated Cost</label>
                        <input type="number" v-validate="'required'" class="control" step="0.01" id="est_price"
                               name="est_price"
                               value="{{ old('est_price') ?: $work_order->est_price }}"
                               data-vv-as="&quot;Estimate Price&quot;"/>
                        <span class="control-error"
                              v-if="errors.has('est_price')">@{{ errors.first('est_price') }}</span>
                    </div>
                    <div class="control-group" :class="[errors.has('est_plan') ? 'has-error' : '']">
                        <label for="est_plan" class="">Repair Note</label>
                        <textarea class="control" name="est_plan" v-validate="'max:250'"
                                  data-vv-as="&quot;Estimat Plan&quot;"> {{old('est_plan') ?: $work_order->est_plan}} </textarea>
                        <span class="control-error"
                              v-if="errors.has('est_plan')">@{{ errors.first('est_plan') }}</span>
                    </div>
                    <div class="control-group date" :class="[errors.has('est_date') ? 'has-error' : '']">
                        <label for="est_date">ETA</label>
                        <date>
                            <input type="text" class="control" id="est_date" name="est_date"
                                   value="{{ old('est_date') ?: $work_order->est_date }}"
                                   data-vv-as="&quot;Estimate Date&quot;" v-slugify-target="'slug'"/>
                        </date>
                        <span class="control-error"
                              v-if="errors.has('est_date')">@{{ errors.first('est_date') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>


        <modal id="assignment" :is-open="modalIds.assignment">
            <h3 slot="header">Assign to a Repair Partner</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.assignment',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('repair_shop_id') ? 'has-error' : '']">
                        <label for="repair_shop_id">Repair Partner</label>
                        <select class="control" v-validate="'required'" id="repair_shop_id" name="repair_shop_id"
                                data-vv-as="&quot;Repair Partner&quot;">
                            @foreach($repair_shops as $shop)
                                <option value="{{$shop->id}}">
                                    {{$shop->name}}
                                </option>
                            @endforeach
                        </select>
                        <span class="control-error" v-if="errors.has('repair_shop_id')">@{{ errors.first('repair_shop_id') }}</span>
                    </div>
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="product_status">Note</label>
                        <textarea type="text" class="control" id="note" name="note">{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>


        <modal id="claim" :is-open="modalIds.claim">
            <h3 slot="header">Mark as UNDER REPAIR</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.claim',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control" name="note">{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="needToPart" :is-open="modalIds.needToPart">
            <h3 slot="header">Mark as PART REQUEST</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.needToPart',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('part_request') ? 'has-error' : '']">
                        <label for="part_request" class="required">Required Part</label>
                        <textarea type="text" class="control"
                                  name="part_request" required
                        >{!! old('part_request') !!}</textarea>
                        <span class="control-error" v-if="errors.has('part_request')">@{{ errors.first('part_request') }}</span>
                    </div>
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="orderPart" :is-open="modalIds.orderPart">
            <h3 slot="header">Mark as PART ORDERED</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.orderPart',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="savePart" :is-open="modalIds.savePart">
            <h3 slot="header">Mark as PART IN STOCK
            </h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.savePart',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="savagePart" :is-open="modalIds.savagePart">
            <h3 slot="header">Mark as SAVAGE FOR PART
            </h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.savagePart',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="unitTest" :is-open="modalIds.unitTest">
            <h3 slot="header">Mark as UNIT ON TEST</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.unitTest',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>


        <modal id="accept" :is-open="modalIds.accept">
            <h3 slot="header">Mark as ESTIMATION ACCEPTED
            </h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.accept',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div slot="body">
                        <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                            <label for="note">Note</label>
                            <textarea type="text" class="control"
                                      name="note"
                            >{!! old('note') !!}</textarea>
                            <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>
        <modal id="reject" :is-open="modalIds.reject">
            <h3 slot="header">Mark as REJECTED</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.reject',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>

        <modal id="return" :is-open="modalIds.return">
            <h3 slot="header">Mark as CANCELLED</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.return',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                        <label for="note">Note</label>
                        <textarea type="text" class="control"
                                  name="note"
                        >{!! old('note') !!}</textarea>
                        <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </modal>


        <modal id="complete" :is-open="modalIds.complete">
            <h3 slot="header">Mark as COMPLETED</h3>
            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.complete',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <div class="control-group" :class="[errors.has('checklists') ? 'has-error' : '']">
                        <label for="checklists">Check list</label>
                        @foreach($checklists as $checklist)
                            <span class="checkbox">
                                <input type="checkbox" name="checklists[]" value="{{$checklist->id}}"/>
                                <label class="checkbox-view" for="checkbox2"></label>
                                {{$checklist->name}}
                                </span>
                        @endforeach
                        <span class="control-error"
                              v-if="errors.has('checklists')">@{{ errors.first('checklists') }}</span>
                    </div>

                    @if ($work_order->borrow_imei)
                    <h4>Need to borrow phone</h4>
                    <div class="control-group" :class="[errors.has('checklists') ? 'has-error' : '']">
                        <label for="checklists">IMEI</label>
                        <input type="text" class="control" readonly value="{{$work_order->borrow_imei}}">
                    </div>
                    <div class="control-group" :class="[errors.has('checklists') ? 'has-error' : '']">
                        <label for="checklists">Model</label>
                        <input type="text" class="control" readonly value="{{$work_order->borrow_model}}">
                    </div>
                    <div class="control-group" :class="[errors.has('checklists') ? 'has-error' : '']">
                        <label for="checklists">Price</label>
                        <input type="number" class="control" readonly value="{{$work_order->borrow_price}}">
                    </div>
                    <div class="control-group" :class="[errors.has('checklists') ? 'has-error' : '']">
                        <label for="checklists">Note</label>
                        <input type="text" class="control" readonly value="{{$work_order->borrow_note}}">
                    </div>
                    @endif
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>

            </div>
        </modal>
        <modal id="completeInvoice" :is-open="modalIds.completeInvoice">
            <h3 slot="header">Complete Invoice For Work Order</h3>

            <div slot="body">
                <form method="POST" action="{{ route('admin.work_order.completeInvoice',$work_order->id)  }}"
                      @submit.prevent="onSubmit">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">
                    <button type="submit" class="btn btn-md btn-primary">
                        Save
                    </button>
                </form>
                <div class="sale-section">
                    <div class="secton-title">
                        <span>Need to Borrow Phone</span>
                    </div>
                    <div class="section-content">
                        <div class="row">
                                            <span class="title">
                                               IMEI
                                            </span>
                            <span class="value">
                                                {{$work_order->borrow_imei}}
                                            </span>
                        </div>
                        <div class="row">
                                            <span class="title">
                                               Model
                                            </span>
                            <span class="value">
                                                {{$work_order->borrow_model}}
                                            </span>
                        </div>
                        <div class="row">
                                            <span class="title">
                                               Price
                                            </span>
                            <span class="value">
                                                ${{number_format($work_order->borrow_price)}}
                                            </span>
                        </div>

                        <div class="row">
                                            <span class="title">
                                                Note
                                            </span>
                            <span class="value">
                                                {{$work_order->borrow_note}}
                                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </modal>
    </div>
@stop
