@extends('admin::layouts.content')

@section('page_title')
    Edit Work Log
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{route('admin.work_log.update',$work_log->id)}}" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Edit Work Log
                    </h1>


                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                       Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">


                    <accordian :title="'{{ __('admin::app.catalog.categories.general') }}'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('note') ? 'has-error' : '']">
                                <label for="note" >Note</label>
                                <textarea class="control" name="note" v-validate="'max:250'"  data-vv-as="&quot;Note&quot;">{!! old('note') ?: $work_log->note !!} </textarea>
                                <span class="control-error" v-if="errors.has('note')">@{{ errors.first('note') }}</span>
                            </div>



                        </div>
                    </accordian>



                </div>
            </div>

        </form>
    </div>
@stop

