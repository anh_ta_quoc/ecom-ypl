@extends('repair::layouts.content')

@section('page_title')
    Work Log
@stop
@section('tab')
    <div class="tabs"><ul><li ><a href="{{route('admin.work_order.view',$work_order_id)}}">
                    Info
                </a></li> <li class="active"><a href="{{route('admin.work_log.index',$work_order_id)}}">
                    Work Log
                </a></li></ul></div>
@stop
@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <i class="icon angle-left-icon back-link" onclick=" window.location = '{{ url('/admin/work_order/') }}';"></i>

                <h1>  Work Logs For Work Order # {{$work_order_id}}</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.work_log.create',$work_order_id) }}" class="btn btn-lg btn-primary">
                    Add Work Log
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('workLogGrid', 'YPL\Repair\DataGrids\WorkLogDataGrid')

            {!! $workLogGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

