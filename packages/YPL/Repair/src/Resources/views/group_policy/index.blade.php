@extends('repair::layouts.content')

@section('page_title')
    Group Policies
@stop
@section('tab')
    <div class="tabs"><ul><li class="active"><a href="{{route('admin.group_policy.index')}}">
                    Group Scheme
                </a></li> <li><a href="{{route('admin.policy.index')}}">
                    Policy
                </a></li></ul></div>
@stop
@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <i class="icon angle-left-icon back-link" onclick=" window.location = '{{ url('/admin/group_policy/') }}';"></i>

                <h1>Group  Policies</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.group_policy.create') }}" class="btn btn-lg btn-primary">
                    Add Group Policy
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('groupPolicyGrid', 'YPL\Repair\DataGrids\GroupPolicyDataGrid')

            {!! $groupPolicyGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

