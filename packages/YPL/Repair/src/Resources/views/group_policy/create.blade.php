@extends('repair::layouts.content')

@section('page_title')
    Add Group Policy
@stop

@section('tab')
    <div class="tabs"><ul><li class="active"><a href="{{route('admin.group_policy.index')}}">
                    Group
                </a></li> <li><a href="{{route('admin.policy.index')}}">
                    Policy
                </a></li></ul></div>
@stop
@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.group_policy.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Add Group Policy
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()




                    <accordian :title="'General'" :active="true">
                        <div slot="body">


                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') }}" data-vv-as="&quot;Name&quot;">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>


                            <div class="control-group" :class="[errors.has('is_required') ? 'has-error' : '']">
                                <label for="is_required" class="required">Status</label>
                                <select class="control" v-validate="'required'" id="status" name="status" data-vv-as="&quot;Is Required&quot;">
                                    <option value="1">
                                       Yes
                                    </option>
                                    <option value="0">
                                        No
                                    </option>
                                </select>
                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('is_required') }}</span>
                            </div>
                            <div class="control-group" :class="[errors.has('policy[]') ? 'has-error' : '']">
                                <label for="is_required" class="required">Policies</label>
                                <select class="control" multiple v-validate="'required'" id="status" name="policy_ids[]" data-vv-as="&quot;Is Required&quot;">
                                   @foreach($policies as $policy)
                                        <option value="{{$policy->id}}">{{$policy->name}}</option>
                                   @endforeach
                                </select>
                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('is_required') }}</span>
                            </div>


                        </div>
                    </accordian>



                </div>
            </div>

        </form>
    </div>

@stop
