@extends('admin::layouts.content')

@section('page_title')
   Product Brand
@stop
@section('tab')
    <div class="tabs"><ul><li><a href="{{route('admin.item.index')}}">
                    Product Model
                </a></li> <li  class="active"><a href="{{route('admin.item_category.index')}}">
                    Product Brand
                </a></li></ul></div>
@stop
@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Product Brand</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.item_category.create') }}" class="btn btn-lg btn-primary">
                    Add  Product Brand
                </a>
            </div>
        </div>


        <div class="page-content">
            {!! app('YPL\Repair\DataGrids\ItemCategoryDataGrid')->render() !!}
        </div>

    </div>
@stop