@extends('admin::layouts.content')

@section('page_title')
    Repair Categories
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Repair Categories</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.repair_category.create') }}" class="btn btn-lg btn-primary">
                    Add New Category
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('repairCategoryGrid', 'YPL\Repair\DataGrids\RepairCategoryDataGrid')

            {!! $repairCategoryGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

@push('scripts')
    @include('admin::export.export', ['gridName' => $repairCategoryGrid])
@endpush

