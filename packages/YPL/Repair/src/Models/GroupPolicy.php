<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\GroupPolicy as GroupPolicyContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class GroupPolicy extends Model implements GroupPolicyContract
{

    public $fillable = [
        'name'
    ];
    public function policies()
    {
        return $this->belongsToMany('YPL\Repair\Models\Policy', 'map_policies');

    }

}