<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\WorkOrderItem as WorkOrderItemContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class WorkOrderItem extends Model implements WorkOrderItemContract
{

    public $fillable = [
        'work_order_id',
        'item_repair_id',
        'item_repair_price',
        'item_id'
    ];


}