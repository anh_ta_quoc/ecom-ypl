<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Category\Contracts\CategoryTranslation as CategoryTranslationContract;

/**
 * Class RepairCategoryTranslation
 *
 * @package Webkul\RepairCategory\Models
 *
 * @property-read string $url_path maintained by database triggers
 */
class RepairCategoryTranslation extends Model implements CategoryTranslationContract
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'locale_id',
    ];
}