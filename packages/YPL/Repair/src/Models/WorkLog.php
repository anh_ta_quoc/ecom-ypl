<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;

use YPL\Repair\Contracts\WorkLog as WorkLogContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class WorkLog extends Model implements WorkLogContract
{

    protected $fillable = ['user_id','work_order_id','note','old_status','new_status'];

}