<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;

use YPL\Repair\Contracts\WorkOrder as WorkOrderContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class WorkOrder extends Model implements WorkOrderContract
{


    protected $fillable = ['customer_name', 'customer_phone', 'customer_email', 'customer_address',
        'product_name', 'product_imei', 'product_brand', 'product_status',
        'note', 'status', 'job_status', 'type',
        'est_price', 'est_date', 'est_plan', 'budget',
        'shop_id', 'insurance_id', 'partner_id',
        'repair_shop_id', 'repair_type', 'repair_person_id',
        'checklists', 'policies', 'tracking_number',
        'group_policy_id','storage_location','repair_location',
        'invoice_status','urgent_status','part_request','borrow_imei','borrow_model','borrow_price','borrow_note'];

    public const STATUS_SUBMITTED = 'Submitted';
    public const STATUS_POSTAGE_INSTRUCTIONS_ISSUED = 'Postage instruction issued';
    public const STATUS_ALLOCATED = 'Allocated';
    public const STATUS_DELIVERED_TO_WAREHOUSE = 'Delivered to warehouse';

    public const STATUS_ESTIMATED_REQUIRED = 'Estimated Required';
    public const STATUS_ESTIMATED_QUOTED = 'Estimated Quoted';
    public const STATUS_ESTIMATED_ACCEPTED = 'Estimated Accepted';
//    public const STATUS_ASSIGNED = 'Assigned';
    public const STATUS_UNDER_REPAIR = 'Under Repair';
    public const STATUS_PART_REQUEST = 'Part Request';
    public const STATUS_PART_ORDERED = 'Part Ordered';
    public const STATUS_PART_IN_STOCK = 'Part In Stock';
    public const STATUS_TECHNICIAN_COMPLETED = 'Technician Completed';
    public const STATUS_UNIT_ON_TEST = 'Unit on Test';
    public const STATUS_WAITING_FOR_PAYMENT = 'Job Completed';
    public const STATUS_CANCELED_OTHER = 'Cancelled';
    public const STATUS_SAVAGE_FOR_PART = 'Savage for Part';


    public const JOB_STATUS_OPENED = 'Opened';
    public const JOB_STATUS_CLOSED = 'Closed';
    public const JOB_STATUS_CANCELLED = 'Cancelled';

    public const INVOICE_STATUS_WAITING = 'Waiting For Payment';
    public const INVOICE_STATUS_PAID = 'Paid';

    public const REPAIR_TYPE_SHOP = 'Shop';
    public const REPAIR_TYPE_PARTNER = 'Partner';


    public function canIssue(): bool
    {
        if ($this->status === self::STATUS_SUBMITTED && auth()->guard('admin')->user()->role_id==1) {
            return true;
        }

        return false;
    }

    public function canAllocate(): bool
    {
        if (($this->status === self::STATUS_POSTAGE_INSTRUCTIONS_ISSUED || $this->status === self::STATUS_SUBMITTED) &&
            auth()->guard('admin')->user()->role_id==1
        ){
            return true;
        }
        return false;
    }

    public function canDeliver(): bool
    {
        if (($this->status === self::STATUS_ALLOCATED ||
            $this->status === self::STATUS_POSTAGE_INSTRUCTIONS_ISSUED ||
            $this->status === self::STATUS_SUBMITTED) &&
            auth()->guard('admin')->user()->role_id==1) {
            return true;
        }
        return false;
    }
//    public function canAssignment(): bool
//    {
//        if ($this->status === self::STATUS_ACCEPTED && $this->repair_shop_id == null) {
//            return true;
//        }
//        return false;
//    }
    public function canRequestPart(): bool
    {
        if ($this->status === self::STATUS_UNDER_REPAIR && ($this->repair_type == WorkOrder::REPAIR_TYPE_SHOP or
                $this->repair_type == null)) {
            return true;
        }
        return false;
    }
    public function canOrderPart(): bool
    {
        if ($this->status === self::STATUS_PART_REQUEST) {
            return true;
        }
        return false;
    }
    public function canSavePart(): bool
    {
        if ($this->status === self::STATUS_PART_ORDERED) {
            return true;
        }
        return false;
    }
    public function canSavagePart(): bool
    {
        if ($this->status === self::STATUS_PART_IN_STOCK || $this->status === self::STATUS_UNIT_ON_TEST ||$this->status === self::STATUS_TECHNICIAN_COMPLETED) {
            return true;
        }
        return false;
    }

    public function canRepair(): bool
    {
        if ($this->status === self::STATUS_ESTIMATED_ACCEPTED &&
            $this->repair_shop_id == null &&
            (auth()->guard('admin')->user()->type=='shop' or auth()->guard('admin')->user()->type==null)) {
            return true;
        }
        if (auth()->guard('admin')->user()->type=='repair_shop' &&
            auth()->guard('admin')->user()->repair_shop_id==$this->repair_shop_id &&
            $this->status === self::STATUS_ESTIMATED_ACCEPTED) {
            return true;
        }

        return false;
    }
    public function canAssignment(): bool
    {
        if ($this->status === self::STATUS_ESTIMATED_ACCEPTED && $this->repair_shop_id == null) {
            return true;
        }
        return false;
    }

    public function canEstimate(): bool
    {
        if ($this->status === self::STATUS_DELIVERED_TO_WAREHOUSE && auth()->guard('admin')->user()->role_id==1) {
            return true;
        }
        return false;
    }

    public function canEstimateQuote(): bool
    {
        if ($this->status === self::STATUS_ESTIMATED_REQUIRED && (auth()->guard('admin')->user()->role_id==1
            or auth()->guard('admin')->user()->type == 'shop' )) {
            return true;
        }
        return false;
    }
    public function canUnitTest():bool
    {
        if (($this->status === self::STATUS_PART_IN_STOCK or $this->status === self::STATUS_UNDER_REPAIR)

        ){
            return true;
        }
        return false;
    }

    public function canReturn(): bool
    {
        if ($this->status === self::STATUS_UNDER_REPAIR ) {
            return true;
        }
        if ($this->status === self::STATUS_ESTIMATED_ACCEPTED && $this->repair_shop_id != null){
            return true;
        }
        if ($this->status === self::STATUS_ESTIMATED_ACCEPTED && auth()->guard('admin')->user()->role_id==1 ){
            return true;
        }

        return false;
    }
//    public function canAccept():bool
//    {
//        if ($this->status === self::STATUS_UNDER_REPAIR) {
//            return true;
//        }
//        return false;
//    }
    public function canReject(): bool
    {
        if ($this->status != self::STATUS_CANCELED_OTHER && $this->status != self::STATUS_TECHNICIAN_COMPLETED  && $this->status != self::STATUS_SAVAGE_FOR_PART) {
            return true;
        }
        return false;
    }

    public function canEstimateAccept(): bool
    {
        if ($this->status === self::STATUS_ESTIMATED_QUOTED) {
            return true;
        }
        return false;
    }
    public function canEstimateReject(): bool
    {
        if ($this->status === self::STATUS_ESTIMATED_QUOTED) {
            return true;
        }
        return false;
    }

    public function canComplete(): bool
    {
        if ($this->status === self::STATUS_UNIT_ON_TEST) {
            return true;
        }
        return false;
    }

    public function canUpdateInvoice(): bool
    {
        if ($this->status === self::STATUS_TECHNICIAN_COMPLETED && $this->status != self::STATUS_CANCELED_OTHER) {
            return true;
        }
        return false;
    }

    public function item_repairs()
    {
        return $this->belongsToMany('YPL\Repair\Models\ItemRepair', 'work_order_items');

    }
    public function group_policy()
    {
        return $this->belongsTo('YPL\Repair\Models\GroupPolicy','group_policy_id');
    }

}