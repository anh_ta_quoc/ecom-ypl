<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\Shop as ShopContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class Shop extends Model implements ShopContract
{

    public $fillable = [
        'name',
        'location',
    ];


}