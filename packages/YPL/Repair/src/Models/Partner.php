<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\Partner as PartnerContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class Partner extends Model implements PartnerContract
{

    public $fillable = [
        'name',
        'location',
        'type',
        'phone',
        'noti_email'
    ];
    public const TYPE_REPAIR = 'Repair Partner';
    public const TYPE_INSURANCE = 'Insurance';
    public const TYPE_PARTNER = 'Partner';


}