<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\Policy as PolicyContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class Policy extends Model implements PolicyContract
{

    public $fillable = [
        'name',
        'is_required'
    ];


}