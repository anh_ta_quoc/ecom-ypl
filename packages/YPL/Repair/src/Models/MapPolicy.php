<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\MapPolicy as MapPolicyContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class MapPolicy extends Model implements MapPolicyContract
{

    public $fillable = [
        'policy_id',
        'group_policy_id',
        'status'
    ];


}