<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\ItemCategory as ItemCategoryContract;
/**
 * Class ItemCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class ItemCategory extends Model implements ItemCategoryContract
{

    public $fillable = [
        'name',
        'parent_id'
    ];
    public function childs() {

        return $this->hasMany('YPL\Repair\Models\ItemCategory','parent_id','id') ;

    }


}