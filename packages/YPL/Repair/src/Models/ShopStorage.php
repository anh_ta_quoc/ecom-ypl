<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\ShopStorage as ShopStorageContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class ShopStorage extends Model implements ShopStorageContract
{

    public $fillable = [
        'location',
        'shop_id'
    ];


}