<?php

namespace YPL\Repair\Models;

use Webkul\Core\Eloquent\TranslatableModel;

use YPL\Repair\Contracts\RepairCategory as RepairCategoryContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class RepairCategory extends TranslatableModel implements RepairCategoryContract
{

    public $translatedAttributes = [
        'name',
        'description',
    ];

    protected $fillable = ['position', 'status'];

    protected $with = ['translations'];

    /**
     * Get image url for the category image.
     */


     /**
     * The filterable attributes that belong to the category.
     */



}