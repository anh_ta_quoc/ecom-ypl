<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\Item as ItemContract;
use Illuminate\Support\Facades\Storage;
/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class Item extends Model implements ItemContract
{

    public $fillable = [
        'name',
        'item_category_id'
    ];

    public function item_category()
    {
        return $this->belongsTo('YPL\Repair\Models\ItemCategory','item_category_id');
    }
    public function item_repairs()
    {
        return $this->hasMany('YPL\Repair\Models\ItemRepair','item_id');
    }

    public function image_url()
    {
        if (! $this->feature_image) {
            return;
        }

        return Storage::url($this->feature_image);
    }

    /**
     * Get image url for the category image.
     */
    public function getImageUrlAttribute()
    {
        return $this->image_url();
    }
}