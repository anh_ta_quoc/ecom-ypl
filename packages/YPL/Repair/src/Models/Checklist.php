<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\Checklist as ChecklistContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class Checklist extends Model implements ChecklistContract
{

    public $fillable = [
        'name',
        'is_required'
    ];


}