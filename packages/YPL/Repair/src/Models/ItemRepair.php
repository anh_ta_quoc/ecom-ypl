<?php

namespace YPL\Repair\Models;

use Illuminate\Database\Eloquent\Model;
use YPL\Repair\Contracts\ItemRepair as ItemRepairContract;

/**
 * Class RepairCategory
 *
 * @package  YPL\Repair\Models
 *
 */
class ItemRepair extends Model implements ItemRepairContract
{

    public $fillable = [
        'name',
        'price',
        'item_id'
    ];
    public function item()
    {
        return $this->belongsTo('YPL\Repair\Models\Item','item_id');
    }

}