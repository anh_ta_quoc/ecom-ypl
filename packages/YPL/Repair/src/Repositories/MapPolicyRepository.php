<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class MapPolicyRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\MapPolicy';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\MapPolicy
     */
    public function create(array $data)
    {


        $map_policy = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $map_policy;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\Policy
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $map_policy = $this->find($id);


        $map_policy->update($data);



        return $map_policy;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }



}