<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class WorkOrderItemRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\WorkOrderItem';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\WorkOrderItem
     */
    public function create(array $data)
    {


        $work_order_item = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $work_order_item;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\WorkOrderItem
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $work_order_item = $this->find($id);


        $work_order_item->update($data);



        return $work_order_item;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }



}