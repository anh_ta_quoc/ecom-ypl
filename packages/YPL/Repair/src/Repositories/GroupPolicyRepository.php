<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class GroupPolicyRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\GroupPolicy';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\GroupPolicy
     */
    public function create(array $data)
    {


        $group_policy = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $group_policy;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\Policy
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $group_policy = $this->find($id);


        $group_policy->update($data);



        return $group_policy;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }



}