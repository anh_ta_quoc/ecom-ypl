<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class RepairCategoryRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\RepairCategory';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\RepairCategory
     */
    public function create(array $data)
    {
//        Event::dispatch('catalog.category.create.before');

        if (isset($data['locale']) && $data['locale'] == 'all') {
            $model = app()->make($this->model());

            foreach (core()->getAllLocales() as $locale) {
                foreach ($model->translatedAttributes as $attribute) {
                    if (isset($data[$attribute])) {
                        $data[$locale->code][$attribute] = $data[$attribute];
                        $data[$locale->code]['locale_id'] = $locale->id;
                    }
                }
            }
        }

        $category = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $category;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\RepairCategory
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $category = $this->find($id);

//        Event::dispatch('catalog.category.update.before', $id);

        $category->update($data);

//        $this->uploadImages($data, $category);

//        if (isset($data['attributes'])) {
//            $category->filterableAttributes()->sync($data['attributes']);
//        }
//
//        Event::dispatch('catalog.category.update.after', $id);

        return $category;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//        Event::dispatch('catalog.category.delete.before', $id);
//
        parent::delete($id);

//        Event::dispatch('catalog.category.delete.after', $id);
    }


    public function getPartial($columns = null)
    {
        $categories = $this->model->all();

        $trimmed = [];

        foreach ($categories as $key => $category) {
            if ($category->name != null || $category->name != "") {
                $trimmed[$key] = [
                    'id'   => $category->id,
                    'name' => $category->name,
                    'slug' => $category->slug,
                ];
            }
        }

        return $trimmed;
    }
}