<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class ItemRepairRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\ItemRepair';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\ItemRepair
     */
    public function create(array $data)
    {


        $item_repair = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $item_repair;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\ItemRepair
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $item_repair = $this->find($id);


        $item_repair->update($data);



        return $item_repair;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }



}