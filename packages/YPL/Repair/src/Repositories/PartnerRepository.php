<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class PartnerRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\Partner';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\Partner
     */
    public function create(array $data)
    {


        $work_order = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $work_order;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\Partner
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $work_order = $this->find($id);


        $work_order->update($data);



        return $work_order;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }



}