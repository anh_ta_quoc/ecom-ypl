<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class WorkLogRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\WorkLog';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\WorkLog
     */
    public function create(array $data)
    {


        $work_log = $this->model->create($data);




//        Event::dispatch('catalog.category.create.after', $category);

        return $work_log;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\WorkLog
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $work_log = $this->find($id);


        $work_log->update($data);



        return $work_log;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }



}