<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class ItemCategoryRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\ItemCategory';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\ItemCategory
     */
    public function create(array $data)
    {

        $item_category = $this->model->create($data);


        return $item_category;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\ItemCategory
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $work_order_item = $this->find($id);


        $work_order_item->update($data);



        return $work_order_item;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }

    public function getRootCategories()
    {
        return $this->getModel()->where('parent_id', NULL)->get();
    }

    /**
     * get visible category tree
     *
     * @param  int  $id
     * @return \Illuminate\Support\Collection
     */



}