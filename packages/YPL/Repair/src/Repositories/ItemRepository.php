<?php

namespace YPL\Repair\Repositories;
use Webkul\Core\Eloquent\Repository;


class ItemRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'YPL\Repair\Contracts\Item';
    }

    /**
     * @param  array  $data
     * @return \YPL\Repair\Contracts\Item
     */
    public function create(array $data)
    {


        $item = $this->model->create($data);

        $this->uploadImages($data, $item);



//        Event::dispatch('catalog.category.create.after', $category);

        return $item;
    }



    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \YPL\Repair\Contracts\Item
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $item = $this->find($id);


        $item->update($data);

        $this->uploadImages($data, $item);


        return $item;
    }

    /**
     * @param  int  $id
     * @return void
     */
    public function delete($id)
    {
//
        parent::delete($id);

    }
    public function uploadImages($data, $item, $type = "feature_image")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'item/' . $item->id;

                if ($request->hasFile($file)) {
                    if ($item->{$type}) {
                        Storage::delete($item->{$type});
                    }

                    $item->{$type} = $request->file($file)->store($dir);
                    $item->save();
                }
            }
        } else {
            if ($item->{$type}) {
                Storage::delete($item->{$type});
            }

            $item->{$type} = null;
            $item->save();
        }
    }



}