<?php

namespace YPL\Repair\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use YPL\Repair\Models\Partner;


class NewAssignmentNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @param  \YPL\Repair\Contracts\WorkOrder  $work_order
     */
    public $work_order;

    /**
     * Create a new message instance.
     *
     * @param  \YPL\Repair\Contracts\WorkOrder  $work_order
     * @return void
     */
    public function __construct($work_order)
    {
        $this->work_order = $work_order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $work_order = $this->work_order;
        $repair_shop_id = $work_order->repair_shop_id;
        $partner = Partner::findOrFail($repair_shop_id);
        if ($partner->noti_email != null) {
            return $this->from(core()->getSenderEmailDetails()['email'], core()->getSenderEmailDetails()['name'])
                ->to($work_order->customer_email, $work_order->customer_name)
                ->subject('Quotation For Work Order #' . $work_order->id)
                ->view('repair::emails.quotation');
        } else {
            return null;
        }
    }
}
