<?php

namespace YPL\Repair\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCompleteWorkOrderNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @param  \YPL\Repair\Contracts\WorkOrder  $work_order
     */
    public $work_order;

    /**
     * Create a new message instance.
     *
     * @param  \YPL\Repair\Contracts\WorkOrder  $work_order
     * @return void
     */
    public function __construct($work_order)
    {
        $this->work_order = $work_order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $work_order = $this->work_order;

        return $this->from(core()->getSenderEmailDetails()['email'], core()->getSenderEmailDetails()['name'])
                    ->to($work_order->customer_email, $work_order->customer_name)
                    ->subject('Complete Work Order #'.$work_order->id)
                    ->view('repair::emails.completeWO');
    }
}
