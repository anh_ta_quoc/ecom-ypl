<?php

namespace YPL\Repair\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequestPartNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The invoice instance.
     *
     * @param  \YPL\Repair\Contracts\WorkOrder  $work_order
     */
    public $work_orders;

    /**
     * Create a new message instance.
     *
     * @param  \YPL\Repair\Contracts\WorkOrder  $work_order
     * @return void
     */
    public function __construct($work_orders)
    {
        $this->work_orders = $work_orders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $work_orders = $this->work_orders;
        $name = 'Admin';
        $email = 'Admin@jtechstore.com.au';
        return $this->from(core()->getSenderEmailDetails()['email'], core()->getSenderEmailDetails()['name'])
                    ->to($email, $name)
                    ->subject('Need Request Part For Work Orders')
                    ->view('repair::emails.request_part');
    }
}
