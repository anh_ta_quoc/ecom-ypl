<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;

use Mail;
use YPL\Repair\Repositories\PolicyRepository;

class ApiController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $policyRepository;


    public function __construct(
        PolicyRepository $policyRepository

    )
    {
        $this->_config = request('_config');

        $this->policyRepository = $policyRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index($insurance_id)
    {
       $policies = $this->policyRepository->where('partner_id',$insurance_id)->get();
        return $policies;
    }





}