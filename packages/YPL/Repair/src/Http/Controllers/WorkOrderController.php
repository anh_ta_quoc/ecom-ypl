<?php

namespace YPL\Repair\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;
use Mail;
use YPL\Repair\Mail\NewCompleteWorkOrderNotification;
use YPL\Repair\Mail\NewQuotationNotification;
use YPL\Repair\Mail\NewRequestPartNotification;
use YPL\Repair\Mail\NewUrgentStatusNotification;
use YPL\Repair\Models\Partner;
use YPL\Repair\Models\WorkLog;
use YPL\Repair\Models\WorkOrder;
use YPL\Repair\Repositories\ChecklistRepository;
use YPL\Repair\Repositories\WorkOrderRepository;
use YPL\Repair\Repositories\ShopRepository;
use YPL\Repair\Repositories\PartnerRepository;
use YPL\Repair\Repositories\PolicyRepository;
use YPL\Repair\Repositories\WorkLogRepository;
use YPL\Repair\Repositories\GroupPolicyRepository;
use Excel;
use YPL\Repair\Imports\WorkOrderImport;

class WorkOrderController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $workOrderRepository;
    protected $partnerRepository;
    protected $shopRepository;
    protected $policyRepository;
    protected $checklistRepository;
    protected $workLogRepository;
    protected $groupPolicyRepository;


    public function __construct(
        WorkOrderRepository $workOrderRepository,
        ShopRepository $shopRepository,
        PartnerRepository $partnerRepository,
        PolicyRepository $policyRepository,
        ChecklistRepository $checklistRepository,
        WorkLogRepository $workLogRepository,
        GroupPolicyRepository $groupPolicyRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->workOrderRepository = $workOrderRepository;
        $this->shopRepository = $shopRepository;
        $this->partnerRepository = $partnerRepository;
        $this->policyRepository = $policyRepository;
        $this->checklistRepository = $checklistRepository;
        $this->workLogRepository = $workLogRepository;
        $this->groupPolicyRepository = $groupPolicyRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $shops = $this->shopRepository->all();
        $partners = $this->partnerRepository->where('type', 'partner')->get();
        $insurances = $this->partnerRepository->where('type', 'insurance')->get();
        if (count($insurances) > 0) {
            $policies = $this->policyRepository->where('partner_id', $insurances->first()->id)->get();
        } else {
            $policies = [];

        }
        if (auth()->guard('admin')->user()->type == 'insurance') {
            $policies = $this->policyRepository->where('partner_id', auth()->guard('admin')->user()->insurance_id)->get();
        }


        return view($this->_config['view'], compact('shops', 'partners', 'insurances', 'policies'));
    }

    public function create_batch()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $this->validate(request(), [
            'customer_name' => 'string|required',
            'customer_email' => 'string|required',
            'customer_phone' => 'string|required',
            'product_name' => 'string|required',
            'product_brand' => 'string|required',
            'product_imei' => 'string|required'
        ]);
        $input = request()->all();

        $input['job_status'] = WorkOrder::JOB_STATUS_OPENED;
        $input['status'] = WorkOrder::STATUS_SUBMITTED;
        $work_order = $this->workOrderRepository->create($input);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Work Order']));

        return redirect()->route($this->_config['redirect']);
    }

    public function store_batch()
    {

        Excel::import(new WorkOrderImport(), request()->file('import_file'));

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Work Order']));
        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $group_policies = $this->groupPolicyRepository->all();

        return view($this->_config['view'], compact('work_order', 'group_policies'));
    }

    public function view($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $repair_shops = $this->partnerRepository->where('type', Partner::TYPE_REPAIR)->get();
        $checklists = $this->checklistRepository->all();

        return view($this->_config['view'], compact('work_order', 'repair_shops', 'checklists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $work_order = $this->workOrderRepository->update(request()->all(), $id);
        $input = request()->all();
        if(isset($input['urgent_status']) &&$input['urgent_status'] == 'Yes' ) {
            try {
                $work_orders = $this->workOrderRepository->where('urgent_status','Yes')->get();
                Mail::queue(new NewUrgentStatusNotification($work_orders));
            } catch (\Exception $e) {
                report($e);
            }
        }
        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Work Order']));

        return redirect()->route($this->_config['redirect'], $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);

        try {
            $this->workOrderRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Work Order']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Work Order']));
        }

        return response()->json(['message' => false], 400);
    }

    public function issue($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_POSTAGE_INSTRUCTIONS_ISSUED;
        $work_order->save();
        $new_status = $work_order->status;
        /*
         *
         */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from SUBMITTED to POSTAGE_INSTRUCTION_ISSUED';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Collect device For Work Order # ' . $work_order->id . ' success ');
        return redirect()->route($this->_config['redirect'], $work_order->id);

    }

    public function allocate($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_ALLOCATED;
        $work_order->product_status = request()->product_status;
        $work_order->save();
        $new_status = $work_order->status;
        /*
         *
         */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from '.$old_status.' to ALLOCATED';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Allocate device For Work Order # ' . $work_order->id . ' success ');
        return redirect()->route($this->_config['redirect'], $work_order->id);

    }

    public function delivery($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_DELIVERED_TO_WAREHOUSE;
        $work_order->product_status = request()->product_status;
        $work_order->save();
        $new_status = $work_order->status;
        /*
         *
         */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;// 'Update WorkOrder from '.$old_status.' to DELIVERED_TO_WAREHOUSE';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Delivery device For Work Order # ' . $work_order->id . ' to warehouse success ');
        return redirect()->route($this->_config['redirect'], $work_order->id);

    }

    public function needToEstimate($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_ESTIMATED_REQUIRED;
        $work_order->save();
        $new_status = $work_order->status;
        /*
         *
         */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from DELIVERED_TO_WAREHOUSE to ESTIMATE_REQUIRED';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Require to estimate For Work Order # ' . $work_order->id . ' to warehouse success ');
        return redirect()->route($this->_config['redirect'], $work_order->id);

    }

    public function sendQuotation($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        if (request()->est_date != null && request()->est_date != '') {
            $work_order->est_date = Carbon::createFromFormat('Y-m-d', request()->est_date);
        }
        $work_order->est_price = request()->est_price;
        $work_order->est_plan = request()->est_plan;
        $work_order->status = WorkOrder::STATUS_ESTIMATED_QUOTED;
        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from ESTIMATED_REQUIRED to ESTIMATED_QUOTED';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        if ($work_order->type != 'insurance') {
            try {
                Mail::queue(new NewQuotationNotification($work_order));
            } catch (\Exception $e) {
                report($e);
            }
        } else {
            if ($work_order->est_price <= $work_order->budget) {
                $old_status = $work_order->status;

                $work_order->status = WorkOrder::STATUS_ESTIMATED_ACCEPTED;
                $work_order->save();
                $new_status = $work_order->status;

                $work_log = [];
                $work_log['work_order_id'] = $work_order->id;
                $work_log['user_id'] = auth()->guard('admin')->user()->id;
                $work_log['note'] = 'System Auto Update WorkOrder from ESTIMATED_QUOTED to ESTIMATED_ACCEPTED';
                $work_log['old_status'] = $old_status;
                $work_log['new_status'] = $new_status;
                $this->workLogRepository->create($work_log);

            } else {
                $old_status = $work_order->status;

                $work_order->status = WorkOrder::STATUS_CANCELED_OTHER;
                $work_order->save();
                $new_status = $work_order->status;

                $work_log = [];
                $work_log['work_order_id'] = $work_order->id;
                $work_log['user_id'] = auth()->guard('admin')->user()->id;
                $work_log['note'] = 'System Auto Update WorkOrder from ESTIMATED_QUOTED to CANCELED_OTHER';
                $work_log['old_status'] = $old_status;
                $work_log['new_status'] = $new_status;
                $this->workLogRepository->create($work_log);
            }
        }


        session()->flash('success', 'Update Quotation For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function assignment($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $partner = $this->partnerRepository->findOrFail(request()->repair_shop_id);
        if ($partner == null) {
            session()->flash('error', 'Repair shop not existed');
        }
        $work_order->repair_shop_id = request()->repair_shop_id;
        $work_order->repair_type = WorkOrder::REPAIR_TYPE_PARTNER;
        $work_order->save();

        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = 'Assigned WorkOrder at ESTIMATED_ACCEPT status to Repair Shop : ' . $partner->name;
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $old_status;
        $this->workLogRepository->create($work_log);
//        try {
//
//            Mail::queue(new NewAssignmentNotification($work_order));
//
//        } catch (\Exception $e) {
//            report($e);
//        }

        session()->flash('success', 'Assign For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function claim($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_UNDER_REPAIR;
        $type = auth()->guard('admin')->user()->type;
        if ($type == null) {
            $type = 'shop';
        }
        if ($type == 'shop') {
            $work_order->repair_type = WorkOrder::REPAIR_TYPE_SHOP;
            $work_order->repair_shop_id = auth()->guard('admin')->user()->shop_id ?: 1;
        } else {
            $work_order->repair_type = WorkOrder::REPAIR_TYPE_PARTNER;

        }
        $work_order->repair_person_id = auth()->guard('admin')->user()->id;

        $work_order->save();
        $new_status = $work_order->status;
        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from ESTIMATED_ACCEPTED to UNDER_REPAIR';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Claim To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function needToPart($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;

        $work_order->status = WorkOrder::STATUS_PART_REQUEST;
        $work_order->part_request = request('part_request');
        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $work_log['note'] = request()->note;//'Update WorkOrder from UNDER_REPAIR to PART_REQUEST';

        $this->workLogRepository->create($work_log);
        try {
            $work_orders = $this->workOrderRepository->where('status',WorkOrder::STATUS_PART_REQUEST)->get();

            Mail::queue(new NewRequestPartNotification($work_orders));

        } catch (\Exception $e) {
            report($e);
        }
        session()->flash('success', 'Request Part For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function orderPart($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;

        $work_order->status = WorkOrder::STATUS_PART_ORDERED;
        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from PART_REQUEST to PART_ORDERED';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Order Part For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function savePart($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;

        $work_order->status = WorkOrder::STATUS_PART_IN_STOCK;
        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;// 'Update WorkOrder from PART_ORDERED to PART_IN_STOCK ';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        if (request()->note) {
            $work_log['note'] = $work_log['note'] . ':' . request()->note;
        }

        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Save Part For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function savagePart($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;

        $work_order->status = WorkOrder::STATUS_SAVAGE_FOR_PART;
        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;// 'Update WorkOrder from PART_ORDERED to PART_IN_STOCK ';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        if (request()->note) {
            $work_log['note'] = $work_log['note'] . ':' . request()->note;
        }

        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Savage For Part In Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function unitTest($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_UNIT_ON_TEST;
        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;//'Update WorkOrder from PART_IN_STOCK to UNIT_TEST ';
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Unit Test For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function complete($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;

        $work_order->status = WorkOrder::STATUS_TECHNICIAN_COMPLETED;
        $work_order->job_status = WorkOrder::JOB_STATUS_CLOSED;

        $work_order->save();
        $new_status = $work_order->status;

        /*
        *
        */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        $this->workLogRepository->create($work_log);
        try {

            Mail::queue(new NewCompleteWorkOrderNotification($work_order));

        } catch (\Exception $e) {
            report($e);
        }
        session()->flash('success', 'Complete For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }


    public function return($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
//        if ($work_order->repair_person_id != auth()->guard('admin')->user()->id) {
//            session()->flash('error', 'Can\'t return to Repair for Work Order # ' . $work_order->id . ' because of wrong permission ');
//        }

        $work_order->status = WorkOrder::STATUS_ESTIMATED_ACCEPTED;
        $work_order->repair_shop_id = null;
        $work_order->repair_type = null;
        $work_order->repair_person_id = null;
        $work_order->save();
        $new_status = $work_order->status;
        /*
            *
            */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Cancel To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function reject($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;
        $work_order->status = WorkOrder::STATUS_CANCELED_OTHER;
        $work_order->job_status = WorkOrder::JOB_STATUS_CANCELLED;

        $work_order->save();
        $new_status = $work_order->status;

        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note;
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;

        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Rejected To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function accept($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $old_status = $work_order->status;

        $work_order->status = WorkOrder::STATUS_ESTIMATED_ACCEPTED;
        $work_order->save();
        $new_status = $work_order->status;

        /*
               *
               */
        $work_log = [];
        $work_log['work_order_id'] = $work_order->id;
        $work_log['user_id'] = auth()->guard('admin')->user()->id;
        $work_log['note'] = request()->note; //Update WorkOrder from ESTIMATED_QUOTED to ESTIMATED_ACCEPTED '
        $work_log['old_status'] = $old_status;
        $work_log['new_status'] = $new_status;
        $this->workLogRepository->create($work_log);
        session()->flash('success', 'Accept To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function rejectInvoice($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $work_order->job_status = WorkOrder::JOB_STATUS_CLOSED;
        $work_order->save();

        session()->flash('success', 'Close invoice To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }

    public function completeInvoice($id)
    {
        $work_order = $this->workOrderRepository->findOrFail($id);
        $work_order->job_status = WorkOrder::JOB_STATUS_COMPLETED;
        $work_order->save();

        session()->flash('success', 'Complete invoice To Repair For Work Order # ' . $work_order->id . ' success ');

        return redirect()->route($this->_config['redirect'], $work_order->id);
    }


    public function changeJobStatus()
    {
        $data = request()->all();


        if (!isset($data['id']) && !isset($data['job_status'])) {
            return response()->json([
                'status' => false,
                'message' => 'Fail : Wrong information'
            ]);
        }
        $workOrder = $this->workOrderRepository->findOrFail($data['id']);

        //TODO : validator status
        $workOrder->job_status = $data['job_status'];
        $workOrder->save();


        return response()->json([
            'status' => true,
            'message' => 'Success: Work Order updated'
        ]);

    }


}