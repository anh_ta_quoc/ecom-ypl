<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;

use Mail;
use YPL\Repair\Repositories\ItemRepairRepository;
use YPL\Repair\Repositories\ItemRepository;

class ItemRepairController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $itemRepairRepository;
    protected $itemRepository;



    public function __construct(
        ItemRepairRepository $itemRepairRepository,
        ItemRepository $itemRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->itemRepairRepository = $itemRepairRepository;
        $this->itemRepository = $itemRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index($item_id)
    {
        $item = $this->itemRepository->findOrFail($item_id);
        return view($this->_config['view'], compact('item_id','item'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($item_id)
    {
        $item = $this->itemRepository->findOrFail($item_id);
        return view($this->_config['view'], compact('item_id','item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($item_id)
    {

        $this->validate(request(), [

            'name' => 'string|required',
            'price' => 'required',
        ]);
        $input = request()->all();

        $input['item_id'] =  $item_id;

        $partner = $this->itemRepairRepository->create($input);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'ItemRepair']));

        return redirect()->route($this->_config['redirect'], compact('item_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item_repair = $this->itemRepairRepository->findOrFail($id);


        return view($this->_config['view'], compact('item_repair'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


        $this->validate(request(), [
            'name' => 'string|required',
            'price' => 'required',
        ]);
        $item_repair = $this->itemRepairRepository->update(request()->all(), $id);
        $item_id = $item_repair->item_id;
        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'ItemRepair']));

        return redirect()->route($this->_config['redirect'],compact('item_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemRepair = $this->itemRepairRepository->findOrFail($id);

        try {
            $this->itemRepairRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'ItemRepair']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'ItemRepair']));
        }

        return response()->json(['message' => false], 400);
    }





}