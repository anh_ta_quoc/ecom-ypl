<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use YPL\Repair\Repositories\ItemCategoryRepository;

class ItemCategoryController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CategoryRepository object
     *
     * @var \Webkul\Category\Repositories\CategoryRepository
     */
    protected $itemCategoryRepository;




    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Category\Repositories\ItemCategory  $itemCategoryRepository
     * @return void
     */
    public function __construct(
        ItemCategoryRepository $itemCategoryRepository
    )
    {
        $this->itemCategoryRepository = $itemCategoryRepository;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->itemCategoryRepository->all();

        return view($this->_config['view'], compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
//        $this->validate(request(), [
//            'slug'        => ['required', 'unique:category_translations,slug', new \Webkul\Core\Contracts\Validations\Slug],
//            'name'        => 'required',
//            'image.*'     => 'mimes:jpeg,jpg,bmp,png',
//            'description' => 'required_if:display_mode,==,description_only,products_and_description',
//        ]);

        $this->itemCategoryRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Category']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item_category = $this->itemCategoryRepository->findOrFail($id);

        $categories = $this->itemCategoryRepository->all();


        return view($this->_config['view'], compact('item_category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


        $this->itemCategoryRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Category']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->itemCategoryRepository->findOrFail($id);

        if(strtolower($category->name) == "root") {
            session()->flash('warning', trans('admin::app.response.delete-category-root', ['name' => 'Category']));
        } else {
            try {

                $this->itemCategoryRepository->delete($id);

                return response()->json(['message' => true], 200);
            } catch(\Exception $e) {
                session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Category']));
            }
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Remove the specified resources from database
     *
     * @return \Illuminate\Http\Response
     */

}