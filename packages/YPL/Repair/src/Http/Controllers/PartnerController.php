<?php

namespace YPL\Repair\Http\Controllers;



use Mail;
use YPL\Repair\Repositories\PartnerRepository;

class PartnerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $partnerRepository;


    public function __construct(
        PartnerRepository $partnerRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->partnerRepository = $partnerRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $this->validate(request(), [

            'name' => 'string|required',
            'location' => 'string|required',
        ]);

        $partner = $this->partnerRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Partner']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $partner = $this->partnerRepository->findOrFail($id);


        return view($this->_config['view'], compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $this->validate(request(), [

            'name' => 'string|required',
            'location' => 'string|required',
        ]);

        $this->partnerRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Partner']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = $this->partnerRepository->findOrFail($id);

        try {
            $this->partnerRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Partner']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Partner']));
        }

        return response()->json(['message' => false], 400);
    }





}