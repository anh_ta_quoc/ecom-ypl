<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;

use Mail;
use YPL\Repair\Repositories\PolicyRepository;

class PolicyController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $policyRepository;


    public function __construct(
        PolicyRepository $policyRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->policyRepository = $policyRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $this->validate(request(), [

            'name' => 'string|required',
        ]);
        $input = request()->all();
        $this->policyRepository->create($input);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Policy']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $policy = $this->policyRepository->findOrFail($id);


        return view($this->_config['view'], compact('policy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


        $this->validate(request(), [

            'name' => 'string|required',
        ]);

        $policy =  $this->policyRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Policy']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $policy = $this->policyRepository->findOrFail($id);

        try {
            $this->policyRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Policy']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Policy']));
        }

        return response()->json(['message' => false], 400);
    }





}