<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;

use Mail;
use YPL\Repair\Repositories\ItemRepository;
use YPL\Repair\Repositories\ItemCategoryRepository;

class ItemController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $itemRepository;
    protected $itemCategoryRepository;


    public function __construct(
        ItemRepository $itemRepository,
        ItemCategoryRepository $itemCategoryRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->itemRepository = $itemRepository;
        $this->itemCategoryRepository = $itemCategoryRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->itemCategoryRepository->all();
        return view($this->_config['view'], compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $this->validate(request(), [

            'name' => 'string|required',
            'item_category_id' => 'required',
        ]);

        $partner = $this->itemRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Item']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->itemRepository->findOrFail($id);
        $categories = $this->itemCategoryRepository->all();


        return view($this->_config['view'], compact('item','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


        $this->validate(request(), [

            'name' => 'string|required',
            'item_category_id' => 'required',
        ]);

        $this->itemRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Item']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->itemRepository->findOrFail($id);

        try {
            $this->itemRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Item']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Item']));
        }

        return response()->json(['message' => false], 400);
    }





}