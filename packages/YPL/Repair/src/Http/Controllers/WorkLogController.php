<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;

use Mail;
use YPL\Repair\Repositories\WorkLogRepository;

class WorkLogController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $workLogRepository;


    public function __construct(
        WorkLogRepository $workLogRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->workLogRepository = $workLogRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index($work_order_id)
    {
        return view($this->_config['view'],compact('work_order_id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($work_order_id)
    {
        return view($this->_config['view'],compact('work_order_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($work_order_id)
    {

        $this->validate(request(), [

            'note' => 'string|required',
        ]);
        $input = request()->all();
        $input['work_order_id'] =  $work_order_id;
        $input['user_id'] =  auth()->guard('admin')->user()->id;
        $partner = $this->workLogRepository->create($input);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'WorkLog']));

        return redirect()->route($this->_config['redirect'],$work_order_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $work_log = $this->workLogRepository->findOrFail($id);
        $work_order_id = $work_log->work_order_id;

        return view($this->_config['view'], compact('work_log','work_order_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


        $this->validate(request(), [

            'note' => 'string|required',
        ]);

        $workLog =  $this->workLogRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'WorkLog']));

        return redirect()->route($this->_config['redirect'],$workLog->work_order_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($work_order_id,$id)
    {
        $workLog = $this->workLogRepository->findOrFail($id);

        try {
            $this->workLogRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'WorkLog']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'WorkLog']));
        }

        return response()->json(['message' => false], 400);
    }





}