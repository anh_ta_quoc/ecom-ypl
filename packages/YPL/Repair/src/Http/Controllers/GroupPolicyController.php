<?php

namespace YPL\Repair\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Admin\Http\Controllers\Controller;

use Mail;
use YPL\Repair\Repositories\GroupPolicyRepository;
use YPL\Repair\Repositories\PolicyRepository;
use YPL\Repair\Repositories\MapPolicyRepository;

class GroupPolicyController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $groupPolicyRepository;
    protected $mapPolicyRepository;
    protected $policyRepository;


    public function __construct(
        GroupPolicyRepository $groupPolicyRepository,
        MapPolicyRepository $mapPolicyRepository,
        PolicyRepository $policyRepository

    )
    {
        $this->_config = request('_config');

        $this->middleware('admin');

        $this->groupPolicyRepository = $groupPolicyRepository;
        $this->mapPolicyRepository = $mapPolicyRepository;
        $this->policyRepository = $policyRepository;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $policies = $this->policyRepository->all();
        return view($this->_config['view'], compact('policies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $this->validate(request(), [

            'name' => 'string|required',
        ]);

        $group_policy = $this->groupPolicyRepository->create(request()->all());
        $input = request()->all();
        foreach($input['policy_ids'] as $policy_id){
            $data = [];
            $data['policy_id'] = $policy_id;
            $data['group_policy_id'] = $group_policy->id;
            $data['status'] = 0;
            $this->mapPolicyRepository->create($data);
        }

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'GroupPolicy']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $group_policy = $this->groupPolicyRepository->findOrFail($id);
        $policies = $this->policyRepository->all();

        return view($this->_config['view'], compact('group_policy','policies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {


        $this->validate(request(), [

            'name' => 'string|required',

        ]);


        $this->groupPolicyRepository->update(request()->all(), $id);
        $groupPolicy = $this->groupPolicyRepository->findOrFail($id);
        $this->mapPolicyRepository->where('group_policy_id',$groupPolicy->id)->delete();
        $input = request()->all();
        foreach($input['policy_ids'] as $policy_id){
            $data = [];
            $data['policy_id'] = $policy_id;
            $data['group_policy_id'] = $groupPolicy->id;
            $data['status'] = 0;
            $this->mapPolicyRepository->create($data);
        }


        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'GroupPolicy']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupPolicy = $this->groupPolicyRepository->findOrFail($id);

        try {
            $this->groupPolicyRepository->delete($id);
            $this->mapPolicyRepository->where('group_policy_id',$groupPolicy->id)->delete();


            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'GroupPolicy']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'GroupPolicy']));
        }

        return response()->json(['message' => false], 400);
    }





}