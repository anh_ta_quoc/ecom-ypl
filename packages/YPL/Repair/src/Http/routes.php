<?php
/**
 * Created by PhpStorm.
 * User: Teko
 * Date: 7/14/2020
 * Time: 4:37 PM
 */

Route::group(['prefix' => 'api'], function ($router) {
    Route::get('policy/{insurance_id?}', 'YPL\Repair\Http\Controllers\ApiController@index')
        ->name('policy.all');
});


Route::group(['middleware' => ['web']], function () {
    Route::prefix('admin')->group(function () {
        Route::get('item_category', 'YPL\Repair\Http\Controllers\ItemCategoryController@index')
            ->defaults('_config', [
                'view' => 'repair::item_category.index'])
            ->name('admin.item_category.index');
        Route::get('item_category/create', 'YPL\Repair\Http\Controllers\ItemCategoryController@create')
            ->defaults('_config', [
                'view' => 'repair::item_category.create'])
            ->name('admin.item_category.create');
        Route::post('item_category/store', 'YPL\Repair\Http\Controllers\ItemCategoryController@store')
            ->defaults('_config', [
                'redirect' => 'admin.item_category.index'])
            ->name('admin.item_category.store');
        Route::get('item_category/edit/{id}', 'YPL\Repair\Http\Controllers\ItemCategoryController@edit')
            ->defaults('_config', [
                'view' => 'repair::item_category.edit'])
            ->name('admin.item_category.edit');

        Route::put('/item_category/edit/{id}', 'YPL\Repair\Http\Controllers\ItemCategoryController@update')->defaults('_config', [
            'redirect' => 'admin.item_category.index'
        ])->name('admin.item_category.update');

        Route::post('/item_category/delete/{id}', 'YPL\Repair\Http\Controllers\ItemCategoryController@destroy')->name('admin.item_category.delete');


        Route::get('item', 'YPL\Repair\Http\Controllers\ItemController@index')
            ->defaults('_config', [
                'view' => 'repair::item.index'])
            ->name('admin.item.index');
        Route::get('item/create', 'YPL\Repair\Http\Controllers\ItemController@create')
            ->defaults('_config', [
                'view' => 'repair::item.create'])
            ->name('admin.item.create');
        Route::post('item/store', 'YPL\Repair\Http\Controllers\ItemController@store')
            ->defaults('_config', [
                'redirect' => 'admin.item.index'])
            ->name('admin.item.store');
        Route::get('item/edit/{id}', 'YPL\Repair\Http\Controllers\ItemController@edit')
            ->defaults('_config', [
                'view' => 'repair::item.edit'])
            ->name('admin.item.edit');

        Route::put('/item/edit/{id}', 'YPL\Repair\Http\Controllers\ItemController@update')->defaults('_config', [
            'redirect' => 'admin.item.index'
        ])->name('admin.item.update');

        Route::post('/item/delete/{id}', 'YPL\Repair\Http\Controllers\ItemController@destroy')->name('admin.item.delete');


        Route::get('work_order', 'YPL\Repair\Http\Controllers\WorkOrderController@index')
            ->defaults('_config', [
                'view' => 'repair::work_order.index'])
            ->name('admin.work_order.index');
        Route::post('work_order/export', 'YPL\Repair\Http\Controllers\ExportController@export')->name('work_order.datagrid.export');

        Route::get('work_order/create', 'YPL\Repair\Http\Controllers\WorkOrderController@create')
            ->defaults('_config', [
                'view' => 'repair::work_order.create'])
            ->name('admin.work_order.create');
        Route::get('work_order/create_batch', 'YPL\Repair\Http\Controllers\WorkOrderController@create_batch')
            ->defaults('_config', [
                'view' => 'repair::work_order.create_batch'])
            ->name('admin.work_order.create_batch');
        Route::post('work_order/store', 'YPL\Repair\Http\Controllers\WorkOrderController@store')
            ->defaults('_config', [
                'redirect' => 'admin.work_order.index'])
            ->name('admin.work_order.store');
        Route::post('work_order/store_batch', 'YPL\Repair\Http\Controllers\WorkOrderController@store_batch')
            ->defaults('_config', [
                'redirect' => 'admin.work_order.index'])
            ->name('admin.work_order.store_batch');
        Route::get('work_order/edit/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@edit')
            ->defaults('_config', [
                'view' => 'repair::work_order.edit'])
            ->name('admin.work_order.edit');
        Route::get('work_order/view/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@view')
            ->defaults('_config', [
                'view' => 'repair::work_order.view'])
            ->name('admin.work_order.view');

        Route::get('work_order/print/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@view')
            ->defaults('_config', [
                'view' => 'repair::work_order.print'])
            ->name('admin.work_order.print');

        Route::put('/work_order/update/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@update')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.update');

        Route::post('/work_order/delete/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@destroy')->name('admin.work_order.delete');


        Route::post('/work_order/change_job_status', 'YPL\Repair\Http\Controllers\WorkOrderController@changeJobStatus')->defaults('_config', [
            'redirect' => 'admin.work_order.index'
        ])->name('admin.work_order.changeJobStatus');

        Route::put('/work_order/issue/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@issue')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.issue');

        Route::put('/work_order/allocate/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@allocate')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.allocate');

        Route::put('/work_order/delivery/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@delivery')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.delivery');

        Route::put('/work_order/needToEstimate/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@needToEstimate')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.needToEstimate');

        Route::put('/work_order/sendQuotation/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@sendQuotation')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.sendQuotation');


        Route::put('/work_order/claim/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@claim')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.claim');
        Route::put('/work_order/assignment/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@assignment')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.assignment');
        Route::put('/work_order/return/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@return')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.return');
        Route::put('/work_order/needToPart/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@needToPart')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.needToPart');

        Route::put('/work_order/orderPart/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@orderPart')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.orderPart');

        Route::put('/work_order/savePart/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@savePart')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.savePart');
        Route::put('/work_order/savagePart/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@savagePart')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.savagePart');

        Route::put('/work_order/unitTest/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@unitTest')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.unitTest');

        Route::put('/work_order/complete/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@complete')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.complete');
        Route::put('/work_order/completeInvoice/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@completeInvoice')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.completeInvoice');

        Route::put('/work_order/reject/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@reject')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.reject');

        Route::put('/work_order/accept/{id}', 'YPL\Repair\Http\Controllers\WorkOrderController@accept')->defaults('_config', [
            'redirect' => 'admin.work_order.view'
        ])->name('admin.work_order.accept');



        Route::get('shop', 'YPL\Repair\Http\Controllers\ShopController@index')
            ->defaults('_config', [
                'view' => 'repair::shop.index'])
            ->name('admin.shop.index');
        Route::get('shop/create', 'YPL\Repair\Http\Controllers\ShopController@create')
            ->defaults('_config', [
                'view' => 'repair::shop.create'])
            ->name('admin.shop.create');
        Route::post('shop/store', 'YPL\Repair\Http\Controllers\ShopController@store')
            ->defaults('_config', [
                'redirect' => 'admin.shop.index'])
            ->name('admin.shop.store');
        Route::get('shop/edit/{id}', 'YPL\Repair\Http\Controllers\ShopController@edit')
            ->defaults('_config', [
                'view' => 'repair::shop.edit'])
            ->name('admin.shop.edit');

        Route::put('/shop/update/{id}', 'YPL\Repair\Http\Controllers\ShopController@update')->defaults('_config', [
            'redirect' => 'admin.shop.index'
        ])->name('admin.shop.update');

        Route::post('/shop/delete/{id}', 'YPL\Repair\Http\Controllers\ShopController@destroy')->name('admin.shop.delete');

        Route::get('partner', 'YPL\Repair\Http\Controllers\PartnerController@index')
            ->defaults('_config', [
                'view' => 'repair::partner.index'])
            ->name('admin.partner.index');
        Route::get('partner/create', 'YPL\Repair\Http\Controllers\PartnerController@create')
            ->defaults('_config', [
                'view' => 'repair::partner.create'])
            ->name('admin.partner.create');
        Route::post('partner/store', 'YPL\Repair\Http\Controllers\PartnerController@store')
            ->defaults('_config', [
                'redirect' => 'admin.partner.index'])
            ->name('admin.partner.store');
        Route::get('partner/edit/{id}', 'YPL\Repair\Http\Controllers\PartnerController@edit')
            ->defaults('_config', [
                'view' => 'repair::partner.edit'])
            ->name('admin.partner.edit');

        Route::put('/partner/update/{id}', 'YPL\Repair\Http\Controllers\PartnerController@update')->defaults('_config', [
            'redirect' => 'admin.partner.index'
        ])->name('admin.partner.update');

        Route::post('/partner/delete/{id}', 'YPL\Repair\Http\Controllers\PartnerController@destroy')->name('admin.partner.delete');


        Route::get('policy', 'YPL\Repair\Http\Controllers\PolicyController@index')
            ->defaults('_config', [
                'view' => 'repair::policy.index'])
            ->name('admin.policy.index');
        Route::get('policy/create', 'YPL\Repair\Http\Controllers\PolicyController@create')
            ->defaults('_config', [
                'view' => 'repair::policy.create'])
            ->name('admin.policy.create');
        Route::post('policy/store', 'YPL\Repair\Http\Controllers\PolicyController@store')
            ->defaults('_config', [
                'redirect' => 'admin.policy.index'])
            ->name('admin.policy.store');
        Route::get('policy/edit/{id}', 'YPL\Repair\Http\Controllers\PolicyController@edit')
            ->defaults('_config', [
                'view' => 'repair::policy.edit'])
            ->name('admin.policy.edit');

        Route::put('/policy/update/{id}', 'YPL\Repair\Http\Controllers\PolicyController@update')->defaults('_config', [
            'redirect' => 'admin.policy.index'
        ])->name('admin.policy.update');

        Route::post('/policy/delete/{id}', 'YPL\Repair\Http\Controllers\PolicyController@destroy')->name('admin.policy.delete');


        Route::get('group_policy', 'YPL\Repair\Http\Controllers\GroupPolicyController@index')
            ->defaults('_config', [
                'view' => 'repair::group_policy.index'])
            ->name('admin.group_policy.index');
        Route::get('group_policy/create', 'YPL\Repair\Http\Controllers\GroupPolicyController@create')
            ->defaults('_config', [
                'view' => 'repair::group_policy.create'])
            ->name('admin.group_policy.create');
        Route::post('group_policy/store', 'YPL\Repair\Http\Controllers\GroupPolicyController@store')
            ->defaults('_config', [
                'redirect' => 'admin.group_policy.index'])
            ->name('admin.group_policy.store');
        Route::get('group_policy/edit/{id}', 'YPL\Repair\Http\Controllers\GroupPolicyController@edit')
            ->defaults('_config', [
                'view' => 'repair::group_policy.edit'])
            ->name('admin.group_policy.edit');

        Route::put('/group_policy/update/{id}', 'YPL\Repair\Http\Controllers\GroupPolicyController@update')->defaults('_config', [
            'redirect' => 'admin.group_policy.index'
        ])->name('admin.group_policy.update');

        Route::post('/group_policy/delete/{id}', 'YPL\Repair\Http\Controllers\GroupPolicyController@destroy')->name('admin.group_policy.delete');



        Route::get('item_repair/{item_id?}', 'YPL\Repair\Http\Controllers\ItemRepairController@index')
            ->defaults('_config', [
                'view' => 'repair::item_repair.index'])
            ->name('admin.item_repair.index');
        Route::get('item_repair/{item_id}/create', 'YPL\Repair\Http\Controllers\ItemRepairController@create')
            ->defaults('_config', [
                'view' => 'repair::item_repair.create'])
            ->name('admin.item_repair.create');
        Route::post('item_repair/{item_id}/store', 'YPL\Repair\Http\Controllers\ItemRepairController@store')
            ->defaults('_config', [
                'redirect' => 'admin.item_repair.index'])
            ->name('admin.item_repair.store');
        Route::get('item_repair/edit/{id}', 'YPL\Repair\Http\Controllers\ItemRepairController@edit')
            ->defaults('_config', [
                'view' => 'repair::item_repair.edit'])
            ->name('admin.item_repair.edit');

        Route::put('/item_repair/update/{id}', 'YPL\Repair\Http\Controllers\ItemRepairController@update')->defaults('_config', [
            'redirect' => 'admin.item_repair.index'
        ])->name('admin.item_repair.update');

        Route::post('/item_repair/delete/{id}', 'YPL\Repair\Http\Controllers\ItemRepairController@destroy')->name('admin.item_repair.delete');

        Route::get('checklist', 'YPL\Repair\Http\Controllers\ChecklistController@index')
            ->defaults('_config', [
                'view' => 'repair::checklist.index'])
            ->name('admin.checklist.index');
        Route::get('checklist/create', 'YPL\Repair\Http\Controllers\ChecklistController@create')
            ->defaults('_config', [
                'view' => 'repair::checklist.create'])
            ->name('admin.checklist.create');
        Route::post('checklist/store', 'YPL\Repair\Http\Controllers\ChecklistController@store')
            ->defaults('_config', [
                'redirect' => 'admin.checklist.index'])
            ->name('admin.checklist.store');
        Route::get('checklist/edit/{id}', 'YPL\Repair\Http\Controllers\ChecklistController@edit')
            ->defaults('_config', [
                'view' => 'repair::checklist.edit'])
            ->name('admin.checklist.edit');

        Route::put('/checklist/update/{id}', 'YPL\Repair\Http\Controllers\ChecklistController@update')->defaults('_config', [
            'redirect' => 'admin.checklist.index'
        ])->name('admin.checklist.update');

        Route::post('/checklist/delete/{id}', 'YPL\Repair\Http\Controllers\ChecklistController@destroy')->name('admin.checklist.delete');


        Route::get('work_log/{work_order_id?}', 'YPL\Repair\Http\Controllers\WorkLogController@index')
            ->defaults('_config', [
                'view' => 'repair::work_log.index'])
            ->name('admin.work_log.index');
        Route::get('work_log/{work_order_id}/create', 'YPL\Repair\Http\Controllers\WorkLogController@create')
            ->defaults('_config', [
                'view' => 'repair::work_log.create'])
            ->name('admin.work_log.create');
        Route::post('work_log/{work_order_id}/store', 'YPL\Repair\Http\Controllers\WorkLogController@store')
            ->defaults('_config', [
                'redirect' => 'admin.work_log.index'])
            ->name('admin.work_log.store');
        Route::get('work_log/edit/{id}', 'YPL\Repair\Http\Controllers\WorkLogController@edit')
            ->defaults('_config', [
                'view' => 'repair::work_log.edit'])
            ->name('admin.work_log.edit');

        Route::put('/work_log/update/{id}', 'YPL\Repair\Http\Controllers\WorkLogController@update')->defaults('_config', [
            'redirect' => 'admin.work_log.index'
        ])->name('admin.work_log.update');

        Route::post('/work_log/delete/{id}', 'YPL\Repair\Http\Controllers\WorkLogController@destroy')->name('admin.work_log.delete');

    });
});