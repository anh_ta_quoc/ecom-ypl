<?php
namespace YPL\Repair\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

/**
 * Event service provider
 *
 * @author Anhtq <gondt8@gmail.com>
 * @copyright 2019 YPL Software
 */
class RepairServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');
        $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'repair');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/menu.php', 'menu.admin'
        );
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/acl.php', 'acl'
        );
        $this->app->register(EventServiceProvider::class);

        $this->app->register(ModuleServiceProvider::class);



    }
}
