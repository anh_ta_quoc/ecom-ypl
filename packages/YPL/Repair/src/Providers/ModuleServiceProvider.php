<?php

namespace  YPL\Repair\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \YPL\Repair\Models\RepairCategory::class,
        \YPL\Repair\Models\WorkOrder::class,
        \YPL\Repair\Models\WorkLog::class,
        \YPL\Repair\Models\Checklist::class,
        \YPL\Repair\Models\Partner::class,
        \YPL\Repair\Models\Policy::class,
        \YPL\Repair\Models\Shop::class,
        \YPL\Repair\Models\ShopStorage::class,
        \YPL\Repair\Models\ItemRepair::class,
        \YPL\Repair\Models\Item::class,
        \YPL\Repair\Models\ItemCategory::class,
        \YPL\Repair\Models\WorkOrderItem::class,
        \YPL\Repair\Models\GroupPolicy::class,
        \YPL\Repair\Models\MapPolicy::class


    ];
}