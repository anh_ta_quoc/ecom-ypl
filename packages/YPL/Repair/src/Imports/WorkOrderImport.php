<?php

namespace YPL\Repair\Imports;

use App\Work;
use YPL\Repair\Models\Partner;
use YPL\Repair\Models\Shop;
use YPL\Repair\Models\WorkOrder;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class WorkOrderImport implements ToModel, WithValidation, WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        $shop_id = '';
        $partner_id = '';
        $insurance_id = '';
        if ($row['type']=='shop') {
            $shop = Shop::where('name',$row['shop_name'])->get();
            if (count($shop) > 0 ) {
                $shop_id = $shop->first()->id;
            }
        }
        if ($row['type']=='partner') {
            $shop = Partner::where('name',$row['partner_name'])->get();
            if (count($shop) > 0 ) {
                $partner_id = $shop->first()->id;
            }
        }
        if ($row['type']=='insurance') {
            $shop = Partner::where('name',$row['insurance_name'])->get();
            if (count($shop) > 0 ) {
                $insurance_id = $shop->first()->id;
            }
        }

        return new WorkOrder([
            'customer_name'     => $row['customer_name'],
            'customer_email'    => $row['customer_email'],
            'customer_phone'    => $row['customer_phone'],
            'customer_address'  => $row['customer_address'],
            'product_name'      => $row['product_name'],
            'product_imei'      => $row['product_name'],
            'product_brand'      => $row['product_brand'],
            'note'              => $row['note'],
            'type'              => $row['type'],
            'job_status'        => WorkOrder::JOB_STATUS_OPENED,
            'status'            => WorkOrder::STATUS_SUBMITTED,
            'budget'            => $row['budget'],
            'shop_id'           =>$shop_id,
            'partner_id'        =>$partner_id,
            'insurance_id'      =>$insurance_id,
        ]);
    }

    public function rules(): array
    {
        return [

        ];
    }
}
