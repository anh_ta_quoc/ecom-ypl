<?php

return [
    [
        'key' => 'repair',
        'name' => 'Repair',
        'route' => 'admin.work_order.index',
        'sort' => 1,
        'icon-class' => 'repair-icon',
    ],
    [
        'key' => 'repair.work_order',
        'name' => 'Work Order',
        'route' => 'admin.work_order.index',
        'sort' => 2,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.shop',
        'name' => 'Shop',
        'route' => 'admin.shop.index',
        'sort' => 3,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.checklist',
        'name' => 'Checklist',
        'route' => 'admin.checklist.index',
        'sort' => 4,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.shop.shop',
        'name' => 'Shop',
        'route' => 'admin.shop.index',
        'sort' => 5,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.shop.partner',
        'name' => 'Partner',
        'route' => 'admin.partner.index',
        'sort' => 6,
        'icon-class' => '',
    ],


    [
        'key' => 'repair.work_order.work_log',
        'name' => 'Work Log',
        'route' => 'admin.work_log.index',
        'sort' =>7,
        'icon-class' => '',
    ],

    [
        'key' => 'repair.group_policy',
        'name' => 'Group Policy',
        'route' => 'admin.group_policy.index',
        'sort' => 8,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.item',
        'name' => 'Repair Model',
        'route' => 'admin.item.index',
        'sort' => 9,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.item.item',
        'name' => 'Product',
        'route' => 'admin.item.index',
        'sort' => 10,
        'icon-class' => '',
    ],
    [
        'key' => 'repair.item.product_brand',
        'name' => 'Product Brand',
        'route' => 'admin.item_category.index',
        'sort' => 11,
        'icon-class' => '',
    ],

];