<?php

return [
    [
        'key' => 'repair',
        'name' => 'Repair',
        'route' => 'admin.work_order.index',
        'sort' => 1,
    ],

    [
        'key' => 'repair.work_order',
        'name' => 'Work Order',
        'route' => 'admin.work_order.index',
        'sort' => 2,
    ],
    [
        'key' => 'repair.shop',
        'name' => 'Shop',
        'route' => 'admin.shop.index',
        'sort' => 3,
    ],
    [
        'key' => 'repair.checklist',
        'name' => 'Checklist',
        'route' => 'admin.checklist.index',
        'sort' => 4,
    ]

];