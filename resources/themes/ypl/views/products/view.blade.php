@extends('shop::layouts.master')
@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')
@inject ('customHelper', 'Webkul\Velocity\Helpers\Helper')
@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
@inject ('configurableOptionHelper', 'Webkul\Product\Helpers\ConfigurableOption')
@php
    $channel = core()->getCurrentChannel();

    $homeSEO = $channel->home_seo;
    if (isset($homeSEO)) {
        $homeSEO = json_decode($channel->home_seo);

        $metaTitle = $homeSEO->meta_title;

        $metaDescription = $homeSEO->meta_description;

        $metaKeywords = $homeSEO->meta_keywords;
    }
@endphp

@php
    $total = $reviewHelper->getTotalReviews($product);

    $avgRatings = $reviewHelper->getAverageRating($product);
    $avgStarRating = ceil($avgRatings);

    $productImages = [];
    $images = $productImageHelper->getGalleryImages($product);
    foreach ($images as $key => $image) {
        array_push($productImages, $image['medium_image_url']);
    }
    $relatedProducts = $product->related_products()->get();


    $config = $configurableOptionHelper->getConfigurationConfig($product);
@endphp

@section('page_title')
    {{ isset($metaTitle) ? $metaTitle : "" }}
@endsection

@section('head')

    @if (isset($homeSEO))
        @isset($metaTitle)
            <meta name="title" content="{{ $metaTitle }}"/>
        @endisset

        @isset($metaDescription)
            <meta name="description" content="{{ $metaDescription }}"/>
        @endisset

        @isset($metaKeywords)
            <meta name="keywords" content="{{ $metaKeywords }}"/>
        @endisset
    @endif
@endsection

@section('content-wrapper')
        <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('shop.home.index')}}">Home</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{$product['name']}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="container">
                <div class="product product--layout--standard" data-layout="standard">
                    <div class="product__content">
                        <!-- .product__gallery -->
                        @include('shop::products.view.gallery')
                        <!-- .product__gallery / end -->
                        <!-- .product__info -->
                        <div class="product__info">
                            <div class="product__wishlist-compare">
                                <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Wishlist">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="/themes/ypl/assets/images/sprite.svg#wishlist-16"></use>
                                    </svg>
                                </button>
                                <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Compare">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="/themes/ypl/assets/images/sprite.svg#compare-16"></use>
                                    </svg>
                                </button>
                            </div>
                            <h1 class="product__name">{{$product['name']}}</h1>
                            <div class="product__rating">
                                <div class="product__rating-stars">
                                    <div class="rating">
                                        <div class="rating__body">
                                            <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                <g class="rating__fill">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                </g>
                                                <g class="rating__stroke">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                </g>
                                            </svg>
                                            <div class="rating__star rating__star--only-edge rating__star--active">
                                                <div class="rating__fill">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                                <div class="rating__stroke">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                            </div>
                                            <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                <g class="rating__fill">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                </g>
                                                <g class="rating__stroke">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                </g>
                                            </svg>
                                            <div class="rating__star rating__star--only-edge rating__star--active">
                                                <div class="rating__fill">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                                <div class="rating__stroke">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                            </div>
                                            <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                <g class="rating__fill">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                </g>
                                                <g class="rating__stroke">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                </g>
                                            </svg>
                                            <div class="rating__star rating__star--only-edge rating__star--active">
                                                <div class="rating__fill">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                                <div class="rating__stroke">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                            </div>
                                            <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                <g class="rating__fill">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                </g>
                                                <g class="rating__stroke">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                </g>
                                            </svg>
                                            <div class="rating__star rating__star--only-edge rating__star--active">
                                                <div class="rating__fill">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                                <div class="rating__stroke">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                            </div>
                                            <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                <g class="rating__fill">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                </g>
                                                <g class="rating__stroke">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                </g>
                                            </svg>
                                            <div class="rating__star rating__star--only-edge rating__star--active">
                                                <div class="rating__fill">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                                <div class="rating__stroke">
                                                    <div class="fake-svg-icon"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product__rating-legend">
                                    <a href="">{{$total}} Reviews</a>
                                    <span>/</span>
                                    <a href="">Write A Review</a>
                                </div>
                            </div>
                            <div class="product__description">{!! $product['short_description'] !!}</div>
                            {{--<ul class="product__features">--}}
                                {{--<li>Speed: 750 RPM</li>--}}
                                {{--<li>Power Source: Cordless-Electric</li>--}}
                                {{--<li>Battery Cell Type: Lithium</li>--}}
                                {{--<li>Voltage: 20 Volts</li>--}}
                                {{--<li>Battery Capacity: 2 Ah</li>--}}
                            {{--</ul>--}}
                            <ul class="product__meta">
                                @include('shop::products.view.stock', ['meta' => true])
                                {{--<li>Brand:--}}
                                    {{--<a href="">Wakita</a>--}}
                                {{--</li>--}}
                                <li>SKU: {{$product['sku']}}</li>
                            </ul>
                        </div>
                        <!-- .product__info / end -->
                        <!-- .product__sidebar -->
                        <div class="product__sidebar">
                            @include('shop::products.view.stock')
                            <div class="product__prices">{!! $product['priceHTML'] !!}</div>
                            <!-- .product__options -->
                            {{--<form class="product__options">--}}
                                {{--<div class="form-group product__option">--}}
                                    {{--<label class="product__option-label">Color</label>--}}
                                    {{--<div class="input-radio-color">--}}
                                        {{--<div class="input-radio-color__list">--}}
                                            {{--<label class="input-radio-color__item input-radio-color__item--white" style="color: #fff;" data-toggle="tooltip" title="White">--}}
                                                {{--<input type="radio" name="color">--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<label class="input-radio-color__item" style="color: #ffd333;" data-toggle="tooltip" title="Yellow">--}}
                                                {{--<input type="radio" name="color">--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<label class="input-radio-color__item" style="color: #ff4040;" data-toggle="tooltip" title="Red">--}}
                                                {{--<input type="radio" name="color">--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                            {{--<label class="input-radio-color__item input-radio-color__item--disabled" style="color: #4080ff;" data-toggle="tooltip" title="Blue">--}}
                                                {{--<input type="radio" name="color" disabled="disabled">--}}
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group product__option">--}}
                                    {{--<label class="product__option-label">Material</label>--}}
                                    {{--<div class="input-radio-label">--}}
                                        {{--<div class="input-radio-label__list">--}}
                                            {{--<label>--}}
                                                {{--<input type="radio" name="material">--}}
                                                {{--<span>Metal</span>--}}
                                            {{--</label>--}}
                                            {{--<label>--}}
                                                {{--<input type="radio" name="material">--}}
                                                {{--<span>Wood</span>--}}
                                            {{--</label>--}}
                                            {{--<label>--}}
                                                {{--<input type="radio" name="material" disabled="disabled">--}}
                                                {{--<span>Plastic</span>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group product__option">--}}
                                    {{--<label class="product__option-label" for="product-quantity">Quantity</label>--}}
                                    {{--<div class="product__actions">--}}
                                        {{--<div class="product__actions-item">--}}
                                            {{--<div class="input-number product__quantity">--}}
                                                {{--<input id="product-quantity" class="input-number__input form-control form-control-lg" type="number" min="1" value="1">--}}
                                                {{--<div class="input-number__add"></div>--}}
                                                {{--<div class="input-number__sub"></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="product__actions-item product__actions-item--addtocart">--}}
                                            {{--<button class="btn btn-primary btn-lg">Add to cart</button>--}}
                                        {{--</div>--}}
                                        {{--<div class="product__actions-item product__actions-item--wishlist">--}}
                                            {{--<button type="button" class="btn btn-secondary btn-svg-icon btn-lg" data-toggle="tooltip" title="Wishlist">--}}
                                                {{--<svg width="16px" height="16px">--}}
                                                    {{--<use xlink:href="/themes/ypl/assets/images/sprite.svg#wishlist-16"></use>--}}
                                                {{--</svg>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                        {{--<div class="product__actions-item product__actions-item--compare">--}}
                                            {{--<button type="button" class="btn btn-secondary btn-svg-icon btn-lg" data-toggle="tooltip" title="Compare">--}}
                                                {{--<svg width="16px" height="16px">--}}
                                                    {{--<use xlink:href="/themes/ypl/assets/images/sprite.svg#compare-16"></use>--}}
                                                {{--</svg>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                            <!-- .product__options / end -->
                        </div>
                        <!-- .product__end -->
                        <div class="product__footer">
                            {{--<div class="product__tags tags">--}}
                                {{--<div class="tags__list">--}}
                                    {{--<a href="">Mounts</a>--}}
                                    {{--<a href="">Electrodes</a>--}}
                                    {{--<a href="">Chainsaws</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="product__share-links share-links">
                                <ul class="share-links__list">
                                    <li class="share-links__item share-links__item--type--like">
                                        <a href="">Like</a>
                                    </li>
                                    <li class="share-links__item share-links__item--type--tweet">
                                        <a href="">Tweet</a>
                                    </li>
                                    <li class="share-links__item share-links__item--type--pin">
                                        <a href="">Pin It</a>
                                    </li>
                                    <li class="share-links__item share-links__item--type--counter">
                                        <a href="">4K</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-tabs product-tabs--sticky">
                    <div class="product-tabs__list">
                        <div class="product-tabs__list-body">
                            <div class="product-tabs__list-container container">
                                <a href="#tab-description" class="product-tabs__item product-tabs__item--active">Description</a>
                                {{--<a href="#tab-specification" class="product-tabs__item">Specification</a>--}}
                                <a href="#tab-reviews" class="product-tabs__item">Reviews</a>
                            </div>
                        </div>
                    </div>
                    <div class="product-tabs__content">
                        <div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
                            <div class="typography">
                             {!! $product['description'] !!}
                             </div>
                        </div>
                        {{--<div class="product-tabs__pane" id="tab-specification">--}}
                            {{--<div class="spec">--}}
                                {{--<h3 class="spec__header">Specification</h3>--}}
                                {{--<div class="spec__section">--}}
                                    {{--<h4 class="spec__section-title">General</h4>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Material</div>--}}
                                        {{--<div class="spec__value">Aluminium, Plastic</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Engine Type</div>--}}
                                        {{--<div class="spec__value">Brushless</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Battery Voltage</div>--}}
                                        {{--<div class="spec__value">18 V</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Battery Type</div>--}}
                                        {{--<div class="spec__value">Li-lon</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Number of Speeds</div>--}}
                                        {{--<div class="spec__value">2</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Charge Time</div>--}}
                                        {{--<div class="spec__value">1.08 h</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Weight</div>--}}
                                        {{--<div class="spec__value">1.5 kg</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="spec__section">--}}
                                    {{--<h4 class="spec__section-title">Dimensions</h4>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Length</div>--}}
                                        {{--<div class="spec__value">99 mm</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Width</div>--}}
                                        {{--<div class="spec__value">207 mm</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="spec__row">--}}
                                        {{--<div class="spec__name">Height</div>--}}
                                        {{--<div class="spec__value">208 mm</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="spec__disclaimer">Information on technical characteristics, the delivery set, the country of manufacture and the appearance of the goods is for reference only and is based on the latest information available at the time of publication.</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="product-tabs__pane" id="tab-reviews">

                            <div class="reviews-view">
                                @if ($total)
                                <div class="reviews-view__list">
                                    <h3 class="reviews-view__header">Customer Reviews</h3>
                                    <div class="reviews-list">
                                        <ol class="reviews-list__content">
                                            @foreach ($reviewHelper->getReviews($product)->paginate(5) as $review)
                                            <li class="reviews-list__item">
                                                <div class="review">
                                                    <div class="review__avatar">
                                                        <img src="/themes/ypl/assets/images/avatars/avatar-1.jpg" alt="">
                                                    </div>
                                                    <div class="review__content">
                                                        <div class="review__author">{{ $review->name ? $review->name . ' - ' : ''}}{{$review->title }} </div>
                                                        <div class="review__rating">
                                                            <div class="rating">
                                                                <div class="rating__body">
                                                                    <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                                        </g>
                                                                    </svg>
                                                                    <div class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                                        </g>
                                                                    </svg>
                                                                    <div class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                                        </g>
                                                                    </svg>
                                                                    <div class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                                        </g>
                                                                    </svg>
                                                                    <div class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star" width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                                        </g>
                                                                    </svg>
                                                                    <div class="rating__star rating__star--only-edge">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="review__text"> {{ $review->comment }}</div>
                                                        <div class="review__date">{{ core()->formatDate($review->created_at, 'd m Y') }}</div>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach

                                        </ol>
                                        <div class="reviews-list__pagination">
                                            {{--TODO: ADD pagination for reviews--}}
                                            <ul class="pagination justify-content-center">
                                                <li class="page-item disabled">
                                                    <a class="page-link page-link--with-arrow" href="" aria-label="Previous">
                                                        <svg class="page-link__arrow page-link__arrow--left" aria-hidden="true" width="8px" height="13px">
                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-left-8x13"></use>
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="">1</a>
                                                </li>
                                                <li class="page-item active">
                                                    <a class="page-link" href="">2
                                                        <span class="sr-only">(current)</span>
                                                    </a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link page-link--with-arrow" href="" aria-label="Next">
                                                        <svg class="page-link__arrow page-link__arrow--right" aria-hidden="true" width="8px" height="13px">
                                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-8x13"></use>
                                                        </svg>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                    <form class="reviews-view__form">
                                    <h3 class="reviews-view__header">Write A Review</h3>
                                    <div class="row">
                                        <div class="col-12 col-lg-9 col-xl-8">
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="review-stars">Review Stars</label>
                                                    <select id="review-stars" class="form-control">
                                                        <option>5 Stars Rating</option>
                                                        <option>4 Stars Rating</option>
                                                        <option>3 Stars Rating</option>
                                                        <option>2 Stars Rating</option>
                                                        <option>1 Stars Rating</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="review-author">Your Name</label>
                                                    <input type="text" class="form-control" id="review-author" placeholder="Your Name">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="review-email">Email Address</label>
                                                    <input type="text" class="form-control" id="review-email" placeholder="Email Address">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="review-text">Your Review</label>
                                                <textarea class="form-control" id="review-text" rows="6"></textarea>
                                            </div>
                                            <div class="form-group mb-0">
                                                <button type="submit" class="btn btn-primary btn-lg">Post Your Review</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-products-carousel -->
        @if ($relatedProducts->count())

            <div class="block block-products-carousel" data-layout="grid-5" data-mobile-grid-columns="2">
            <div class="container">
                <div class="block-header">
                    <h3 class="block-header__title">Related Products</h3>
                    <div class="block-header__divider"></div>
                    <div class="block-header__arrows-list">
                        <button class="block-header__arrow block-header__arrow--left" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-left-7x11"></use>
                            </svg>
                        </button>
                        <button class="block-header__arrow block-header__arrow--right" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-7x11"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="block-products-carousel__slider">
                    <div class="block-products-carousel__preloader"></div>
                    <div class="owl-carousel">
                        @foreach($relatedProducts as $product)
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card product-card--hidden-actions">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#quickview-16"></use>
                                        </svg>
                                        <span class="fake-svg-icon"></span>
                                    </button>
                                    <div class="product-card__badges-list">
                                        <div class="product-card__badge product-card__badge--new">New</div>
                                    </div>
                                    <div class="product-card__image product-image">
                                        <a href="{{ route('shop.productOrCategory.index', $product->url_key) }}" class="product-image__body">
                                            <img class="product-image__img" src="{{$productImageHelper->getProductBaseImage($product)['medium_image_url']}}" alt="">
                                        </a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name">
                                            <a href="{{ route('shop.productOrCategory.index', $product->url_key) }}">{{$product['name']}}</a>
                                        </div>
                                        <div class="product-card__rating">
                                            <div class="product-card__rating-stars">
                                                <div class="rating">
                                                    <div class="rating__body">
                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-card__rating-legend">{{$reviewHelper->getTotalReviews($product)}} Reviews</div>
                                        </div>
                                        {{--<ul class="product-card__features-list">--}}
                                            {{--<li>Speed: 750 RPM</li>--}}
                                            {{--<li>Power Source: Cordless-Electric</li>--}}
                                            {{--<li>Battery Cell Type: Lithium</li>--}}
                                            {{--<li>Voltage: 20 Volts</li>--}}
                                            {{--<li>Battery Capacity: 2 Ah</li>--}}
                                        {{--</ul>--}}
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability:
                                            <span class="text-success">In Stock</span>
                                        </div>
                                        <div class="product-card__prices">{!! $product['priceHTML'] !!}</div>
                                        <div class="product-card__buttons">
                                            {{--<button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>--}}
                                            {{--<button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>--}}
                                            {{--<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">--}}
                                                {{--<svg width="16px" height="16px">--}}
                                                    {{--<use xlink:href="/themes/ypl/assets/images/sprite.svg#wishlist-16"></use>--}}
                                                {{--</svg>--}}
                                                {{--<span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>--}}
                                            {{--</button>--}}
                                            {{--<button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">--}}
                                                {{--<svg width="16px" height="16px">--}}
                                                    {{--<use xlink:href="/themes/ypl/assets/images/sprite.svg#compare-16"></use>--}}
                                                {{--</svg>--}}
                                                {{--<span class="fake-svg-icon fake-svg-icon--compare-16"></span>--}}
                                            {{--</button>--}}
                                            {!! $product['addToCartHTML'] !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
         @endif
        <!-- .block-products-carousel / end -->
@endsection