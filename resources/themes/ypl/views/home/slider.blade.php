@if ($velocityMetaData->slider)
    <div class="block-slideshow__body">
        <div class="owl-carousel">
            @if (! empty($sliderData))
                @foreach ($sliderData as $index => $slider)
                    @php
                        $textContent = str_replace("\r\n", '', $slider['content']);
                    @endphp
                    <a class="block-slideshow__slide" @if($slider['slider_path']) href="{{ $slider['slider_path'] }}" @endif>
                        <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop"
                             style="background-image: url('{{ url()->to('/') . '/storage/' . $slider['path'] }}')"></div>
                        <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile"
                             style="background-image: url('{{ url()->to('/') . '/storage/' . $slider['path'] }}')"></div>
                        <div class="block-slideshow__slide-content">
                            <div class="block-slideshow__slide-title"></div>
                            <div class="block-slideshow__slide-text">
                                {{ $textContent }}
                            </div>
                            <div class="block-slideshow__slide-button">
                                <span class="btn btn-primary btn-lg">Shop Now</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            @else
                <a class="block-slideshow__slide" href="#">
                    <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop"
                         style="background-image: url('{{ asset('/themes/velocity/assets/images/banner.png') }}')"></div>
                    <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile"
                         style="background-image: url('{{ asset('/themes/velocity/assets/images/banner.png') }}')"></div>
                </a>
            @endif
        </div>
    </div>
@endif
