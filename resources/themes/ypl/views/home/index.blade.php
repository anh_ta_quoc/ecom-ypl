@extends('shop::layouts.master')

@php
    $channel = core()->getCurrentChannel();
    $homeSEO = $channel->home_seo;
    if (isset($homeSEO)) {
        $homeSEO = json_decode($channel->home_seo);

        $metaTitle = $homeSEO->meta_title;

        $metaDescription = $homeSEO->meta_description;

        $metaKeywords = $homeSEO->meta_keywords;
    }
@endphp

@section('page_title')
    {{ isset($metaTitle) ? $metaTitle : "" }}
@endsection

@section('head')

    @if (isset($homeSEO))
        @isset($metaTitle)
            <meta name="title" content="{{ $metaTitle }}"/>
        @endisset

        @isset($metaDescription)
            <meta name="description" content="{{ $metaDescription }}"/>
        @endisset

        @isset($metaKeywords)
            <meta name="keywords" content="{{ $metaKeywords }}"/>
        @endisset
    @endif
@endsection

@section('content-wrapper')
    <div class="block-slideshow block-slideshow--layout--with-departments block">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 d-none d-lg-block"></div>
                <div class="col-12 col-lg-9">
                    @include('shop::home.slider')

                </div>
            </div>
        </div>
    </div><!-- .block-slideshow / end --><!-- .block-features -->
    <div class="block block-features block-features--layout--classic">
        <div class="container">
            <div class="block-features__list">
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#fi-free-delivery-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">Free Shipping</div>
                        <div class="block-features__subtitle">For orders from $50</div>
                    </div>
                </div>
                <div class="block-features__divider"></div>
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#fi-24-hours-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">Support 24/7</div>
                        <div class="block-features__subtitle">Call us anytime</div>
                    </div>
                </div>
                <div class="block-features__divider"></div>
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#fi-payment-security-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">100% Safety</div>
                        <div class="block-features__subtitle">Only secure payments</div>
                    </div>
                </div>
                <div class="block-features__divider"></div>
                <div class="block-features__item">
                    <div class="block-features__icon">
                        <svg width="48px" height="48px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#fi-tag-48"></use>
                        </svg>
                    </div>
                    <div class="block-features__content">
                        <div class="block-features__title">Hot Offers</div>
                        <div class="block-features__subtitle">Discounts up to 90%</div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .block-features / end --><!-- .block-products-carousel -->
    <div class="block block-products-carousel" data-layout="grid-4">
        <div class="container">
            <div class="block-header"><h3 class="block-header__title">Featured Products</h3>
                <div class="block-header__divider"></div>
                <ul class="block-header__groups-list">

                </ul>
                <div class="block-header__arrows-list">
                    <button class="block-header__arrow block-header__arrow--left" type="button">
                        <svg width="7px" height="11px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-left-7x11"></use>
                        </svg>
                    </button>
                    <button class="block-header__arrow block-header__arrow--right" type="button">
                        <svg width="7px" height="11px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-7x11"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="block-products-carousel__slider">
                <div class="block-products-carousel__preloader"></div>
                <div class="owl-carousel">
                    @foreach($arr_feature_products as $item)
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card product-card--hidden-actions">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#quickview-16"></use>
                                        </svg>
                                        <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__badges-list">
                                        <div class="product-card__badge product-card__badge--new">New</div>
                                    </div>
                                    <div class="product-card__image"><a
                                            href="{{route('shop.productOrCategory.index',$item['slug'])}}"><img
                                                src="{{$item['image']}}" alt=""></a></div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a
                                                href="{{route('shop.productOrCategory.index',$item['slug'])}}">{{$item['name']}}</a>
                                        </div>
                                        <div class="product-card__rating">
                                            <div class="product-card__rating-stars">
                                                <div class="rating">
                                                    <div class="rating__body">
                                                        {{--TODO: calculate avrRating--}}
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-card__rating-legend">{{$item['totalReviews']}}Reviews
                                            </div>
                                        </div>
                                        <ul class="product-card__features-list">

                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span>
                                        </div>
                                        <div class="product-card__prices">{!!$item['priceHTML']!!}</div>
                                        <div class="product-card__buttons">

                                            {!! $item['addToCartHtml']!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div><!-- .block-products-carousel / end --><!-- .block-banner -->
    <div class="block block-banner">
        <div class="container"><a href="#" class="block-banner__body">
                <div class="block-banner__image block-banner__image--desktop"
                     style="background-image: url('/themes/ypl/assets/images/banners/banner-1.jpg')"></div>
                <div class="block-banner__image block-banner__image--mobile"
                     style="background-image: url('/themes/ypl/assets/images/banners/banner-1-mobile.jpg')"></div>
                <div class="block-banner__title">Hundreds<br class="block-banner__mobile-br">Hand Tools</div>
                <div class="block-banner__text">Hammers, Chisels, Universal Pliers, Nippers, Jigsaws, Saws</div>
                <div class="block-banner__button"><span class="btn btn-sm btn-primary">Shop Now</span></div>
            </a></div>
    </div><!-- .block-banner / end --><!-- .block-products -->

    <div class="block block-products-carousel" data-layout="horizontal">
        <div class="container">
            <div class="block-header"><h3 class="block-header__title">New Arrivals</h3>
                <div class="block-header__divider"></div>

                <div class="block-header__arrows-list">
                    <button class="block-header__arrow block-header__arrow--left" type="button">
                        <svg width="7px" height="11px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-left-7x11"></use>
                        </svg>
                    </button>
                    <button class="block-header__arrow block-header__arrow--right" type="button">
                        <svg width="7px" height="11px">
                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-7x11"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="block-products-carousel__slider">
                <div class="block-products-carousel__preloader"></div>
                <div class="owl-carousel">
                    @foreach($arr_new_products as $item)
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="/themes/ypl/assets/images/sprite.svg#quickview-16"></use>
                                        </svg>
                                        <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__badges-list">
                                        <div class="product-card__badge product-card__badge--new">New</div>
                                    </div>
                                    <div class="product-card__image">
                                        <a href="{{route('shop.productOrCategory.index',$item['slug'])}}">
                                            <img src="{{$item['image']}}" alt="">
                                        </a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name">
                                            <a href="{{route('shop.productOrCategory.index',$item['slug'])}}">{{$item['name']}}</a>
                                        </div>
                                        <div class="product-card__rating">
                                            <div class="product-card__rating-stars">
                                                <div class="rating">
                                                    <div class="rating__body">
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star rating__star--active" width="13px"
                                                             height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div
                                                            class="rating__star rating__star--only-edge rating__star--active">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                        <svg class="rating__star" width="13px" height="12px">
                                                            <g class="rating__fill">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal"></use>
                                                            </g>
                                                            <g class="rating__stroke">
                                                                <use
                                                                    xlink:href="/themes/ypl/assets/images/sprite.svg#star-normal-stroke"></use>
                                                            </g>
                                                        </svg>
                                                        <div class="rating__star rating__star--only-edge">
                                                            <div class="rating__fill">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                            <div class="rating__stroke">
                                                                <div class="fake-svg-icon"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-card__rating-legend">{{$item['totalReviews']}}Reviews
                                            </div>
                                        </div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span>
                                        </div>
                                        <div class="product-card__prices">{!! $item['priceHTML'] !!}</div>
                                        <div class="product-card__buttons">

                                            {!! $item['addToCartHtml']!!}

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div><!-- .block-products-carousel / end --><!-- .block-posts -->

    <div class="block block-brands">
        <div class="container">
            <div class="block-brands__slider">
                <div class="owl-carousel">
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-1.png"
                                                                     alt=""></a></div>
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-2.png"
                                                                     alt=""></a></div>
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-3.png"
                                                                     alt=""></a></div>
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-4.png"
                                                                     alt=""></a></div>
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-5.png"
                                                                     alt=""></a></div>
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-6.png"
                                                                     alt=""></a></div>
                    <div class="block-brands__item"><a href="#"><img src="/themes/ypl/assets/images/logos/logo-7.png"
                                                                     alt=""></a></div>
                </div>
            </div>
        </div>
    </div><!-- .block-brands / end -->


@endsection