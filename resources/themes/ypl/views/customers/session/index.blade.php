@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.login-form.page-title') }}
@endsection

@section('content-wrapper')
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('shop.home.index') }}">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="/themes/ypl/assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">My Account</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title"><h1>My Account</h1></div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex flex-column">
                    <div class="card flex-grow-1 mb-md-0">
                        {!! view_render_event('bagisto.shop.customers.login.before') !!}
                        <div class="card-body">
                            <h3 class="card-title">Login</h3>
                            <form method="POST"
                                  action="{{ route('customer.session.create') }}">
                                @csrf()
                                {!! view_render_event('bagisto.shop.customers.login_form_controls.before') !!}
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control" placeholder="Enter email"
                                           name="email"
                                           value="{{ old('email') }}">
                                    <span class="control-error" v-if="errors.has('email')">
                                    @{{ errors.first('email') }}
                                </span>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password"
                                           name="password"
                                           value="{{ old('password') }}">
                                    <span class="control-error" v-if="errors.has('password')">
                                        @{{ errors.first('password') }}
                                    </span>
                                    <small class="form-text text-muted">
                                        <a href="{{ route('customer.forgot-password.create') }}">Forgotten Password</a>
                                    </small>
                                </div>
                                <div class="mt10">
                                    @if (Cookie::has('enable-resend'))
                                        @if (Cookie::get('enable-resend') == true)
                                            <a href="{{ route('customer.resend.verification-email', Cookie::get('email-for-resend')) }}">{{ __('shop::app.customer.login-form.resend-verification') }}</a>
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <span class="form-check-input input-check">
                                            <span class="input-check__body">
                                                <input class="input-check__input" type="checkbox" id="login-remember">
                                                <span class="input-check__box"></span>
                                                <svg class="input-check__icon" width="9px" height="7px">
                                                    <use xlink:href="/themes/ypl/assets/images/sprite.svg#check-9x7"></use>
                                                </svg>
                                            </span>
                                        </span>
                                        <label class="form-check-label" for="login-remember">Remember Me</label>
                                    </div>
                                </div>

                                {!! view_render_event('bagisto.shop.customers.login_form_controls.after') !!}
                                <button type="submit" class="btn btn-primary mt-4">Login</button>
                            </form>
                        </div>
                        {!! view_render_event('bagisto.shop.customers.login.after') !!}
                    </div>
                </div>
                <div class="col-md-6 d-flex flex-column mt-4 mt-md-0">
                    <div class="card flex-grow-1 mb-0">
                        <div class="card-body"><h3 class="card-title">Register</h3>
                            <form
                                method="post"
                                action="{{ route('customer.register.create') }}">
                                @csrf()
                                <div class="form-group" :class="[errors.has('last_name') ? 'has-error' : '']">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name"
                                           name="first_name"
                                           value="{{ old('first_name') }}">
                                    <span class="control-error" v-if="errors.has('first_name')">
                                        @{{ errors.first('first_name') }}
                                    </span>
                                </div>
                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.firstname.after') !!}

                                <div class="form-group" :class="[errors.has('last_name') ? 'has-error' : '']">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" placeholder="Last Name"
                                           name="last_name"
                                           value="{{ old('last_name') }}">
                                    <span class="control-error" v-if="errors.has('last_name')">
                                        @{{ errors.first('last_name') }}
                                    </span>
                                </div>
                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.lastname.after') !!}

                                <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                                    <label>Email</label>
                                    <input type="text" class="form-control"
                                           placeholder="Email"
                                           name="email"
                                           value="{{ old('email') }}">
                                    <span class="control-error" v-if="errors.has('email')">
                                        @{{ errors.first('email') }}
                                    </span>
                                </div>
                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.email.after') !!}

                                <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password"
                                           name="password"
                                           value="{{ old('password') }}">
                                    <span class="control-error" v-if="errors.has('password')">
                                        @{{ errors.first('password') }}
                                    </span>
                                </div>
                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.password.after') !!}

                                <div class="form-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" placeholder="Confirm Password"
                                           name="password_confirmation"
                                           value="{{ old('password_confirmation') }}">
                                    <span class="control-error" v-if="errors.has('password_confirmation')">
                                        @{{ errors.first('password_confirmation') }}
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-primary mt-4">
                                    {{ __('shop::app.customer.signup-form.title') }}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
