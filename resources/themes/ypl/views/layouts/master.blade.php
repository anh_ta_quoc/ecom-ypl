<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">

<head>

    <title>@yield('page_title')</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}">
    <script>
        window.csrfToken = '<?php echo csrf_token(); ?>'
    </script>

    <script
        type="text/javascript"
        baseUrl="{{ url()->to('/') }}"
        src="{{ asset('themes/velocity/assets/js/velocity.js') }}">
    </script>

    <script
        type="text/javascript"
        src="{{ asset('themes/velocity/assets/js/jquery.ez-plus.js') }}">
    </script>



    <link rel="icon" type="image/png" href="images/favicon.png"><!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i"><!-- css -->
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/vendor/owl-carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/fonts/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/vendor/photoswipe/photoswipe.cs')}}s">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/vendor/photoswipe/default-skin/default-skin.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/vendor/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/css/ovic-mobile-menu.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/css/style.css')}}"><!-- font - fontawesome -->
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/vendor/fontawesome/css/all.min.css')}}">
    <!-- font - stroyka -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="{{asset('themes/ypl/assets/fonts/stroyka/stroyka.css')}}">


    @yield('head')

    @section('seo')
        @if (! request()->is('/'))
            <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
        @endif
    @show


    @stack('css')

    {!! view_render_event('bagisto.shop.layout.head') !!}

</head>


<body @if (core()->getCurrentLocale()->direction == 'rtl') class="rtl" @endif style="scroll-behavior: smooth;">

{!! view_render_event('bagisto.shop.layout.body.before') !!}

@include('shop::UI.particals')
<div id="app" class="site">
    {!! view_render_event('bagisto.shop.layout.header.before') !!}

    @include('shop::layouts.header.index')

    {!! view_render_event('bagisto.shop.layout.header.after') !!}

    <div class="site__body">

        @yield('slider')

        {!! view_render_event('bagisto.shop.layout.content.before') !!}

        @yield('content-wrapper')

        {!! view_render_event('bagisto.shop.layout.content.after') !!}

        {!! view_render_event('bagisto.shop.layout.full-content.before') !!}

        @yield('full-content-wrapper')

        {!! view_render_event('bagisto.shop.layout.full-content.after') !!}

    </div>

    {!! view_render_event('bagisto.shop.layout.footer.before') !!}

    @include('shop::layouts.footer.footer')

    {!! view_render_event('bagisto.shop.layout.footer.after') !!}

    @if (core()->getConfigData('general.content.footer.footer_toggle'))
        <div class="footer">
            <p style="text-align: center;">
                @if (core()->getConfigData('general.content.footer.footer_content'))
                    {{ core()->getConfigData('general.content.footer.footer_content') }}
                @else
                    {!! trans('admin::app.footer.copy-right') !!}
                @endif
            </p>
        </div>
    @endif
</div>
<!-- photoswipe -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <!--<button class="pswp__button pswp__button&#45;&#45;share" title="Share"></button>-->
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    let DIRECTION = null;

    function direction() {
        if (DIRECTION === null) {
            DIRECTION = getComputedStyle(document.body).direction;
        }

        return DIRECTION;
    }

    function isRTL() {
        return direction() === 'rtl';
    }
</script>

<!-- js -->
@stack('scripts')

<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{asset('themes/ypl/assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/nouislider/nouislider.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/photoswipe/photoswipe.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/photoswipe/photoswipe-ui-default.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/select2/js/select2.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/js/number.js')}}"></script>
<script src="{{asset('themes/ypl/assets/js/main.js')}}"></script>
<script src="{{asset('themes/ypl/assets/js/header.js')}}"></script>
<script src="{{asset('themes/ypl/assets/js/ovic-mobile-menu.js')}}"></script>
<script src="{{asset('themes/ypl/assets/js/mobilemenu.min.js')}}"></script>
<script src="{{asset('themes/ypl/assets/vendor/svg4everybody/svg4everybody.min.js')}}"></script>
<script>svg4everybody();</script>

@yield('custom_script')

<script type="text/javascript">
    (() => {
        var showAlert = (messageType, messageLabel, message) => {
            if (messageType && message !== '') {
                let html = `<div class="alert ${messageType} alert-dismissible" id="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>${messageLabel} !</strong> ${message}.
                        </div>`;
                toastr.info(message)

                // window.setTimeout(() => {
                //     $(".alert").fadeTo(500, 0).slideUp(500, () => {
                //         $(this).remove();
                //     });
                // }, 5000);
            }
        };

        @if ($message = session('success'))
            toastr.success("{{ $message }}");
        @elseif ($message = session('warning'))
            toastr.warning("{{ $message }}");
        @elseif ($message = session('error'))
            toastr.error("{{ $message }}");
        @elseif ($message = session('info'))
            toastr.info("{{ $message }}");
        @endif

        window.serverErrors = [];
        @if (isset($errors))
            @if (count($errors))
            window.serverErrors = @json($errors->getMessages());
        @endif
            @endif

            window._translations = @json(app('Webkul\Velocity\Helpers\Helper')->jsonTranslations());
    })();
</script>

{!! view_render_event('bagisto.shop.layout.body.after') !!}

</html>